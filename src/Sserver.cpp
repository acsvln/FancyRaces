/**
 * \file
 * This file contains implementation of commItem, ServerThread, ServerSocketThread and UserSocket classes.
**/

#include "Sserver.h"
#include <sstream>

using namespace std;
using namespace irr;

/**
 * Contains informations (nick, communication socket handle etc.) about connected player
**/
class UserSocket {
public:
	/**
	 * just initializes class to to default values
	**/
	UserSocket(Socket *s, int attr, std::string nick):
		ac_(s), attr_(attr), nick_(nick), l_ready_(false), carprofile_("test"), simid_(-1), connType(NOTHING), points_(0)
		{}

//global attributes
	unsigned int attr_; //!< user attributes
	std::string nick_; //!< user nick
	AsyncComm ac_; //!< handle to user's socket
	std::string carprofile_; //!< car profile path

//game:connect
	enum {
		NOTHING,
		PLAYER,
		SPECTATOR,
	} connType; //!< specifies, of which kind is the connected user

//game:loading
	bool l_ready_; //!< specifies, whether user loaded all the data required to show current scene, and is ready to go
//game:play
	int simid_; //!< internal id of the user inside of the simulation (used only if connType==PLAYER)
	int points_; //!< number of points gained during game

	/**
	 * stub for ac_.SendString using the connect variable
	**/
	void SendString(const string &str) {
		if (ac_.Alive()) ac_.SendString(str);
	}

	/**
	 * stub for ac_.ReadString using the alive status
	**/
	bool ReadString(string &str) {
		if (ac_.Alive()) return ac_.ReadString(str);
		return false;
	}

	/**
	 * Returns true, if the socket belongs to player and he's connected
	**/
	bool IsPlayer() {return ac_.Alive() && connType==PLAYER;}

	/**
	 * Returns true if IsPlayer() is true and user has assigned a vehicle in simulation
	**/
	bool IsPlayer2() {return IsPlayer() && simid_!=-1;}

	/**
	 * Returns true, if the socket belongs to a spectator and he's connected
	**/
	bool IsSpectator() {return ac_.Alive() && connType==SPECTATOR;}
};

/**
 * Class for the thread, that Does al\l the communication with clients, accepts new connections and so on. It works in cycle, and everytime a message comes from a client, the function puts it to the commSockSrvQueue queue. If server put some message for client(s) to the commSrvSockQueue, it sends it to them...
**/
class ServerSocketThread: public OpenThreads::Thread {
public:
	/**
	* Constructor for the server class; starts the server on the port 'port'. The rest of the arguments isn't used.
	**/
	ServerSocketThread(ServerThread *srv): OpenThreads::Thread(), st(srv) {}
	virtual ~ServerSocketThread() {}
	virtual void quit() {stop=true; while (isRunning()) YieldCurrentThread();}
private:
	virtual void run();

	ServerThread *st;
	bool stop;
};

void ServerSocketThread::run()
{
	UserSocket *us;
	string str;

	DBGCOUT("ServerSocketThread", "run", "start");
	stop=false;
	SocketServer in(st->port, 5, NonBlockingSocket);

	while (!stop) {
		//check for new messages from server
		st->dataMutex.lock();

		while (!st->commSrvSockQueue.empty()) { //do all the communication
			commItem m=st->commSrvSockQueue.front();
			st->commSrvSockQueue.pop();

			for (socket_list::iterator os=st->srv_clients.begin(); os!=st->srv_clients.end(); os++) {
				if (m.us==0 || m.us==*os) { //m.us==0 => broadcast; otherwise send to specified socket
					//TODO: only send position etc. broadcasts to connected users
					if (!(m.attr & commItem::TTL) ||
						((m.attr & commItem::TTL) && (st->GetDevice()!=0) && (m.ttl>=st->GetDevice()->getTimer()->getRealTime()))
						) {
							(*os)->SendString(m.msg);
					}
				}
			}
		}

		//accept new connections
		Socket* s=in.Accept();
		if (s) {
			DBGCOUT("ServerSocketThread", "run", "accept connection");
			us=new UserSocket(s, 0, "");
			st->srv_clients.push_back(us);
		}

		//dispatch connections
		for (socket_list::iterator os=st->srv_clients.begin(); os!=st->srv_clients.end(); os++) {
			if (!(*os)->ac_.Alive() && !((*os)->attr_&USER_BOT) && (*os)->connType==UserSocket::PLAYER) {
				st->commSockSrvQueue.push(commItem((*os), "%:"AC_DISCONNECT"\nNICK:"+(*os)->nick_));
				(*os)->connType=UserSocket::NOTHING;
				continue;
			}

			while ((*os)->ReadString(str)) {
				st->commSockSrvQueue.push(commItem((*os), str));
			}
		}
		st->dataMutex.unlock();

		YieldCurrentThread();
	}

	DBGCOUT("ServerSocketThread", "run", "close all");
	//Close all connections
	while (!st->srv_clients.empty()) {
		us=st->srv_clients.front();
		st->srv_clients.pop_front();
		us->SendString("%:"AC_DISCONNECT);
		delete us;
	}

	stop=false;
}

enum {
	STATE_CONFIG,
	STATE_LOAD,
	STATE_START,
	STATE_GAMEPLAY,
	STATE_GAMEPAUSE,
	STATE_RESULTS,
};

string ServerThread::GetUserlist()
{
	string usrs="%:"AC_USERS"\n";
	int b=0;
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if ((*os)->connType==UserSocket::PLAYER) {
			usrs+=(*os)->nick_ + ":" + (*os)->carprofile_ + "\n";
		} else if ((*os)->connType==UserSocket::SPECTATOR) {
			usrs+=(*os)->nick_ + " (spectator)\n";
		} else if ((*os)->attr_&USER_BOT) {
			usrs+=(string)"BOT"+ numtostring(++b)+":"+ (*os)->carprofile_ + "\n";
		}
	}
	return usrs;
}

void ServerThread::BroadcastPlayersInfo(UserSocket *target, bool lock)
{
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if ((*os)->simid_==-1) continue; //skip spectators

		SVehicle *av;
		ss->GetPlayerInfo((*os)->simid_, &av);
		const dReal* pos, *quat;
		core::vector3df rot;
		char buf[1000];
		int i;
		string s;

		if ((*os)->attr_&USER_BOT)
			Bot_Process(*os, av);

		if (lock) dataMutex.lock();

			cmd_map cm;
			cm["%"]=AC_PLAYER;
			cm["ID"]=numtostring((*os)->simid_);
			cm["WHAT"]="POSROT";
			cm["TIME"]=numtostring(ss->GetSimTime());

			pos=av->chassis->ode_body->getPosition();
			quat=av->chassis->ode_body->getQuaternion();
			sprintf(buf, "%7.5g %7.5g %7.5g %7.5g %7.5g %7.5g %7.5g", pos[0], pos[1], pos[2], quat[0], quat[1], quat[2], quat[3]);
			cm["BODY"]=buf;	

			for (i=0; i<(signed)av->wheels.size(); i++) {
				pos=av->wheels[i].ode_body->getPosition();
				quat=av->wheels[i].ode_body->getQuaternion();

				sprintf(buf, "%7.5g %7.5g %7.5g %7.5g %7.5g %7.5g %7.5g", pos[0], pos[1], pos[2], quat[0], quat[1], quat[2], quat[3]);
				cm["WHEEL"+numtostring(i)]=buf;
			}

			cmd_Map2String(cm, s);
			//cerr << s << endl;
			commSrvSockQueue.push(commItem(target, s, commItem::TTL, device->getTimer()->getRealTime()+posInfoMaxDelay));
			cm.clear();

			cm.clear();
			cm["%"]=AC_PLAYER;
			cm["ID"]=numtostring((*os)->simid_);
			cm["WHAT"]="CONTROL";
			cm["TIME"]=numtostring(ss->GetSimTime());
			cm["STEER"]=numtostring(av->c_steer, "%5.2g");
			cm["ACCEL"]=numtostring(av->c_accel, "%5.2g");
			cm["BRAKE"]=numtostring(av->c_brakes, "%5.2g");
			cm["HANDBRAKE"]=numtostring(av->c_handbrake, "%5.2g");
			cm["GEAR"]=numtostring(av->c_gear);
			cm["RESCUE"]=numtostring(av->c_rescue, "%5.2g");
			cm["CLUTCH"]=numtostring(av->c_clutch, "%5.2g");
			cm["RPM"]=numtostring(av->GetRPM());
			cm["SPEED"]=numtostring(av->GetSpeed());
			cmd_Map2String(cm, s);
			commSrvSockQueue.push(commItem(target, s, commItem::TTL, device->getTimer()->getRealTime()+posInfoMaxDelay));
		
		if (lock) dataMutex.unlock();
	}
}

void ServerThread::BroadcastObjectsInfo(UserSocket *target)
{
	if (ss->GetMoveableObjects().size()==0) return;

	int k=0;
	char buf[1000];
	cmd_map cm;
	string s;
	const dReal* pos, *quat;
	cm["%"]=AC_OBJECTS;
	cm["TIME"]=numtostring(ss->GetSimTime());

	dataMutex.lock();

	for (std::deque<const SObject*>::iterator i=ss->GetMoveableObjects().begin(); i!=ss->GetMoveableObjects().end(); i++) {
		pos=(*i)->ode_body->getPosition();
		quat=(*i)->ode_body->getQuaternion();
		if ((*i)->ode_body->isEnabled()) {
			//cerr << k << " ";
			sprintf(buf, "%7.5g %7.5g %7.5g %7.5g %7.5g %7.5g %7.5g", pos[0], pos[1], pos[2], quat[0], quat[1], quat[2], quat[3]);
			cm[numtostring(k)]=buf;
		}
		k++;
	}
	cmd_Map2String(cm, s);
	commSrvSockQueue.push(commItem(target, s, commItem::TTL, device->getTimer()->getRealTime()+posInfoMaxDelay));
	dataMutex.unlock();
}

void ServerThread::BroadcastSimStatus(int &serverState)
{
	string str;
	int c=0;
	int f=0;
	bool endrace=false;

	dataMutex.lock();

	cmd_map cmd;

	cmd["%"]=AC_SIMSTATUS;
	cmd["TIME"]=numtostring(ss->GetSimTime());
	//...new checkpoint-crossings
	while (!ss->events.empty()) {
		if (ss->events.front().type==SSimulation_event::CROSSED_CHECKPOINT) {
			cmd["CHECKPOINT"+numtostring(c++)]=numtostring(ss->events.front().crossCheckpoint.vehicleId)+" "+numtostring(ss->events.front().crossCheckpoint.checkpoint);
		} else if (ss->events.front().type==SSimulation_event::CROSSED_FINISH) {
			cmd["FINISH"+numtostring(f++)]=numtostring(ss->events.front().crossFinish.vehicleId)+" "+numtostring(ss->events.front().crossFinish.laps);
		} else if (ss->events.front().type==SSimulation_event::RACE_FINISHED)
			endrace=true;
		ss->events.pop_front();
	}
	cmd["NCHECKPOINT"]=numtostring(c);
	cmd["NFINISH"]=numtostring(f);
	
	cmd_Map2String(cmd, str);
	commSrvSockQueue.push(commItem(0, str));

	if (endrace) {
		cmd.clear();

		//switch to results state
		serverState=STATE_RESULTS;
		string res;
		deque<pair<string, int> > order;
		int k=0;

		int noplayers=0;
		for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) if ((*os)->IsPlayer2()) noplayers++;

		for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
			SVehicle *av;
			ss->GetPlayerInfo((*os)->simid_, &av);
			(*os)->points_+=noplayers-av->finishorder;
			order.push_back(make_pair((*os)->nick_, (*os)->points_));
		}
		
		//insertion sort :-)
		while (!order.empty()) {
			//find the lowest value and pop
			deque<pair<string, int> >::iterator f=order.begin();
			for (deque<pair<string, int> >::iterator i=order.begin()+1; i!=order.end(); i++) {
				if (i->second>f->second) f=i;
			}

			cmd["RESULTS"+numtostring(k++)]=f->first+" ("+numtostring(f->second)+" pt"+(f->second>1?"s":"")+")";
			order.erase(f);
		}
		cmd["%"]=AC_FINISHED;
		cmd_Map2String(cmd, str);

		commSrvSockQueue.push(commItem(0, str)); //this message is sent to everyone, always
	}
	dataMutex.unlock();
}

bool ServerThread::FixUserConfig(UserSocket *us)
{
	bool r, r2=false, rv=false;

	if (us->nick_=="") us->nick_="X";
	do {
		r=false;
		for (socket_list::iterator i=srv_clients.begin(); i!=srv_clients.end(); i++) {
			if (*i==us) continue;
			if (us->nick_==(*i)->nick_) {
				r=true;
				r2=true;
				us->nick_+=numtostring(rand()%10);
			}
		}
	} while (r);
	if (r2) {
		commSrvSockQueue.push(commItem(us, (string)"%:"AC_NICK"\nDATA:" + us->nick_));
		rv=true;
	}

	return rv;
}

int ServerThread::GetMapSlots(std::string map)
{
	CfgFile cfg(device->getFileSystem(), "data/maps/"+map+"/config.ini");
	int i;
	for (i=1;; i++) {
		if (!cfg.Getval_exists("Startingplaces/Start_"+numtostring(i)+"/pos")) break;
	}
	DBGCOUT("ServerThread", "GetMapSlots", numtostring(i-1));
	return i-1;
}

int ServerThread::GetOccupiedSlots()
{
	int rv=0;
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if (((*os)->IsPlayer()) || ((*os)->attr_&USER_BOT)) rv++;
	}
	return rv;
}

void ServerThread::run()
{
	serverState=STATE_CONFIG;
	u32 btime=0;
	int i;
	
	DBGCOUT("ServerThread", "run()", "started...");

//INIT
	stop=false;
	socketThread=new ServerSocketThread(this);
	socketThread->start();

	device=irr::createDevice(irr::video::EDT_NULL);
	DeviceFS_AddData(device);
	//create car + map list
	irr::io::IFileSystem *ifs=device->getFileSystem();
	irr::core::string<irr::c8> cwd=ifs->getWorkingDirectory();
	ifs->changeWorkingDirectoryTo((cwd+"/data/maps").c_str());
	irr::io::IFileList *ifl=ifs->createFileList();
	for (i=0; i<ifl->getFileCount(); i++) {
		if (ifl->isDirectory(i) && (ifl->getFileName(i)[0] != '.')) {
			list_maps.push_back(ifl->getFileName(i).c_str());
		}
	}
	ifl->drop();
	
	ifs->changeWorkingDirectoryTo((cwd+"/data/cars").c_str());
	ifl=ifs->createFileList();
	for (i=0; i<ifl->getFileCount(); i++) {
		if (ifl->isDirectory(i) && (ifl->getFileName(i)[0] != '.')) {
			list_cars.push_back(ifl->getFileName(i).c_str());
		}
	}
	ifl->drop();

	ifs->changeWorkingDirectoryTo(cwd.c_str());
	cur_map=list_maps[0];
	cur_map_maxSlots=GetMapSlots(cur_map);

	DBGCOUT("ServerThread", "STATE", "config");
//MAIN LOOP
	while (!stop) {
		dataMutex.lock();

			while (!commSockSrvQueue.empty()) {
				commItem s=commSockSrvQueue.front();
				commSockSrvQueue.pop();
				cmd_map cmd;
				cmd_String2Map(s.msg, cmd);
				
				if (cmd["%"]==AC_DISCONNECT) { //this is a special message
					if (serverState==STATE_CONFIG) {
						commSrvSockQueue.push(commItem(0, GetUserlist()));
						commSrvSockQueue.push(commItem(0, "%:"AC_CHAT"\nDATA:server: " + cmd["NICK"] + " left"));
					} else {
						//if the game already started, remove player from game (not from simulation; see the RemoveFromRace for explanation)
						if (ss) {
							ss->RemoveFromRace(s.us->simid_);
						}
					}
				} else
				switch (serverState) {
				case STATE_CONFIG:
					{
						if (s.us->connType==UserSocket::NOTHING) {
							if (cmd["%"]==AC_JOIN) {
								if (GetOccupiedSlots()>=cur_map_maxSlots) goto MAKE_SPECTATOR;

								s.us->connType=UserSocket::PLAYER;
								s.us->nick_=cmd["DATA"];
								s.us->carprofile_=list_cars[0];
								commSrvSockQueue.push(commItem(s.us, "%:"AC_JOIN));
								FixUserConfig(s.us);
								commSrvSockQueue.push(commItem(0, "%:"AC_CHAT"\nDATA:server: " + s.us->nick_ + " joined us"));

								//send the new user list to all
								commSrvSockQueue.push(commItem(0, GetUserlist()));
								//DBGCOUT("ServerThread", "run", GetUserlist());

								//send car list
								string str;
								str="%:"AC_LIST_CARS"\n";
								for (deque<string>::iterator li=list_cars.begin(); li!=list_cars.end(); li++) {
									str+=*li+"\n";
								}
								commSrvSockQueue.push(commItem(s.us, str));

								commSrvSockQueue.push(commItem(s.us, "%:"AC_CAR"\nDATA:"+s.us->carprofile_));
								commSrvSockQueue.push(commItem(s.us, "%:"AC_MAP"\nDATA:"+cur_map));
							} else if (cmd["%"]==AC_SPECTATE) {
MAKE_SPECTATOR:
								s.us->connType=UserSocket::SPECTATOR;
								s.us->nick_=cmd["DATA"];
								commSrvSockQueue.push(commItem(s.us, "%:"AC_SPECTATE));
								FixUserConfig(s.us);
								commSrvSockQueue.push(commItem(0, GetUserlist()));
								commSrvSockQueue.push(commItem(s.us, "%:"AC_MAP"\nDATA:"+cur_map));
								
								/*if (serverState==STATE_GAMEPLAY) {
									//dataMutex.lock();
									commSrvSockQueue.push(commItem(s.us, "%:"AC_STARTGAME""));
									SendPlayerInitInfo(s.us);
									BroadcastPlayersInfo(s.us, false);
									commSrvSockQueue.push(commItem(s.us, "%:"AC_WAITING""));
									//dataMutex.unlock();
								}*/
							} else s.us->ac_.disconnect();
						}
						if (cmd["%"]==AC_ADMIN) {
							bool aa=false;
							socket_list::iterator os;
							for (os=srv_clients.begin(); os!=srv_clients.end(); os++)
								if ((*os)->attr_&USER_ADMIN) {
									aa=true;
									break;
								}
							if (!aa) {
								s.us->attr_|=USER_ADMIN; //set admin
								commSrvSockQueue.push(commItem(s.us, (string)"%:"AC_ADMIN""));

								//send available map list to the admin
								string str;
								str="%:"AC_LIST_MAPS"\n";
								for (deque<string>::iterator li=list_maps.begin(); li!=list_maps.end(); li++) {
									str+=*li+"\n";
								}
								commSrvSockQueue.push(commItem(s.us, str));
							} else {
								commSrvSockQueue.push(commItem(s.us, "%:"AC_CHAT"\nDATA:server: there already is an admin ("+(*os)->nick_+")!"));
							}
						} else if (cmd["%"]==AC_NICK) {
							//DBGCOUT("ServerThread", "Config", (string)"NickQ " + cmd["DATA"]);
							//don't allow 2 same nicks
							s.us->nick_=cmd["DATA"];
							if (!FixUserConfig(s.us))							
								commSrvSockQueue.push(commItem(s.us, (string)"%:"AC_NICK"\nDATA:" + s.us->nick_));

							commSrvSockQueue.push(commItem(0, GetUserlist()));
						} else if (cmd["%"]==AC_CAR) {
							for (std::deque<std::string>::iterator i=list_cars.begin(); i!=list_cars.end(); i++) {
								if (*i==cmd["DATA"]) {
									s.us->carprofile_=cmd["DATA"];
									commSrvSockQueue.push(commItem(s.us, (string)"%:"AC_CAR"\nDATA:" + s.us->carprofile_));
									commSrvSockQueue.push(commItem(0, GetUserlist()));
								}
							}
						}

						if (s.us->attr_&USER_ADMIN) {
							if (cmd["%"]==AC_STARTGAME) { //start game
								StartSim();
							} else if (cmd["%"]==AC_BOT_ADD) {
								if (GetOccupiedSlots()<cur_map_maxSlots) {
									UserSocket *us=new UserSocket(0, USER_BOT, "bot");
									us->carprofile_=list_cars[rand()%list_cars.size()];
									srv_clients.push_back(us);
									commSrvSockQueue.push(commItem(0, "%:"AC_CHAT"\nDATA:server: bot added"));
									commSrvSockQueue.push(commItem(0, GetUserlist()));
								}
							} else if (cmd["%"]==AC_BOT_RM) {
								for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
									if ((*os)->attr_&USER_BOT) {
										commSrvSockQueue.push(commItem(0, "%:"AC_CHAT"\nDATA:server: bot removed"));
										srv_clients.erase(os);
										commSrvSockQueue.push(commItem(0, GetUserlist()));
										break;
									}
								}
							} else if (cmd["%"]==AC_MAP) {
								for (std::deque<std::string>::iterator i=list_maps.begin(); i!=list_maps.end(); i++) {
									if (*i==cmd["DATA"]) {
										cur_map=cmd["DATA"];
										commSrvSockQueue.push(commItem(0, (string)"%:"AC_MAP"\nDATA:" + cur_map));
									}
								}
								cur_map_maxSlots=GetMapSlots(cur_map);
							} else if (cmd["%"]==AC_GAMESTYLE) {
								gameStyle=atol(cmd["STYLE"].c_str());
								noLaps=atol(cmd["LAPS"].c_str());
							}
						}
						if (cmd["%"]==AC_CHAT) {
							DBGCOUT("ServerThread", "chat", s.us->nick_ + ": " + cmd["DATA"]);
							commSrvSockQueue.push(commItem(0, "%:"AC_CHAT"\nDATA:" + s.us->nick_ + ": " + cmd["DATA"]));
						}
					}
					break;
				case STATE_START:
					{
						if (cmd["%"]==AC_READY) {
							DBGCOUT("ServerThread", "ready", s.us->nick_);
							s.us->l_ready_=true;
						}
						bool x=true;
						for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
							if (!((*os)->l_ready_) && ((*os)->IsPlayer())) {
								x=false;
								break;
							}
						}
						if (x==true) { //all clients are ready
							//commSrvSockQueue.push(commItem(0, "%:"AC_START"\nDATA:" + s.us->nick_ + "\n" + cmd["DATA"]));
							serverState=STATE_GAMEPLAY;
							ss->Start();
							DBGCOUT("ServerThread", "STATE", "gameplay");
						}
						//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);
					}
					break;
				case STATE_GAMEPLAY:
					{
						if (cmd["%"]==AC_CONTROL) {
							SVehicle *av;
							ss->GetPlayerInfo(s.us->simid_, &av);
							av->c_steer=atof(cmd["STEER"].c_str());
							av->c_accel=atof(cmd["ACCEL"].c_str());
							av->c_brakes=atof(cmd["BRAKE"].c_str());
							av->c_handbrake=atof(cmd["HANDBRAKE"].c_str());
							av->c_gear=atol(cmd["GEAR"].c_str());
							av->c_clutch=atof(cmd["CLUTCH"].c_str());
							av->c_rescue=atof(cmd["RESCUE"].c_str());
						} else if (cmd["%"]=="PING") {
							istringstream is(cmd["DATA"].c_str());
							u32 ping;
							is >> ping;

							//if ((GetTickCount()-ping) > 500) {
							//	commSrvSockQueue.empty();
							//	commSockSrvQueue.empty();
							//}

							commSrvSockQueue.push(commItem(s.us, (string)"%:"AC_PONG"\nDATA:"+cmd["DATA"]));
						} else if (cmd["%"]==AC_READY && s.us->connType==UserSocket::SPECTATOR) {
							s.us->l_ready_=true;
						} else if (cmd["%"]==AC_RESTART) {
							if (s.us->attr_&USER_ADMIN) {
								//ok, now we really have to restart... 
								commSrvSockQueue.push(commItem(0, "%:"AC_RESTART));
								CleanupSim();
								CreateSim();
								BroadcastPlayersInfo(0, false);
								commSrvSockQueue.push(commItem(0, "%:"AC_WAITING""));
							}
						}
					}
					break;
				case STATE_GAMEPAUSE:
					{
						DBGCOUT("ServerThread", "STATE", "gamepause");
						commSrvSockQueue.push(commItem(0, AC_RESETAUTOMOVE));
					}
					break;
				case STATE_RESULTS:
					if (s.us->attr_&USER_ADMIN) {
							if (cmd["%"]==AC_STARTGAME) { //start game
								if (gameStyle==0) {
									stop=true;
								} else {
									if (gameStyle==2) { //kick the last player, reset points of other players
										if (GetNoPlayers()<=2) { //there's nothing more to be won :)
											stop=true;
											break;
										} else {
											socket_list::iterator ku, os;
											for (os=srv_clients.begin(); os!=srv_clients.end(); os++) {
												if ((*os)->IsPlayer2()) ku=os;
											}
											for (; os!=srv_clients.end(); os++) {
												if ((*os)->IsPlayer2()) if ((*os)->points_<(*ku)->points_) ku=os;
											}
											(*ku)->connType=UserSocket::SPECTATOR;
											(*ku)->simid_=-1;
											commSrvSockQueue.push(commItem((*ku), "%:"AC_SPECTATE));
										}
									}
									StartSim();
								}
							}
					}
					break;
				}
			}
		dataMutex.unlock();

		//Main server loop
		if (serverState==STATE_GAMEPLAY) {
			if (ss->Step()) {
				if ((device->getTimer()->getTime()-btime)>10) { //broadcast positions etc. max. once in 10 ms
					btime=device->getTimer()->getTime();
					BroadcastPlayersInfo();
					BroadcastObjectsInfo();
					BroadcastSimStatus(serverState);
				}
			}
		} else if (serverState==STATE_GAMEPAUSE) {
		} else if (serverState==STATE_LOAD) {
			DBGCOUT("ServerThread", "STATE", "Load");
			CleanupSim(); //if there was some simulation before
			CreateSim();

			//Let clients know we can start
			dataMutex.lock();

				SendPlayerInitInfo(0); //broadcasts information to all connected players

				serverState=STATE_START;

			dataMutex.unlock();
			
			BroadcastPlayersInfo(); //send initial info about players
			//the end of init info
			commSrvSockQueue.push(commItem(0, "%:"AC_WAITING""));

			DBGCOUT("ServerThread", "run", "SERVERSTATE: start");
		}

		YieldCurrentThread();
	}

//CLEANUP
	socketThread->quit();
	CleanupSim();

	stop=false;
	DBGCOUT("ServerThread", "run()", "quitted...");
}

void ServerThread::CreateSim()
{
	ss=new SSimulation(device);

	ss->SetWorld(cur_map);
	ss->SetLaps(noLaps);

	int i=1;
	dVector3 pos;
	CfgFile *ccfg=ss->getCfgFile();
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if ((*os)->connType!=UserSocket::PLAYER && !((*os)->attr_&USER_BOT))
			continue;

		char buf[100];
		irr::core::vector3df posi;
		sprintf(buf, "Startingplaces/Start_%d/pos", i);
		//TODO: fix if there are less places than players
		posi=ccfg->Getval_vector(buf);
		vector_convert(posi, pos);
		(*os)->simid_=ss->AddPlayer((*os)->carprofile_, pos);
		i++;
	}

	//mark all bots ready
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		Bot_Init(*os);
	}
}

int ServerThread::GetNoPlayers()
{
	int rv=0;
	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if ((*os)->IsPlayer2()) rv++;
	}
	return rv;
}

void ServerThread::CleanupSim()
{
	if (ss) {
		delete ss;
		ss=0;
	}
}

void ServerThread::StartSim()
{
	DBGCOUT("ServerThread", "start game", "");
	commSrvSockQueue.push(commItem(0, "%:"AC_STARTGAME""));

	commSrvSockQueue.push(commItem(0, "%:"AC_SERVERINIT""));
	serverState=STATE_LOAD;
}

void ServerThread::quit()
{
	if (!isRunning()) return;

	stop=true;
	while (this->isRunning()) YieldCurrentThread();
}

void ServerThread::Bot_Process(UserSocket *us, SVehicle *av)
{
	av->c_accel=0.2;
	av->c_steer+=double(rand()%100-49)/1000.0;
	if (av->c_steer>1) av->c_steer=1;
	if (av->c_steer<-1) av->c_steer=-1;
}

void ServerThread::Bot_Init(UserSocket *us)
{
	SVehicle *av;
	if (us->simid_==-1) return; //these are the disconnected users
	ss->GetPlayerInfo(us->simid_, &av);
	av->c_accel=0;
	av->c_steer=0;
	av->c_brakes=0;
	av->c_gear=av->gearbox_N;
	av->c_handbrake=1;
	if (us->attr_&USER_BOT) {
		us->l_ready_=true;
	}
}

void ServerThread::SendPlayerInitInfo(UserSocket *tgt)
{
	//Inform clients to start the game...
	commSrvSockQueue.push(commItem(tgt, "%:"AC_GETREADY""));

	//send basic informations to clients (world name, other resources needed...)
	commSrvSockQueue.push(commItem(tgt, "%:"AC_WORLD"\nDATA:" + cur_map));

	for (socket_list::iterator os=srv_clients.begin(); os!=srv_clients.end(); os++) {
		if ((*os)->simid_==-1) continue;
		string buf;

		buf="%:"AC_CAR"\n"
			"ID:"+numtostring((*os)->simid_)+"\n"+
			"PROFILE:"+(*os)->carprofile_.c_str()+"\n"+
			"NICK:"+(*os)->nick_.c_str()+"\n";
//		cerr << buf << endl;
		commSrvSockQueue.push(commItem(tgt, buf));

		//yourid is needed just for real players, not for spectators anyways, so we don't send them to users
		if (!tgt) {
			commSrvSockQueue.push(commItem(*os, "%:"AC_YOURID"\nDATA:"+numtostring((*os)->simid_)));
		}
	}
}

