/**
 * \file
 * This file contains implementation of CMainMenu class.
**/

#include "Sserver.h"
#include "Autil.h"
#include <iostream>
#include <string>
#include "CMainMenu.h"
#include "main.h"
#include <openalpp/alpp.h>
#include <deque>
#include "CGame.h"

using namespace std;

CMainMenu::CMainMenu(AGameSettings *_ags):currentScreen(NONE),ags(_ags),serverThread(0),adminMode(false),spectate(false)
{
}

CMainMenu::~CMainMenu()
{
	ServerStop();
}

void CMainMenu::GainControl()
{
	gui::IGUIEnvironment* guienv = ags->device->getGUIEnvironment();
	scene::ISceneManager* smgr = ags->device->getSceneManager();
	video::IVideoDriver* driver = ags->device->getVideoDriver();

	ags->device->setEventReceiver(this);

	//initialize the environment (possibly changed by game)
	gui::IGUISkin* newskin = guienv->createSkin(gui::EGST_WINDOWS_METALLIC);
    guienv->setSkin(newskin);
    newskin->drop();
	ags->device->getCursorControl()->setVisible(true);
	smgr->clear();
	ags->device->setWindowCaption(L"Anorasi");

	// load font
	gui::IGUIFont* font = guienv->getFont("data/misc/fontarial.bmp");
	if (font) guienv->getSkin()->setFont(font);

	// add images
	gui::IGUIImage* img = guienv->addImage(core::rect<int>(0,0,640,480), 0, 50000);
	img->setImage(driver->getTexture("data/misc/menu.jpg"));
}

int CMainMenu::run()
{
	terminate=0;
	ags->device->setEventReceiver(this);
	video::IVideoDriver* driver = ags->device->getVideoDriver();
	scene::ISceneManager* smgr = ags->device->getSceneManager();
	gui::IGUIEnvironment* guienv = ags->device->getGUIEnvironment();

	GainControl();
	SwitchScreen(MAIN);

	// draw all
	while (ags->device->run() && terminate==0) {
		driver->beginScene(false, true, video::SColor(0,0,0,0));
		guienv->drawAll();
		smgr->drawAll();	
		driver->endScene();
		ProcessSocketMessages();
		OpenThreads::Thread::YieldCurrentThread();
	}

	const core::list<gui::IGUIElement*> &che=guienv->getRootGUIElement()->getChildren();
	while (!che.empty()) {
		guienv->getRootGUIElement()->removeChild(*che.getLast());
	}

	return terminate;
}

bool CMainMenu::OnEvent(const SEvent &event)
{
	switch (currentScreen) {
	case MAIN:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_KEY_C)
				goto MAIN_CREATE;
			else if (event.KeyInput.Key==KEY_KEY_J)
				goto MAIN_JOIN;
			else if (event.KeyInput.Key==KEY_KEY_S)
				goto MAIN_SETTINGS;
			else if (event.KeyInput.Key==KEY_KEY_Q || event.KeyInput.Key==KEY_ESCAPE)
				goto MAIN_QUIT;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case MAIN+1:
MAIN_CREATE:
				SwitchScreen(CREATE);
				break;
			case MAIN+2:
MAIN_JOIN:
				SwitchScreen(JOIN);
				break;
			case MAIN+3:
MAIN_SETTINGS:
				SwitchScreen(SETTINGS);
				break;
			case MAIN+4:
MAIN_QUIT:
				terminate=-1;
				break;
			}
		}
		break;
	case SETTINGS:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto SETTINGS_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto SETTINGS_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case SETTINGS+5:
SETTINGS_OK:
				//get settings
				ags->fullscreen=((gui::IGUICheckBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SETTINGS+3, true))->isChecked();
				ags->vsync=((gui::IGUICheckBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SETTINGS+4, true))->isChecked();
				ags->driver=(irr::video::E_DRIVER_TYPE)(((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SETTINGS+2, true))->getSelected()+1);
//#ifndef WIN32
//				if (ags->driver==video::E_DRIVER_TYPE::EDT_DIRECT3D8 || ags->driver==video::E_DRIVER_TYPE::EDT_DIRECT3D9)
//					ags->driver==video::E_DRIVER_TYPE::EDT_OPENGL;
//#endif
				SwitchScreen(MAIN);
				break;
			case SETTINGS+6:
SETTINGS_CANCEL:
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	case CREATE:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto CREATE_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto CREATE_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case CREATE+5:
CREATE_OK:
				{
				const wchar_t* sport=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(CREATE+2, true)->getText();
				int port;
				swscanf(sport, L"%d", &port);

				ServerStart(port);

				connectHost="localhost";
				connectPort=port;
				ags->srv_port=connectPort;
				spectate=((gui::IGUICheckBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(CREATE+3, true))->isChecked();
				adminMode=true;
				}
				SwitchScreen(CONNECT);
				break;
			case CREATE+6:
CREATE_CANCEL:
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	case JOIN:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto JOIN_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto JOIN_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case JOIN+5:
JOIN_OK:
				{
				const wchar_t* saddrw=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(JOIN+1, true)->getText();
				char *saddr=u2a(saddrw);
				const wchar_t* sport=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(JOIN+2, true)->getText();
				int port;
				swscanf(sport, L"%d", &port);

				connectHost=saddr;
				connectPort=port;
				ags->cli_port=connectPort;
				ags->cli_str==connectHost;
				spectate=((gui::IGUICheckBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(JOIN+3, true))->isChecked();
				delete saddr;
				adminMode=false;
				}
				SwitchScreen(CONNECT);
				break;
			case JOIN+6:
JOIN_CANCEL:
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	case CANTCONNECT:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto CANTCONNECT_OK;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case CANTCONNECT+1:
CANTCONNECT_OK:
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	case CONNECTION_LOST:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto CONNECTION_LOST_OK;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case CONNECTION_LOST+1:
CONNECTION_LOST_OK:
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	case PREGAME:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN) {
				if (ags->device->getGUIEnvironment()->hasFocus(ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+2, true))) {
					const wchar_t* swtext=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+2, true)->getText();
					char *stext=u2a(swtext);

					ags->connection->SendString((std::string)"%:"AC_NICK"\nDATA:" + stext);

					delete stext;
				} else if (ags->device->getGUIEnvironment()->hasFocus(ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+4, true))) {
					const wchar_t* swtext=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+4, true)->getText();
					char *stext=u2a(swtext);

					if (stext[0]!='!')
						ags->connection->SendString((std::string)"%:"AC_CHAT"\nDATA:" + stext);
					else {
						for (int i=0; i<(signed)strlen(stext); i++) if (stext[i]=='|') stext[i]='\n';
						ags->connection->SendString((std::string)&stext[1]); 
					}

					delete stext;
					ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+4, true)->setText(L"");
				} else goto PREGAME_PLAY;
			} else if (event.KeyInput.Key==KEY_ESCAPE)
				goto PREGAME_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case PREGAME+9+100:
PREGAME_PLAY:
				ags->connection->SendString("%:"AC_GAMESTYLE"\nSTYLE:"+numtostring(ags->game_style)+"\nLAPS:"+numtostring(ags->no_laps));
				ServerSendMessage(AC_STARTGAME);
				break;
			case PREGAME+10:
PREGAME_CANCEL:
				ServerStop();
				SwitchScreen(MAIN);
				break;
			case PREGAME+5: //car
				SwitchScreen(SELECT_CAR);
				break;
			case PREGAME+6+100: //map
				SwitchScreen(SELECT_MAP);
				break;
			case PREGAME+7+100: //+bot
				if (ags->admin) ags->connection->SendString("%:"AC_BOT_ADD);
				break;
			case PREGAME+8+100: //+bot
				if (ags->admin) ags->connection->SendString("%:"AC_BOT_RM);
				break;
			case PREGAME+10+100:
				SwitchScreen(SELECT_STYLE);
				break;
			}
		}
		break;
	case SELECT_CAR:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto SELECT_CAR_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto SELECT_CAR_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case SELECT_CAR+2:
SELECT_CAR_OK:
				{
				const wchar_t *ws=((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_CAR+1, true))->getListItem(
					((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_CAR+1, true))->getSelected()
					);
				char *as=u2a(ws);
				ags->connection->SendString((std::string)"%:"AC_CAR"\nDATA:" + as);
				delete as;
				}
				SwitchScreen(PREGAME);
				break;
			case SELECT_CAR+3:
SELECT_CAR_CANCEL:
				SwitchScreen(PREGAME);
				break;
			}
		}
		break;
	case SELECT_MAP:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto SELECT_MAP_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto SELECT_MAP_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case SELECT_MAP+2:
SELECT_MAP_OK:
				{
				const wchar_t *ws=((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_MAP+1, true))->getListItem(
					((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_MAP+1, true))->getSelected()
					);
				char *as=u2a(ws);
				ags->connection->SendString((std::string)"%:"AC_MAP"\nDATA:" + as);
				delete as;
				}
				SwitchScreen(PREGAME);
				break;
			case SELECT_MAP+3:
SELECT_MAP_CANCEL:
				SwitchScreen(PREGAME);
				break;
			}
		}
		break;
	case SELECT_STYLE:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto SELECT_STYLE_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto SELECT_STYLE_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case SELECT_STYLE+2:
SELECT_STYLE_OK:
				{
				const wchar_t* ws=ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_STYLE+4, true)->getText();
				char *as=u2a(ws);
				ags->no_laps=atol(as);
				delete as;
				ags->game_style=((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(SELECT_STYLE+1, true))->getSelected();
				if (ags->no_laps<1) ags->no_laps=1;
				if (ags->game_style<0 || ags->game_style>2) ags->game_style=0;
				}
				SwitchScreen(PREGAME);
				break;
			case SELECT_STYLE+3:
SELECT_STYLE_CANCEL:
				SwitchScreen(PREGAME);
				break;
			}
		}
		break;
	case RESULTS:
		if ((event.EventType==EET_KEY_INPUT_EVENT)&&(event.KeyInput.PressedDown==false)) {
			if (event.KeyInput.Key==KEY_RETURN)
				goto RESULTS_OK;
			else if (event.KeyInput.Key==KEY_ESCAPE)
				goto RESULTS_CANCEL;
		}
		if (event.GUIEvent.EventType!=gui::EGET_BUTTON_CLICKED) break;
		if (event.EventType==EET_GUI_EVENT) {
			switch (event.GUIEvent.Caller->getID()) {
			case RESULTS+2:
RESULTS_OK:
				ServerSendMessage(AC_STARTGAME);
				break;
			case RESULTS+3:
RESULTS_CANCEL:
				ServerStop();
				SwitchScreen(MAIN);
				break;
			}
		}
		break;
	}
	return false;
}

#define MakeLabel(l) { \
		gui::IGUIStaticText *st=guienv->addStaticText(l, core::rect<int>(20, 20, 350, 60), false, false, 0, cs+300, false); \
		st->setOverrideFont(guienv->getFont("data/misc/fontarial_big.tga")); \
		st->setOverrideColor(video::SColor(255, 255, 255, 255)); \
		}
void CMainMenu::SwitchScreen(_currentScreen cs)
{
	gui::IGUIEnvironment* guienv = ags->device->getGUIEnvironment();
	const core::list<gui::IGUIElement*> &che=guienv->getRootGUIElement()->getChildren();

	//cleanup - erase items belonging to the old screen
	if (cs!=SELECT_MAP && cs!=SELECT_CAR && cs!=SELECT_STYLE) {
		for (core::list<gui::IGUIElement*>::ConstIterator i=che.begin(); i!=che.end(); i++) {
			if ((*i)->getID()>=currentScreen && (*i)->getID()<(currentScreen+1000)) {
				guienv->getRootGUIElement()->removeChild(*i);
				i=che.begin();
			}
		}
	}

	if (currentScreen==SELECT_CAR || currentScreen==SELECT_MAP || currentScreen==SELECT_STYLE) {
		guienv->getRootGUIElement()->getElementFromId(currentScreen, true)->remove();
		if (cs==PREGAME) {
			currentScreen=cs;
			return;
		}
	}

	currentScreen=cs;
	switch (cs) {
	case MAIN:
		MakeLabel(L"Main menu");
		guienv->addButton(core::rect<int>(20,110,210,160), 0, cs+1, L"Create game [C]");
		guienv->addButton(core::rect<int>(20,190,210,240), 0, cs+2, L"Join game [J]");
		guienv->addButton(core::rect<int>(20,270,210,320), 0, cs+3, L"Settings [S]");
		guienv->addButton(core::rect<int>(20,350,210,400), 0, cs+4, L"Quit [Q]");
		break;
	case CREATE:
		MakeLabel(L"Create game");
		guienv->addStaticText(L"Port:", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		{
		wchar_t* w=a2u(numtostring(ags->cli_port).c_str());
		guienv->addEditBox(w, core::rect<int>(50, 130, 200, 150), true, 0, cs+2);
		delete w;
		}
		guienv->addCheckBox(false, core::rect<int>(20, 160, 300, 190), 0, cs+3, L"Just spectate the game");
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+5, L"Create [Enter]");
		guienv->addButton(core::rect<int>(170,430,300,460), 0, cs+6, L"Cancel [Esc]");
		break;
	case JOIN:
		MakeLabel(L"Join game");
		guienv->addStaticText(L"Address:", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		guienv->addStaticText(L"Port:", core::rect<int>(20, 170, 300, 190), false, false, 0, cs+101);
		{
		wchar_t* w=a2u(numtostring(ags->cli_port).c_str());
		guienv->addEditBox(w, core::rect<int>(50, 190, 200, 210), true, 0, cs+2);
		delete w;
		w=a2u(ags->cli_str.c_str());
		guienv->addEditBox(w, core::rect<int>(50, 130, 200, 150), true, 0, cs+1);
		delete w;
		}
		guienv->addCheckBox(false, core::rect<int>(20, 220, 300, 250), 0, cs+3, L"Just spectate the game");
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+5, L"Join [Enter]");
		guienv->addButton(core::rect<int>(170,430,300,460), 0, cs+6, L"Cancel [Esc]");
		break;
	case CONNECT:
		MakeLabel(L"Connecting...");
		guienv->addStaticText(L"Connecting to the server...", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		ags->device->getVideoDriver()->beginScene(false, true, video::SColor(0,0,0,0));
		guienv->drawAll();	
		ags->device->getVideoDriver()->endScene();
		{
			int i;
			for (i=0; i<5; i++) {
				bool c=ConnectToServer(connectHost.c_str(), connectPort, adminMode, spectate);
				if (c) break;
				OpenThreads::Thread::YieldCurrentThread();
			}
			if (i==5) {
				ServerStop();
				SwitchScreen(CANTCONNECT);
			} else {
				SwitchScreen(PREGAME);
			}
		}

		break;
	case CANTCONNECT:
		MakeLabel(L"Error");
		guienv->addStaticText(L"Couldn't connect to the server...", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+1, L"OK [Enter]");
		break;
	case PREGAME:
		MakeLabel(L"Game arrangement");
		guienv->addListBox(core::rect<int>(20,110,455,400), 0, cs+1, false)->setEnabled(true);
		guienv->addEditBox(L"", core::rect<int>(460,110,620,130), true, 0, cs+2)->setEnabled(true);
		ags->connection->SendString((std::string)"%:"AC_CAR"\nDATA:" + ags->last_car);
		ags->connection->SendString((std::string)"%:"AC_MAP"\nDATA:" + ags->last_map);
		{
		wchar_t* ct=a2u(ags->nick.c_str());
		((gui::IGUIEditBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(cs+2, true))->setText(ct);
		delete ct;
		}
		guienv->addListBox(core::rect<int>(460,135,620,320), 0, cs+3, false)->setEnabled(true);

		guienv->addEditBox(L"", core::rect<int>(20,405,455,425), true, 0, cs+4)->setEnabled(true);

		guienv->addButton(core::rect<int>(460,325,620,345), 0, cs+5, L"Car")->setEnabled(spectate);
		guienv->addButton(core::rect<int>(460,350,620,370), 0, cs+6+100, L"Map")->setEnabled(false);
		guienv->addButton(core::rect<int>(460,375,620,395), 0, cs+10+100, L"Game style")->setEnabled(false);
		guienv->addButton(core::rect<int>(460,400,535,420), 0, cs+7+100, L"+bot")->setEnabled(false);
		guienv->addButton(core::rect<int>(545,400,620,420), 0, cs+8+100, L"-bot")->setEnabled(false);
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+9+100, L"Play [Enter]")->setEnabled(false);
		guienv->addButton(core::rect<int>(170,430,300,460), 0, cs+10, L"Cancel [Esc]");
		break;
	case SETTINGS:
		MakeLabel(L"Settings");
		{
		guienv->addStaticText(L"Graphics driver", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		gui::IGUIListBox* box = guienv->addListBox(core::rect<int>(50,130,310,230), 0, cs+2);
		box->addItem(L"Irrlicht Software Renderer");
		box->addItem(L"Apfelbaum Software Renderer");
#ifdef WIN32
		box->addItem(L"Direct3D 8.1");
		box->addItem(L"Direct3D 9.0");
#else
		box->addItem(L"N/A: Direct3D 8.1");
		box->addItem(L"N/A: Direct3D 9.0");
#endif
		box->addItem(L"OpenGL");
		box->setSelected((int)ags->driver-1);

		guienv->addCheckBox(ags->fullscreen, core::rect<int>(20,250,300,270), 0, cs+3, L"Fullscreen");
		guienv->addCheckBox(ags->vsync, core::rect<int>(20,280,300,300), 0, cs+4, L"Vertical synchronisation");

		guienv->addStaticText(L"New graphics settings will be applied after the application is restarted!", core::rect<int>(20, 370, 600, 390), false, false, 0, cs+101);
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+5, L"OK [Enter]");
		guienv->addButton(core::rect<int>(170,430,300,460), 0, cs+6, L"Cancel [Esc]");
		}
		break;
	case SELECT_MAP:
		{
		gui::IGUIWindow* window = guienv->addWindow(irr::core::rect<s32>(250, 150, 600, 470), true, L"Select map", 0, cs);
		window->getCloseButton()->setEnabled(false);
		gui::IGUIListBox* lb=guienv->addListBox(irr::core::rect<s32>(5, 25, 290, 315), window, cs+1);
		guienv->addButton(irr::core::rect<s32>(295, 25, 345, 55), window, cs+2, L"OK");
		guienv->addButton(irr::core::rect<s32>(295, 60, 345, 90), window, cs+3, L"Cancel");
		int s;
		for (int i=0; unsigned(i)<list_maps.size(); i++) {
			if (list_maps[i]==cur_map) {
				s=i;
			}
			wchar_t* ws=a2u(list_maps[i].c_str());
			lb->addItem(ws);
			delete ws;
		}
		lb->setSelected(s);
		}
		break;
	case SELECT_CAR:
		{
		gui::IGUIWindow* window = guienv->addWindow(irr::core::rect<s32>(250, 150, 600, 470), true, L"Select car", 0, SELECT_CAR);
		window->getCloseButton()->setEnabled(false);
		gui::IGUIListBox* lb=guienv->addListBox(irr::core::rect<s32>(5, 25, 290, 315), window, cs+1);
		guienv->addButton(irr::core::rect<s32>(295, 25, 345, 55), window, cs+2, L"OK");
		guienv->addButton(irr::core::rect<s32>(295, 60, 345, 90), window, cs+3, L"Cancel");
		int s;
		for (int i=0; unsigned(i)<list_cars.size(); i++) {
			if (list_cars[i]==cur_car) {
				s=i;
			}
			wchar_t* ws=a2u(list_cars[i].c_str());
			lb->addItem(ws);
			delete ws;
		}
		lb->setSelected(s);
		}
		break;
	case SELECT_STYLE:
		{
		gui::IGUIWindow* window = guienv->addWindow(irr::core::rect<s32>(250, 150, 600, 290), true, L"Select style", 0, cs);
		window->getCloseButton()->setEnabled(false);

		gui::IGUIListBox* lb=guienv->addListBox(irr::core::rect<s32>(5, 25, 290, 90), window, cs+1);
		lb->addItem(L"Single race");
		lb->addItem(L"Tournament");
		lb->addItem(L"Knockout");
		lb->setSelected(ags->game_style);
		wchar_t*wc=a2u(numtostring(ags->no_laps).c_str());
		guienv->addEditBox(wc, core::rect<s32>(80, 100, 290, 120), true, window, cs+4);
		delete wc;
		guienv->addStaticText(L"Laps: ", core::rect<s32>(5, 100, 75, 120), false, false, window, cs+5);
		guienv->addButton(irr::core::rect<s32>(295, 25, 345, 55), window, cs+2, L"OK");
		guienv->addButton(irr::core::rect<s32>(295, 60, 345, 90), window, cs+3, L"Cancel");
		}
		break;
	case GAMEPLAY:
		{
		//remove all menu graphics from scene
		const core::list<gui::IGUIElement*> &che=guienv->getRootGUIElement()->getChildren();
		while (!che.empty()) {
			guienv->getRootGUIElement()->removeChild(*che.getLast());
		}
		CGame game(ags, spectate, adminMode);
		int rv=game.run();
		//restore menu graphics
		if (rv==2) {
			GainControl();
			results=game.GetResults();
			SwitchScreen(RESULTS);
		} else if (rv==1) {
			GainControl();
			ags->DeleteConnection();
			ServerStop();
			SwitchScreen(MAIN);
		} else terminate=-1;
		}
		break;
	case RESULTS:
		MakeLabel(L"Race results");
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+2, L"Continue [Enter]")->setEnabled(adminMode);
		guienv->addButton(core::rect<int>(170,430,300,460), 0, cs+3, L"Exit [Esc]");
		{
		gui::IGUIListBox* lb=guienv->addListBox(core::rect<int>(20, 110, 620, 410), 0, cs+100);
		for (deque<string>::iterator i=results.begin(); i!=results.end(); i++) {
			wchar_t* wc=a2u(i->c_str());
			lb->addItem(wc);
			delete wc;
		}
		}
		break;
	case CONNECTION_LOST:
		MakeLabel(L"Error");
		guienv->addStaticText(L"Connection to the server lost, sorry...", core::rect<int>(20, 110, 300, 130), false, false, 0, cs+100);
		guienv->addButton(core::rect<int>(20,430,150,460), 0, cs+1, L"OK [Enter]");
		break;
	}
}

bool CMainMenu::ServerStart(int port)
{
	try {
		ServerStop();
		serverThread=new ServerThread(port);
		serverThread->start();
		OpenThreads::Thread::YieldCurrentThread(); //for better timing...
	} catch (...) {
		return false;
	}

	return true;
}

bool CMainMenu::ServerStop()
{
	ags->DeleteConnection();

	if (serverThread) {
		serverThread->quit();
		delete serverThread;
		serverThread=0;
	}

	return true;
}

void CMainMenu::ServerSendMessage(string msg)
{
	if (ags->admin && ags->connection) {
		ags->connection->SendString("%:"+msg);
	}
}

bool CMainMenu::ConnectToServer(const char *server, int port, bool asadmin, bool asspectator)
{
	AsyncComm *ac=0;
	try {
		ac=new AsyncComm(new SocketClient(server, port));
	} catch (...) {
		if (ac) delete ac;
		ac=0;
	}

	if (!ac) return false;

	if (!asspectator)
		ac->SendString("%:"AC_JOIN"\nDATA:"+ags->nick);
	else ac->SendString("%:"AC_SPECTATE"\nDATA:"+ags->nick);

	if (asadmin) ac->SendString("%:"AC_ADMIN"");

	ags->DeleteConnection();
	ags->connection=ac;

	return true;
}

void CMainMenu::ProcessSocketMessages()
{
	if (!ags->connection || !ags->connection->Alive()) {
		//the connection is lost, go back to error screen if necessary
		if (currentScreen==PREGAME || currentScreen==SELECT_MAP || currentScreen==SELECT_CAR ||
			currentScreen==SELECT_STYLE || currentScreen==GAMEPLAY || currentScreen==RESULTS) {
			SwitchScreen(CONNECTION_LOST);
		}
		return;
	}
	AsyncComm *ac=ags->connection;
	std::string str;

	while (ac->ReadString(str)) {
		cmd_map cmd;
		cmd_String2Map(str, cmd);

		if (cmd["%"]==AC_ADMIN) {
			adminMode=true;
			ags->admin=true;
			for (int i=PREGAME+100; i<PREGAME+200; i++) {
				if (ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(i, true))
					ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(i, true)->setEnabled(true);
			}
		} else if (cmd["%"]==AC_STARTGAME) {
			SwitchScreen(GAMEPLAY);
			break;
		} else if (cmd["%"]==AC_CHAT) {
			wchar_t* ct=a2u(cmd["DATA"].c_str());
			((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+1, true))->addItem(ct);
			delete ct;
		} else if (cmd["%"]==AC_USERS) {
			((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+3, true))->clear();
			for (cmd_map::iterator i=cmd.begin(); i!=cmd.end(); i++) {
				if (i->first=="%") continue;
				
				std::string s=i->first + " [" + i->second + "]";
				wchar_t* ct=a2u(s.c_str());
				((gui::IGUIListBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+3, true))->addItem(ct);
				delete ct;
			}
		} else if (cmd["%"]==AC_LIST_CARS) {
			list_cars.clear();
			for (cmd_map::iterator i=cmd.begin(); i!=cmd.end(); i++) {
				if (i->first=="%") continue;
				list_cars.push_back(i->first);
			}
		} else if (cmd["%"]==AC_LIST_MAPS) {
			list_maps.clear();
			for (cmd_map::iterator i=cmd.begin(); i!=cmd.end(); i++) {
				if (i->first=="%") continue;
				list_maps.push_back(i->first);
			}
		} else if (cmd["%"]==AC_CAR) {
			cur_car=cmd["DATA"].c_str();
			ags->last_car=cur_car;
			wchar_t* ct=a2u(("Car ["+cur_car+"]").c_str());
			((gui::IGUIButton*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+5, true))->setText(ct);
			delete ct;
		} else if (cmd["%"]==AC_NICK) {
			ags->nick=cmd["DATA"];
			wchar_t* ct=a2u(cmd["DATA"].c_str());
			((gui::IGUIEditBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+2, true))->setText(ct);
			delete ct;
		} else if (cmd["%"]==AC_MAP) {
			cur_map=cmd["DATA"];
			ags->last_map=cur_map;
			wchar_t* ct=a2u(("Map ["+cur_map+"]").c_str());
			((gui::IGUIButton*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+6+100, true))->setText(ct);
			delete ct;
		} else if (cmd["%"]==AC_JOIN || cmd["%"]==AC_SPECTATE) {
			spectate=(cmd["%"]==AC_SPECTATE);
			if (currentScreen==PREGAME) ((gui::IGUICheckBox*)ags->device->getGUIEnvironment()->getRootGUIElement()->getElementFromId(PREGAME+5, true))->setEnabled(!spectate);
		}
	}
}
