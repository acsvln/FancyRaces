/**
 * \file
 * This file contains implementation of SObject and SObjectGroup classes.
**/

#include "Sobject.h"
#include "Autil.h"

using namespace std;
using namespace irr;

bool SObjectGroup::Load(
	const string &file, CfgFile &cfg, const string &cfg_path, dWorld &world, 
	dSpace& space, scene::ISceneManager* sm, std::deque<const SObject*> &mo, 
	std::map<std::string, int> &surfaceTypes
)
{
	if (cfg.Getval_exists(cfg_path+"file")) {
		scene::IMesh* mesh;
		scene::IAnimatedMesh* amesh;

		string map=(string)"data/maps/"+file+"/"+cfg.Getval_str(cfg_path+"file", "");
		core::vector3df scale=cfg.Getval_vector(cfg_path+"scale", core::vector3df(1, 1, 1));

		try {
			string ext=map.substr(map.rfind('.'));
			if (ext.size()>100) throw "bad size";
			ext=StringLower(ext);

			if (ext==".bmp" || ext==".png") { //TODO:another irrlicht supported formats?
				scene::ITerrainSceneNode* terrain = sm->addTerrainSceneNode(map.c_str());
				terrain->setScale(scale);

				MakeFromHeightmap(terrain, space, surfaceTypes);
			} else if (ext==".x" || ext==".3ds" || ext==".ms3d") {
				amesh = sm->getMesh(map.c_str());
				if (!amesh) {
					DBGCOUT("SObjectGroup", "init", "Can't load object file"+map);
					return false;
				}
				mesh=amesh->getMesh(0);
				if (mesh==0) return false; // do nothing if the mesh is NULL
				sm->getMeshManipulator()->scaleMesh(mesh, scale);
				MakeFromMesh(mesh, space, cfg, cfg_path, surfaceTypes);
			}
		} catch (...) {
			DBGCOUT("SObjectGroup", "init", "no/bad extension, no bussines...");
			return false;
		}
	} else if (cfg.Getval_str(cfg_path+"type", "")=="box") {
		if (cfg.Getval_exists(cfg_path+"Physics/surface")) {
			DBGCOUT("SObjectGroup", "Load", "Loading "+cfg_path);
			SObject *so=new SObject();

			core::vector3df v=cfg.Getval_vector(cfg_path+"scale"), v2;
			so->ode_geom = new dBox(space.id(), v.X, v.Y, v.Z);
			so->ode_geom->setData((SGeomObj*)so);
			
			double weight=cfg.Getval_double(cfg_path+"Physics/weight", 0);
			if (weight>0) {
				dMass m;
				so->ode_body = new dBody(world.id());
				m.setBox(1, v.X, v.Y, v.Z);
				m.adjust((dReal)(weight));
				so->ode_body->setMass(&m);
				so->ode_geom->setBody(so->ode_body->id());
			}

			v=cfg.Getval_vector(cfg_path+"pos");
			v2=cfg.Getval_vector(cfg_path+"rot");
			so->SetPosRot(v.X, v.Y, v.Z, v2.X, v2.Y, v2.Z);
			so->surface_type=surfaceTypes[cfg.Getval_str(cfg_path+"Physics/surface")];

			parts.push_back(so);
			if (so->ode_body) mo.push_back(so);
		}
	} else if (cfg.Getval_str(cfg_path+"type", "")=="sphere") {
		if (cfg.Getval_exists(cfg_path+"Physics/surface")) {
			DBGCOUT("SObjectGroup", "Load", "Loading "+cfg_path);
			SObject *so=new SObject();

			core::vector3df v, v2;
			so->ode_geom = new dSphere(space.id(), (dReal)cfg.Getval_double(cfg_path+"radius", 1));
			so->ode_geom->setData((SGeomObj*)so);

			double weight=cfg.Getval_double(cfg_path+"Physics/weight", 0);
			if (weight>0) {
				dMass m;
				so->ode_body = new dBody(world.id());
				m.setSphere(1, (dReal)cfg.Getval_double(cfg_path+"radius", 1));
				m.adjust((dReal)(weight));
				so->ode_body->setMass(&m);
				so->ode_geom->setBody(so->ode_body->id());
			}

			v=cfg.Getval_vector(cfg_path+"pos");
			v2=cfg.Getval_vector(cfg_path+"rot");
			so->SetPosRot(v.X, v.Y, v.Z, v2.X, v2.Y, v2.Z);
			so->surface_type=surfaceTypes[cfg.Getval_str(cfg_path+"Physics/surface")];

			parts.push_back(so);
			if (so->ode_body) mo.push_back(so);
		}
	} else return false;

	return true;
}

bool SObjectGroup::MakeFromHeightmap(scene::ITerrainSceneNode* terrain, dSpace &space, std::map<std::string, int> &surfaceTypes)
{
	scene::CDynamicMeshBuffer mb(irr::video::EVT_2TCOORDS, irr::video::EIT_32BIT);
// 	scene::SMeshBufferLightMap mb;
	int j,ci,cif,cv;
	SObject *ti=new SObject();

	terrain->getMeshBufferForLOD(mb);
	
	ti->vertexcount=mb.getVertexCount();
	ti->vertices=new dVector3[ti->vertexcount];
	ti->indexcount=mb.getIndexCount();
	ti->indices=new int[ti->indexcount];
	
	//DBGCOUT("SObjectGroup", "MakeIVFromHeightmap", "VC: "+numtostring(ti->vertexcount)+", IC: "+numtostring(ti->indexcount));
	// fill trimesh geom
	ci=0;
	cif=0;
	cv=0;

	// fill indices
	irr::u16* mb_indices=mb.getIndices();
	for(j=0;j<mb.getIndexCount();j++) {
		ti->indices[ci]=cif+mb_indices[j];
		ci++;
	}
	cif=cif+mb.getVertexCount();
	// fill vertices
	if(mb.getVertexType()==irr::video::EVT_STANDARD) {
		irr::video::S3DVertex* mb_vertices=(irr::video::S3DVertex*)mb.getVertices();
		for(j=0;j<mb.getVertexCount();j++) {
			ti->vertices[cv][0]=mb_vertices[j].Pos.X;
			ti->vertices[cv][1]=mb_vertices[j].Pos.Y;
			ti->vertices[cv][2]=mb_vertices[j].Pos.Z;
			cv++;
		}	
	} else if(mb.getVertexType()==irr::video::EVT_2TCOORDS) {
		irr::video::S3DVertex2TCoords* mb_vertices=(irr::video::S3DVertex2TCoords*)mb.getVertices();
		for(j=0;j<mb.getVertexCount();j++) {
			ti->vertices[cv][0]=mb_vertices[j].Pos.X;
			ti->vertices[cv][1]=mb_vertices[j].Pos.Y;
			ti->vertices[cv][2]=mb_vertices[j].Pos.Z;
			cv++;
		}				
	}

	ti->Init(space);
	parts.push_back(ti);
	return true;
}

bool SObjectGroup::MakeFromMesh(scene::IMesh* mesh, dSpace &space, CfgFile &cfg, const string &cfg_path, std::map<std::string, int> &surfaceTypes)
{
	int i;
	for(i=0;i<mesh->getMeshBufferCount();i++){
		SObject *ti=new SObject();

		int j,ci,cif,cv;
		ti->indexcount=0;
		ti->vertexcount=0;
		// count vertices and indices
		irr::scene::IMeshBuffer* mb=mesh->getMeshBuffer(i);
		ti->indexcount+=mb->getIndexCount();
		ti->vertexcount+=mb->getVertexCount();
		//DBGCOUT("SObjectGroup", "init", "MC:" + numtostring(mesh->getMeshBufferCount())+"; VC: "+numtostring(ti->indexcount)+"; IC: "+numtostring(ti->vertexcount));
		// build structure for ode trimesh geom
		ti->vertices=new dVector3[ti->vertexcount];
		ti->indices=new int[ti->indexcount];
		// fill trimesh geom
		ci=0;
		cif=0;
		cv=0;

		// fill indices
		irr::u16* mb_indices=mb->getIndices();
		for(j=0;j<mb->getIndexCount();j++) {
			ti->indices[ci]=cif+mb_indices[j];
			ci++;
		}
		cif=cif+mb->getVertexCount();
		// fill vertices
		if(mb->getVertexType()==irr::video::EVT_STANDARD) {
			irr::video::S3DVertex* mb_vertices=(irr::video::S3DVertex*)mb->getVertices();
			for(j=0;j<mb->getVertexCount();j++) {
				ti->vertices[cv][0]=mb_vertices[j].Pos.X;
				ti->vertices[cv][1]=mb_vertices[j].Pos.Y;
				ti->vertices[cv][2]=mb_vertices[j].Pos.Z;
				cv++;
			}	
		} else if(mb->getVertexType()==irr::video::EVT_2TCOORDS) {
			irr::video::S3DVertex2TCoords* mb_vertices=(irr::video::S3DVertex2TCoords*)mb->getVertices();
			for(j=0;j<mb->getVertexCount();j++) {
				ti->vertices[cv][0]=mb_vertices[j].Pos.X;
				ti->vertices[cv][1]=mb_vertices[j].Pos.Y;
				ti->vertices[cv][2]=mb_vertices[j].Pos.Z;
				cv++;
			}
		}

		ti->Init(space);
		ti->surface_type=surfaceTypes[cfg.Getval_str(cfg_path+"Physics/"+numtostring(i+1)+"/surface", "asphalt")];
		//cerr << "SURFACE " <<cfg_path<<"Physics/"<<numtostring(i+1)+"/surface"<<": " <<ti->surface_type <<"("<<cfg.Getval_str(cfg_path+"Physics/"+numtostring(i+1)+"/surface", "asphalt") <<endl;
	
		parts.push_back(ti);
	}
	return true;
}

SObjectGroup::~SObjectGroup()
{
	while (!parts.empty()) {
		SObject *t=parts.front();
		parts.pop_front();
		delete t;
	}
}

bool SObject::Init(dSpace &space)
{
	irr::core::vector3df pos(0, 0, 0);
	// build the trimesh data
	dTriMeshDataID data=dGeomTriMeshDataCreate();
	dGeomTriMeshDataBuildSimple(data, (dReal*)vertices, vertexcount, (dTriIndex*)indices, indexcount);
	// build the trimesh geom 
	dGeomID ode_geomid=dCreateTriMesh(space, data, 0, 0, 0);
	// set the geom position 
	dGeomSetPosition(ode_geomid, pos.X, pos.Y, pos.Z);
	// lets have a pointer to our bounceable, we could need this in the collision callback
	dGeomSetData(ode_geomid, (void*)((SGeomObj*)this)); 

	// in our application we don't want geoms constructed with meshes (the object) to have a body
	dGeomSetBody(ode_geomid, 0);  

	ode_geom=new dGeomWrapper(ode_geomid);
	return true;
}
