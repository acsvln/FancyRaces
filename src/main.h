/**
 * \file
 * This file contains declaration of AGameSettings classes.
**/

#ifndef _MAIN_H_
#define _MAIN_H_

#include <deque>
#include <fstream>
#include <iostream>
#include <string>
#include "Aasynccomm.h"
#include <irrlicht/irrlicht.h>
#include "Autil.h"

/**
 * Contains some settings which are used by initializing the graphics in game.
**/
class AGameSettings {
public:
	bool fullscreen; //!< fullscreen mode
	bool music; //!< music during the game (not used)
	bool shadows; //!< realtime shadows in the game (not used)
	bool vsync; //!< vertical synchronisation of the screen
	bool admin; //!< true, if we want to connect and start being administrator on the server
	int srv_port; //!< port, where the server will be awaiting connections
	int bpp; //!< bit depth of the game
	std::string cli_str; //!< address of the server, to which the client connects
	int cli_port; //!< port of the server, to which the client connects
	std::string nick; //!< preferred nickname
	int game_style; //!< game style (0=single race; 1=tournament; 2=knockout)
	int no_laps; //!< number of laps per race
	std::string last_map; //!< last map used by the user
	std::string last_car; //!< last car used by the user

	irr::video::E_DRIVER_TYPE driver; //!< driver used for the graphics

	irr::IrrlichtDevice *device; //!< device, where everything is drawn...

	AsyncComm *connection; //!< current connection socket handle

	/**
	 * closes the connection to server (and removes object associated with it)
	**/
	void DeleteConnection() {if (connection) delete connection; connection=0;}
	
	/**
	 * initializes settings to some default values.
	**/
	AGameSettings():
		fullscreen(false), music(false), shadows(false), vsync(true), admin(false), connection(0),
		driver(irr::video::EDT_OPENGL), cli_str("localhost"), cli_port(4041), srv_port(4041), bpp(16/*TODO-let user choose*/),
		nick("novice"),
		game_style(0), no_laps(1)
		{}
		
	/**
	 * Destructor. Calls DeleteConnection() to close connection to server (if one exists).
	**/
	~AGameSettings() {DeleteConnection();}

	/**
	 * loads settings from file
	**/
	void Load(std::string path="anorasi.ini") {
		std::ifstream f(path.c_str(), std::ios::in);
		if (!f.good()) return;
		char buf[1024];

		while (!f.eof()) {
			f.getline(buf, 1024, '\n');

			strtrim(buf);
			if (buf[0]=='\0') continue;
			char *sd=strchr(buf, '=')+1;
			*(strchr(buf, '='))=0;
			std::string sn=buf;

			if (sn=="fullscreen") fullscreen=(atoi(sd)>0)?true:false;
			if (sn=="bpp") cli_port=atoi(sd);
			if (sn=="music") music=(atoi(sd)>0)?true:false;
			if (sn=="shadows") shadows=(atoi(sd)>0)?true:false;
			if (sn=="vsync") vsync=(atoi(sd)>0)?true:false;
			if (sn=="srv_port") srv_port=atoi(sd);
			if (sn=="cli_port") cli_port=atoi(sd);
			if (sn=="cli_str") cli_str=sd;
			if (sn=="nick") nick=sd;
			if (sn=="driver") driver=(irr::video::E_DRIVER_TYPE)(atoi(sd));
			if (sn=="game_style") game_style=atoi(sd);
			if (sn=="no_laps") no_laps=atoi(sd);
			if (sn=="last_map") last_map=sd;
			if (sn=="last_car") last_car=sd;
		}
//#ifndef WIN32
//		if (driver==irr::video::E_DRIVER_TYPE::EDT_DIRECT3D8 || driver==irr::video::E_DRIVER_TYPE::EDT_DIRECT3D9)
//			driver==irr::video::E_DRIVER_TYPE::EDT_OPENGL;
//#endif
	}

	/**
	 * saves the current settings to file
	**/
	void Save(std::string path="anorasi.ini") {
		std::ofstream f(path.c_str(), std::ios::out);

		f << "fullscreen"	<< "=" << fullscreen	<< std::endl;
		f << "bpp"			<< "=" << bpp			<< std::endl;
		f << "music"		<< "=" << music			<< std::endl;
		f << "shadows"		<< "=" << shadows		<< std::endl;
		f << "vsync"		<< "=" << vsync			<< std::endl;
		f << "srv_port"		<< "=" << srv_port		<< std::endl;
		f << "cli_str"		<< "=" << cli_str		<< std::endl;
		f << "cli_port"		<< "=" << cli_port		<< std::endl;
		f << "driver"		<< "=" << driver		<< std::endl;
		f << "nick"			<< "=" << nick			<< std::endl;
		f << "game_style"	<< "=" << game_style	<< std::endl;
		f << "last_map"		<< "=" << last_map		<< std::endl;
		f << "last_car"		<< "=" << last_car		<< std::endl;
	}
};

#endif

/**
 * \mainpage AnoRaSi programmer's manual index page
 *
 * \section compilation Compilation
 *
 * Anorasi source code is multiplatform. Unfortunately, there are currently different compile procedures for both supported platforms - windows and linux.
 *
 * The first step by compiling the source under both OS's is to have the required libraries installed. Namely you have to have Irrlicht, OpenAL++, OpenThreads and ODE installed (including dev package, if you are installing them using some kind of linux package manager).
 * After that you just have to open project file in visual studio and compile it (windows), or run make in root source directory (linux). The compile process isn't bulletproof, so you may experience difficulties - mainly because of not properly installed libraries. Fixing this should be trivial, if you are that experienced you want to compile the application by yourself.
 *
 * \section readhow How to read this manual
 *
 * This manual isn't meant for beginner users. Informations contained here are showing, how does the program work, what internals it makes use of etc. Prerequisities for reading of this manual are knowledge of c++ and intermediate knowledge about the libraries used. In case you aren't familiarized with the libraries functions, it might me a little hard to understand some more technical parts of the manual.
 *
 * The mainpage of this manual is pretty short, because all the informations of this documentation are connected by hypertext links - so you can easily watch the program workflow by clicking the links. All variables and methods of program (excepting Socket class, which is an another man's work - see http://www.adp-gmbh.ch/win/misc/sockets.html) are described in this document, so you shouldn't get too lost in the code by reading it.
 * 
 * \section workflow Program workflow
 *
 * This part is really short. The entry point of this program is the main() function. See it's description for additional informations about program workflow.
 *
 * \section structure Code structure
 *
 * The program code is basically divided into 3 parts:
 *
 * First group contains client code - here do belong all files beginning with letter C. This group can be further divided into code containing main menu code (CMainMenu.h, CMainMenu.cpp) and scene-showing code (CCandies.h, CCandies.cpp, CGame.h, CGame.cpp, CModels.h, CModels.cpp).
 *
 * Second group contains server code - here do belong all files beginning with letter S. The group consists of files containing main server code (Sserver.h, Sserver.cpp) and simulation code (Sgameplace.h, Sgameplace.cpp, Sobject.h, Sobject.cpp, Ssimulation.h, Ssimulation.cpp, Svehicle.h, Svehicle.cpp).
 *
 * Last group contains common code used by both parts - here do belong all files beginning with letter A (Aasynccomm.h, Aasynccomm.cpp, Acfgfile.h, Acfgfile.cpp, Asocket.h, Asocket.cpp, Autil.h, Autil.cpp)
 *
 * Files that can't be put into any of these groups are main.cpp and main.h.
**/

