/**
 * \file
 * This file contains implementations of the AsyncCommThread and AsyncComm classes.
**/

#include "Aasynccomm.h"
#include <iostream>
#include "Autil.h"
#include <OpenThreads/Thread>

using namespace std;

//remark: lock sockets? (when testing if it is still alive) => no need to (in worst case we end up with empty buffer..)

#define RCHAR (char)'#' //!< this character is used to escape control sequences in outgoing message; it is doubled when used in outgoing message (and in incoming ones - two characters are converted to one again)
#define RCHAR_ENDL (char)'$' //!< this character is used to replace the SOCK_ENDL characters in outgoing messages (by using RCHAR as the escape)

/**
 * Class of the thread, which processes all the socket communication and puts it into string array (used by AsynComm)
**/
class AsyncCommThread: public OpenThreads::Thread {
private:
	AsyncComm *us;
public:
	AsyncCommThread(AsyncComm *_us) : OpenThreads::Thread(), us(_us) {}
	virtual ~AsyncCommThread() {}

	virtual void run()
	{
		int err;
		string r;

		while (!us->stop) {
			//read until we get the whole line or have to die
			r="";
			err=0;
			do {
				if (err) YieldCurrentThread();
				r+=us->sock->ReceiveLine(err);
			} while (err==1 && !us->stop);

			if (us->stop || err==2) break;

			//don't accept empty strings
			r.erase(r.end()-1, r.end());

			if (r.empty()) continue;

			if (!r.compare(0, 4, "STOP")) break;

			us->mutex.lock();
			us->strings.push(r);
			us->mutex.unlock();
		}

		us->stop=2;
		if (us->sock) {
			delete us->sock;
			us->sock=0;
		}
	}

	void quit() {
		if (us->stop!=2) {
			us->stop=1;
			while (us->stop!=2) YieldCurrentThread();
		}
		DBGCOUT("ASyncCommThread", "quit", "success");
	}
};

AsyncComm::AsyncComm()
{
	sock=0;
	stop=2;
}

AsyncComm::AsyncComm(Socket *s)
{
	if (s==0) {
		sock=0;
		stop=2;
	} else Init(s);
}

void AsyncComm::Init(Socket *s)
{
	sock=s;
	stop=0;

	s->SetBlocking(NonBlockingSocket);
	thread=new AsyncCommThread(this);
	thread->start();
}

AsyncComm::~AsyncComm()
{
	disconnect();
}

void AsyncComm::disconnect()
{
	thread->quit();
}

void AsyncComm::SendString(std::string str)
{
	string buf;

	if (!Alive()) return;

	string::iterator i;
	for (i=str.begin(); i!=str.end(); i++) {
		if (*i!=SOCK_ENDL && *i!=RCHAR)
			buf+=*i;
		else if (*i==RCHAR) { //double 2 characters
			buf+=RCHAR;
			buf+=RCHAR;
		} else { //replace SOCK_ENDL with 2, 3
			buf+=RCHAR;
			buf+=RCHAR_ENDL;
		}
	}

	sock->SendLine(buf);
}

bool AsyncComm::ReadString(std::string &str)
{
	bool rv=false;
	string buf;

	if (!Alive()) return false;

	mutex.lock();
	if (strings.empty())
		rv=false;
	else {
		rv=true;
		buf=strings.front();
		strings.pop();
		string::iterator i;
		str="";

		for (i=buf.begin(); i!=buf.end(); i++) {
			if (*i!=RCHAR) {
				str+=*i;
			} else {
				if (*(i+1) == RCHAR) {
					str+=RCHAR;
					i++;
				} else if (*(i+1) == RCHAR_ENDL) {
					str+=SOCK_ENDL;
					i++;
				} 
			}
		}
	}
	mutex.unlock();

	return rv;
}
