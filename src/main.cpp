/**
 * \file
 * This file contains implementation of the main() function - entry point of the application.
**/

#include <irrlicht/irrlicht.h>
#include <stdio.h>
#include <time.h>

#include "CMainMenu.h"
#include "Autil.h"
#include "Aasynccomm.h"
#include <deque>
#include <openalpp/alpp.h>
#include <OpenThreads/Thread>

using namespace irr;

#ifdef WIN32
#pragma comment(lib, "OpenThreadsWin32.lib")
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(lib, "OpenAL32.lib")
#pragma comment(lib, "ALut.lib")
#pragma comment(lib, "oalpp.lib")
#else
//TODO: #pragma comment(lib, "libOpenThreads.a")
#endif

using namespace std;

/**
 * Main function of the program. It initializes used libraries (mainly Irrlicht and OpenThreads),
 * then creates the object representating main menu (by class CMainMenu) and calls it's method CMainMenu::run().
 * In the end it frees the used memory
**/
int main(int argc, char **argv)
{
	AGameSettings ags;
	CMainMenu menu(&ags);
	int mr=0;
	ags.Load();

	srand((unsigned int)time(0));
	OpenThreads::Thread::Init();
	
	//initialize interfaces (graphics, sound)
//RESTART_GRAPHICS:
	ags.device = createDevice(ags.driver, core::dimension2d<u32>(640, 480), 16, ags.fullscreen, false, ags.vsync);
	DeviceFS_AddData(ags.device);

	menu.run();

	ags.device->closeDevice();
	ags.device->drop();
	
	//if (mr==2) goto RESTART_GRAPHICS;

	ags.Save();
	return 0;
}
