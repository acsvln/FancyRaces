/**
 * \file
 * This file contains declaration of CMainMenu class.
**/

#ifndef __C_MAIN_MENU_H_INCLUDED__
#define __C_MAIN_MENU_H_INCLUDED__

#include <deque>
#include <string>
#include "Aasynccomm.h"
#include <irrlicht/irrlicht.h>
#include "main.h"
#include "Sserver.h"

using namespace irr;

/**
 *	This class ensures all the stuff required for menu-showing. Namely initializing the irrlicht engine, creating GUI, processing user input, starting/stopping/connecting/communicating with server etc.
**/
class CMainMenu : public IEventReceiver
{
public:
	/**
	 * Constructor.
	 * \n Initializes the class (alocates memory, initializes some variables).
	**/
	CMainMenu(AGameSettings *ags);

	/**
	 * Destructor.
	 * \n Deinitializes the class (deallocates memory, if any allocated).
	**/
	~CMainMenu();

	/**
	 * Initializes the main menu screen and GUI. Then it comes to a while cycle, where it processes the user input and network communication. After user choses to end the menu (either by starting game, or by closing window), the cycle exits, and so does the function. At the function exit, the server is stopped (if it's online).
	 * \n Main methods called by this one are ProcessSocketMessages() and OnEvent() (this one indirectly)
	 * @return CMainMenu::terminate member value is returned
	**/
	int run();

	/**
	 * Dispatches irrlicht events (standard irrlicht overloaded function) - keyboard and irrlicht button press messages.
	 * \n When needed, using SwitchScreen() it shows another screen to the user. In some cases it also starts the server (using StartServer() method) or terminates it (ServerStop()).
	 * \n If connected to the server, it also sends messages according to user actions (by calling the AsyncComm::SendString() method on the AsyncComm::ags member AGameSettings::connection)
	**/
	virtual bool OnEvent(const SEvent &event);

	/**
	 * A public variable, which contains settings for the CGame class.
	 * @see CGame
	**/
	AGameSettings *ags;
private:
	/**
	 * information, in which screen we are currently; the names are pretty self-explaining :)
	**/
	enum _currentScreen {
		NONE=0,
		MAIN=1000,
		CREATE=2000,
		JOIN=3000,
		CONNECT=4000,
		CANTCONNECT=5000,
		PREGAME=6000,
		SETTINGS=7000,
		SELECT_MAP=8000,
		SELECT_CAR=9000,
		SELECT_STYLE=10000,
		GAMEPLAY=11000,
		RESULTS=12000,
		CONNECTION_LOST=13000,
	} currentScreen;

	/**
	 * hides (if necessary) old controls and shows new one(s)
	 * @param cs the new screen we want to show
	**/
	void SwitchScreen(_currentScreen cs);

	std::deque<std::string> list_cars; //!< list of cars available on server
	std::deque<std::string> list_maps; //!< list of maps available on server
	std::string cur_car; //!<currently selected car
	std::string cur_map; //!<currently selected map
	
	ServerThread *serverThread; //!<server thread pointer

	/**
	 * This variable informs the run() procedure, when (and how) it should quit.
	 * \n If -1, the main menu should just quit, no game to be started.
	 * \n If 0, continue the menu.
	 * \n If 1, a game has been started, switch to the CGame.
	**/
	int terminate;

	bool adminMode; //!< enables some controls - to allow user to control the server
	std::string connectHost; //!< host, to which the next call of Connect function will try to connect
	int connectPort; //!< port, to which the next call of Connect function will try to connect
	bool spectate; //!< if true, client is in spectator mode (i.e. user doesn't control any car)

	/**
	 * Function tries to connect to the specified server and to gain the specified rights.
	 * @param asadmin if true, function tries to gain administrator access to the server
	 * @param server url/ip address of the server
	 * @param port port on the server computer to connect to
	 * @param asspectator if true, just connect as a spectator; otherwise as a player
	 * @return returns true, if connected to the server (doesn't indicate, whether we got the requested rights)
	**/
	bool ConnectToServer(const char *server, int port, bool asadmin, bool asspectator);

	/**
	 * Starts a new instance of server (by creating instance of class ServerThread) on specified port. In case one already exists, it's killed and a new one is started.
	**/
	bool ServerStart(int port);

	/**
	 * Terminates connections to server and the server itself (if one ist started)
	 * @return true on success, false otherwise
	**/
	bool ServerStop();

	/**
	 * Sends a simple message (without parameters) to the server (if user is admin and connection exists)
	**/
	void ServerSendMessage(std::string msg);

	/**
	 * If there are any connnections, it processes all the messages and does all the things neccesary... It assumes that the right
	 * screen is open etc. (i.e. that the menu is in consistent state with the messages)
	**/
	void ProcessSocketMessages();

	/**
	 * Gains control over the device (loads necessary data, sets the current object as a listener for Irrlicht "messages" etc.)
	 * @see OnEvent
	**/
	void GainControl();

	std::deque<std::string> results; //!<results of the game, just to transfer informations from CGame to CMainMenu
};

#endif

