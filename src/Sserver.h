/**
 * \file
 * This file contains declaration of commItem and ServerThread classes.
**/

#ifndef _SSERVER_H_
#define _SSERVER_H_

#include "Aasynccomm.h"
#include <string>
#include <OpenThreads/Thread>
#include <OpenThreads/Mutex>
#include <string>
#include <queue>
#include <list>
#include <iostream>
#include <utility>
#include "Ssimulation.h"
#include "Autil.h"
#include <irrlicht/irrlicht.h>

class ServerThread;
class UserSocket;
class ServerSocketThread;

typedef std::list<UserSocket*> socket_list;

/**
 * Structure containing informations about message from server to clients and from clients to server
**/
struct commItem {
	UserSocket *us; //!< target of the message (if 0, all open sockets are targets)
	std::string msg; //!< the message contents
	
	/**
	 * possible values for the attr member
	**/
	enum {
		TTL=1, //!< enables the ttl value
		BROADCAST_TO_CONNECTED=2, //!< this value isn't used
	};
	irr::u32 attr; //!< attributes of the message
	irr::u32 ttl; //!< Time To Live - time, after which the message will be removed from queue instead of being sent

	/**
	 * constructor, fills the structure according to parameters (nothing special happens here, values are simply copied)
	**/
	commItem(UserSocket *us_, const std::string &msg_, irr::u32 attr_=0, irr::u32 ttl_=0):us(us_), msg(msg_), attr(attr_), ttl(ttl_) {}
};

/**
 * Declares possible types of users in UserSocket::attr_
**/
enum {
	USER_ADMIN=1,
	USER_BOT=2,
};

/**
 * Class for a thread, that does all the server work (communication with clients, doing the simulation using the class SSimulation). Also starts a thread (ServerSocketsThread) that processes socket communication. These two threads then communicate through commSrvSockQueue and commSockSrvQueue queues. Also, they use the dataMutex to synchronize the access.
 * @return 0 on successful stop, nonzero otherwise
**/
class ServerThread: public OpenThreads::Thread {
private:
	int port; //!< port, where the server is awaiting connections
	std::string acceptFrom; //!< not used yet
	int passwd; //!< not used yet
	irr::IrrlichtDevice *device;
public:
	/**
	* Constructor for the server class; starts the server on the port 'port'. The rest of the arguments isn't used.
	**/
	ServerThread(int _port, int _adminpass=0, const std::string &_acceptfrom=(std::string)""): OpenThreads::Thread(), ss(0),
		gameStyle(0), noLaps(1), device(0), posInfoMaxDelay(200), cur_map_maxSlots(1)
	{
		port=_port;
		passwd=_adminpass;
		acceptFrom=_acceptfrom;
	}
	virtual ~ServerThread() {}
	virtual void quit();
	irr::IrrlichtDevice *GetDevice() {return device;}
private:
	bool stop; //!< if true, we want the server to quit

	OpenThreads::Mutex dataMutex; //!< mutex for access to commSrvSockQueue and commSockSrvQueue
	socket_list srv_clients; //!< list of all players
	std::queue<commItem> commSrvSockQueue; //!< messages from server to clients
	std::queue<commItem> commSockSrvQueue; //!< messages from clients to server
	SSimulation *ss; //!< the main simulation object
	
	friend class ServerSocketThread;
	ServerSocketThread *socketThread; //!< pointer to the thread, that is managing communication with clients
	std::deque<std::string> list_cars; //!<lists of possible values for cars
	std::deque<std::string> list_maps; //!<lists of possible values for maps
	int gameStyle; //!< game style (0=single race; 1=tournament; 2=knockout)
	int noLaps; //!< number of laps per track
	std::string cur_map; //!< current map used in server
	int cur_map_maxSlots; //!< maximal number of players for current map
	irr::s32 posInfoMaxDelay; //!< maximal delay between sending the message from server and forwarding it by the socket thread to client's socket
	int serverState; //!< defines current state of the server (loading data, perfoming the main simulation loop etc.)

	/**
	 * Checks parameters that client set, and fixes (=sets valid values and sends updates to client(s) them if necessary)
	 * @param us socket of srv_clients, that we wat to get fixed
	 * @return returns true, if something needed to be fixed (and user(s) has been already notified); false otherwise
	**/
	bool FixUserConfig(UserSocket *us);

	/**
	 * @param map name of map
	 * @return number of available slots for specified map name
	**/
	int GetMapSlots(std::string map);

	/**
	 * returns number of bots+joint clients
	**/
	int GetOccupiedSlots();

	/**
	 * the main server function
	**/
	virtual void run();

	/**
	 * The controls for bot defined by 'us' parameter are recalculated by calling this function...
	 * @param ss pointer to the curret simulation
	 * @param us server's handle to the bot
	 * @param av pointer to the bot's vehicle (it's object from ss with ID us->simid_)
	**/
	void Bot_Process(UserSocket *us, SVehicle *av);

	/**
	 * Initialize bot
	 * @param ss pointer to the curret simulation
	 * @param us server's handle to the bot
	**/
	void Bot_Init(UserSocket *us);

	/**
	 * Returns string containing list of connected users, which is suitable for sending to clients...
	**/
	std::string GetUserlist();

	/**
	 * Send all players' position + movement info to all clients. By all players we mean all vehicles in the simulation - even the ones of disconnected users (as we delete them from simulation).
	 * @param target If it's 0, the informations are sent to all clients; otherwise just to the one specified
	 * @param lock if true, the dataMutex will be locked during the function execution
	**/
	void BroadcastPlayersInfo(UserSocket *target=0, bool lock=true);
	
	/**
	 * Send simulation's moveable objects' (except vehicles) position + movement info to all clients.
	 * @param target If it's 0, the informations are sent to all clients; otherwise just to the one specified
	**/
	void BroadcastObjectsInfo(UserSocket *target=0);

	/**
	 * Send current simulation informations to all clients.
	 * If the race is finished, switch server status...
	**/
	void BroadcastSimStatus(int &serverState);

	/**
	 * Sends informations about players to the specified client
	 * @param tgt If it's 0, the informations are sent to all clients; otherwise just to the one specified
	**/
	void SendPlayerInitInfo(UserSocket *tgt);

	/**
	 * Returns number of active players
	**/
	int GetNoPlayers();

	/**
	 * Creates the simulation objects (map, vehicles)
	**/
	void CreateSim();

	/**
	 * called at the end of simulation to free/cleanup used memory
	**/
	void CleanupSim();

	/**
	 * Starts the game by sending the right messages to clients and switching server to appropiate state
	**/
	void StartSim();
};

#endif
