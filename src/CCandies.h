/**
 * \file
 * This file contains declaration of CCandies class.
**/

#ifndef _CCANDIES_H_
#define _CCANDIES_H_

#include <irrlicht/irrlicht.h>
#include <deque>

class CGame;

/**
 * This class holds all the graphics candies in the scene - and serves to CGame by displaying/erasing them
**/
class CCandies {
public:
	/**
	 * initializes the class - loads all the required resources
	**/
	CCandies(CGame *cg_, CfgFile &cfg);

	/**
	 * memory cleanup
	**/
	~CCandies();

	/**
	 * renews all candies, that are specific (and probably different) to each car; this is used if new target car (CGame::cam_tgt) is selected
	**/
	void RenewAllSpecific();

	/**
	 * shows startplaces
	**/
	void StartPlaces_Show();

	/**
	 * hides startplaces
	**/
	void StartPlaces_Hide();

	/**
	 * Informs class that a checkpoint has been crossed.
	 * \n The class shows/hides other checkpoints/finish - according to checkpoint_order.
	 * @param car id of car, which crossed the checkpoint
	 * @param checkpoint index of checkpoint (negative number for finish)
	 * @param time time, when the event occured
	 * @see checkpoint_order
	**/
	void Checkpoint_Crossed(int car, int checkpoint, irr::u32 time);

	/**
	 * Informs class that the finish has been crossed.
	 * \n The class shows/hides other checkpoints/finish - according to checkpoint_order.
	 * @param car id of car, which crossed the checkpoint
	 * @param laps number of laps the car made
	 * @param time time, when the event occured
	 * @see checkpoint_order
	**/
	void Finish_Crossed(int car, int laps, irr::u32 time);
private:
	CGame *cg; //!<reference to parent class

	std::deque<irr::scene::ISceneNode *> startPlaces; //!<irrlicht nodes of startplaces
	bool startPlaces_shown;  //!<if true, startplaces are shown; we don't really need this, but it speeds up things a little bit (i.e. it's an optimalization :-))

	std::deque<irr::scene::ISceneNode *> checkpoints;  //!<irrlicht nodes of checkpoints
	
	int *players_order; //!<contains indices of players sorted by the position in game
	
	/**
	 * Restorts players_order according players' positions in game (number of laps, checkpoints crossed, time when the last checkpoint was crossed etc.)
	**/
	void PlayersResort();

	/**
	 * If RANDOM, all checkpoints are initially visible, they dissapear as player crosses them; when all checkpoints are crossed, finish gets visible.
	 * \n If SEQUENTIAL, all checkpoints are initially hidden; on a time only one (the next to be crossed) checkpoint is visible - when player crosses it, it will get invisible, and the next one will get visible; finish gets visible after crossing the last checkpoint.
	 * \n When user crosses finish, everything will go again into initial state.
	 * @see Checkpoint_Crossed
	**/
	enum {
		RANDOM, SEQUENTIAL
	} checkpoints_order;

	irr::scene::ISceneNode *node_finish; //!<irrlicht node of finish
};

#endif //_CCANDIES_H_
