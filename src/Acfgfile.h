/**
 * \file
 * This file contains declaration CfgFile class.
**/

#ifndef _ACFGFILE_H_
#define _ACFGFILE_H_

#include <map>
#include <string>
#include <deque>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/vector3d.h>

/**
 * This class encapsulates reading of the game's configuration files. Each setting in the file is represented as "Setting1/Setting2/ValueName" (used by Getval_* functions)
**/
class CfgFile {
private:
	std::map<std::string, std::string> cfg;
public:
	CfgFile() {};
	/**
	 * loads the configuration from file to the memory.. Then other functions may be used...
	**/
	CfgFile(irr::io::IFileSystem *fs, std::string path) {Load(fs, path);}
	~CfgFile() {};

	/**
	 * loads the configuration from file to the memory.. Then other functions may be used...
	**/
	bool Load(irr::io::IFileSystem *fs, std::string path);

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is a default returned value, if the setting isn't found.
	**/
	std::string Getval_str(std::string path, std::string def="");
	
	/**
	 * you should probably better never use it.. By this function you may change some setting (just in the memory!). This function returns true, if the setting is present and changed; false otherwise.
	**/
	bool Setval_str(std::string path, std::string val);

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is a default returned value, if the setting isn't found.
	 * @return parsed value of the setting (as a integer)
	**/
	int Getval_int(std::string path, int def=0);

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is a default returned value, if the setting isn't found.
	 * @return parsed value of the setting (as a double)
	**/
	double Getval_double(std::string path, double def=0);

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is a default returned value, if the setting isn't found.
	 * @return parsed value of the setting (as a vector)
	**/
	irr::core::vector3df Getval_vector(std::string path, irr::core::vector3df def=irr::core::vector3df(0, 0, 0));

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is a default returned value, if the setting isn't found.
	**/
	unsigned int Getval_flags(std::string path, std::map<std::string,unsigned int> vals, unsigned int def=0);

	/**
	 * this function is used for retrieving settings from the configuration file. First parameter is the setting 'path', the second is the flag about which you want to know, whether it's used (specified).
	 * @return true, if flag present; false otherwise (even if the key doesn't exist)
	**/
	bool Getval_flag(std::string path, std::string flag);

	/**
	 * @return true if the value is set, false otherwise
	**/
	bool Getval_exists(std::string path);
};

#endif //_ACFGFILE_H_
