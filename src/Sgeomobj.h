/**
 * \file
 * This file contains declaration of SGeomObj and dGeomWrapper classes.
**/

#ifndef _SGEOMOBJ_H_
#define _SGEOMOBJ_H_

#include <ode/ode.h>
#include <irrlicht/irrlicht.h>

/**
 * This is a simple wrapper around dGeom class, that enables simpler use of geom objects inside of SGeomObj (it doesn't have to deal with dGeomID handles to the geom objects)
**/
class dGeomWrapper : public dGeom {
  // intentionally undefined, don't use these
  dGeomWrapper (dGeomWrapper &);
  void operator= (dGeomWrapper &);

public:
  dGeomWrapper (dGeomID geom)
    { _id = geom; }
};

/**
 * A generic struct containing basic informations about a geometric object in ODE simulation. Each geom in ODE has a struct like this associated with it.
**/
struct SGeomObj {
public:
	enum SGeomObj_TYPE {
		TERRAIN,
		CAR_CHASSIS,
		CAR_WHEEL,
		GAMEPLACE,
	} type; //!< type of child
	
	int surface_type;

	/**
	 * constructor, just initializes type and surface_type
	**/
	SGeomObj(SGeomObj_TYPE t, int s=0):
		surface_type(s),type(t),
		ode_geom(0),
		ode_body(0)
		{}

	/**
	 * virtual destructor, does nothing
	**/
	virtual ~SGeomObj() {
		if (ode_geom) delete ode_geom;
		if (ode_body) delete ode_body;
	}

	/**
	 * Moves and rotates the geom or body (if exists) of object.
	**/
	void SetPosRot(dReal x, dReal y, dReal z, dReal rx, dReal ry, dReal rz) {
		//position/rotation of the body (assigned to current the geom) - if any - will be be updated automatically by ode...
		ode_geom->setPosition(x, y, z);
		
		irr::core::vector3df v(rx, ry, rz);
		v=v/180*M_PI;
		dMatrix3 R;
		dQuaternion q;
		irr::core::quaternion qi(v.X, v.Y, v.Z);
		q[0]=qi.W; q[1]=qi.X; q[2]=qi.Y; q[3]=qi.Z;
		dQtoR(q, R);

		ode_geom->setRotation(R);
	}

	dGeom *ode_geom; //!< specific to the ODE simulation - contains handle for the simulation objects (in case ode_geom!=0, the ode_geomid==0)
	dBody *ode_body; //!< specific to the ODE simulation - contains handle for the simulation objects (in case ode_body!=0, the ode_bodyid==0)
};

#endif //_SGEOMOBJ_H_
