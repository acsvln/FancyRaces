/**
 * \file
 * This file contains declarations of SSimulation_event, SSimulation_surface_contact_properties and SSimulation classes.
**/

#ifndef _SSIMULATION_H_
#define _SSIMULATION_H_

#include <ode/ode.h>
#include <ode/odecpp.h>

#include <string>
#include <deque>
#include <utility>
#include <irrlicht/irrlicht.h>
#include "Sgeomobj.h"
#include "Sobject.h"
#include "Svehicle.h"
#include "Sgameplace.h"

/**
 * describes events occured by performing simulation step - used by server
**/
struct SSimulation_event {
	enum  {
		CROSSED_CHECKPOINT,
		CROSSED_FINISH,
		RACE_FINISHED,
	} type; //!<type of event
	union {
		/**
		 * used if event type is CROSSED_CHECKPOINT or CROSSED_FINISH - informs which car crossed which checkpoint/finish
		**/
		struct {
			int vehicleId;
			int checkpoint;
		} crossCheckpoint;
		struct {
			int vehicleId;
			int laps;
		} crossFinish;
	};
};

/**
 * Structure describes properties of contact of two surfaces.
**/
struct SSimulation_surface_contact_properties {
	dReal mu; //!< member mu of the dSurfaceParameters (ODE)
	dReal slip; //!< slipperyness of the surfaces' contact
	dReal damp; //!< dampiness of the contact
	dReal spring; //!< springyness of the contact
};

/**
 * Encapsulates the simulation stuff
**/
class SSimulation {
public:
	/**
	 * constructor - currently just initializes irrlicht engine
	 * @param - irrlicht device for the server to work with
	**/
	SSimulation(irr::IrrlichtDevice *device);
	
	/**
	 * frees memory allocated by the class
	**/
	~SSimulation();

	/**
	 * parses the configuration file and loads the world into the ODE simulation.
	**/
	void SetWorld(std::string file);

	/**
	 * adds a car with specifiec parameters to the simulation, and returns it's id used by the class.
	**/
	int AddPlayer(std::string carprofile, const dVector3 &pos);
	
	/**
	 * if the user with such id exists, it sets his attributes to the v parameter and returns true. Otherwise (if the given id doesn't exist) function returns false.
	**/
	bool GetPlayerInfo(unsigned int id, SVehicle **v);

	/**
	 * performs one step of the simulation (according to the last time, when the method was executed). Returns true, if simulation has been performed. False otherwise.
	**/
	bool Step();

	/**
	 * prepares the simulation for start (initializes timer etc.)
	**/
	void Start();

	/**
	 * returns configuration of the current world
	**/
	CfgFile* getCfgFile(void) {return &cfg;}

	/**
	 * @return returns time, when the last simulation occured
	**/
	irr::u32 GetSimTime() {return lasttime-starttime;}

	/**
	 *	Returns pointer to moveableObjects variable.
	**/
	std::deque<const SObject*> &GetMoveableObjects() {
		return moveableObjects;
	}

	/**
	 * Sets the number of laps each car has to make until the round's finished
	**/
	void SetLaps(int _laps) {laps=_laps;}

	std::deque<SSimulation_event> events; //!<events occured by perfoming Step()

	/**
	 * Removes car from the list of playing cars (used when player disconnects - the car remains in simulation, but we don't care, whether it crosses finish).
	**/
	void RemoveFromRace(int simid);
private:
	double currentStepSize; //!< simulation step size used in nearCallback

	/**
	 * callback for the ODE simulation - generates joints between bodies
	**/
	static void nearCallback (void *data, dGeomID o1, dGeomID o2);

	dWorld odeWorld; //!< object used by ODE library.
	dHashSpace odeSpace; //!< object used by ODE library.

	std::map<std::string, int> surfaces; //!< conversion table from name to index
	SSimulation_surface_contact_properties **surfaceContactProperties; //!< matrix of parameters of contacts between two materials (two dimensional matrix - surfaceContactProperties[surfaces.size][surfaces.size])

	std::deque<SVehicle*> vehicles; //!< contains list of all cars used in simulation.

	dSimpleSpace checkpoints_space; //!< ODE space containing checkpoint objects
	std::deque<SGameplace*> checkpoints_objects; //!< list of checkpoint objects
	enum {
		RANDOM, SEQUENTIAL
	} checkpoints_order; //!< order, how user has to go through checkpoints before he can cross finish
	SGameplace *checkpoint_finish; //!< finish object
	
	int laps; //!< number of laps each car has to make until the round's finished

	irr::u32 starttime; //!< time, when the simulation started
	irr::u32 lasttime; //!< time of the last step performed
	std::deque<SObjectGroup*> terrain; //!< list of terrain objects
	std::deque<const SObject*> moveableObjects; //!< List of objects, which may move during the simulation (excluding vehicles). This is just a subgroup of terrain
	dJointGroup contactgroup; //!< ODE joint group (used in simulation)
	CfgFile cfg; //!< terrain configuration file

	irr::IrrlichtDevice *device; //!< irrlicht device, through which we use some functionalities of irrlicht

	int currentFinish; //!< counter of cars crossed finish line (after they made all laps)
};

#endif //#_SSIMULATION_H_
