/**
 * \file
 * This file contains implementation of CCarModel class.
**/

#include "CModels.h"

using namespace std;

scene::ISceneNode* CCarModel::loadModel(CfgFile &cfg, string what, IrrlichtDevice *device, scene::ISceneManager* sm, video::IVideoDriver* driver)
{
	scene::ISceneNode* node=0;
	if (cfg.Getval_str(what+"model", "")!="") {
		std::string cwd=device->getFileSystem()->getWorkingDirectory().c_str();
		device->getFileSystem()->changeWorkingDirectoryTo(("data/cars/"+profile).c_str());

		scene::IAnimatedMesh* mesh=sm->getMesh(cfg.Getval_str(what+"model", "").c_str());
		if (mesh) {
			node=sm->addAnimatedMeshSceneNode(mesh);
			node->setMaterialFlag(video::EMF_LIGHTING, false);
			//node->setDebugDataVisible(true);
			node->setScale(
				cfg.Getval_vector(what+"scale", core::vector3df(1, 1, 1))
				);
			/*body.arot=core::vector3df((f32)cfg.Getval_double("Look/Body/Rotation_x", 0),
				(f32)cfg.Getval_double("Look/Body/Rotation_y", 0),
				(f32)cfg.Getval_double("Look/Body/Rotation_z", 0));*/
		}
		device->getFileSystem()->changeWorkingDirectoryTo(cwd.c_str());
	} else {
		node=0;
	}
	return node;
}

void CCarModel::Load(IrrlichtDevice *device, scene::ISceneManager* sm, video::IVideoDriver* driver)
{
	CfgFile cfg;
	string buf;

	DBGCOUT("CCarModel", "Load", "Loading car " + profile);
	if (!cfg.Load(device->getFileSystem(), ("data/cars/"+profile+"/config.ini")))
		DBGCOUT("CCarModel", "Load", "Couldn't load "+profile);

	//load car

	body.node=loadModel(cfg, "Look/Body/", device, sm, driver);

	body.node_debug=sm->addCubeSceneNode(1, 0, -1, core::vector3df(0, 0, 0), core::vector3df(0, 0, 0),
		cfg.Getval_vector("Body/Chassis/1/scale", core::vector3df(1, 1, 1))
		);
	body.node_debug->setMaterialTexture(0, driver->getTexture("data/misc/checked.jpg"));

	for (int i=0; ; i++) {
		char buf2[128];

		sprintf(buf2, "Body/Wheel_%d/", i+1);
		buf=buf2;

		if (!cfg.Getval_exists(buf+"attr")) break;

		double radius=cfg.Getval_double((string)buf+"radius", cfg.Getval_double("Body/Wheel/radius", 1));
		double width=cfg.Getval_double((string)buf+"width", cfg.Getval_double("Body/Wheel/width", 1));

		scene::ISceneNode* node=loadModel(cfg, "Look/Wheels/", device, sm, driver);
		CModelAttr wm;

		scene::IMesh* cm=CreateCylinder(25, 2, 1);
		scene::ISceneNode* node_debug=sm->addMeshSceneNode(cm);
		node_debug->setScale(core::vector3df((f32)(radius), (f32)width, (f32)(radius)));
		node_debug->setMaterialTexture(0,	driver->getTexture("data/misc/checked.jpg"));
		node_debug->getMaterial(0).EmissiveColor.set(255,255,255,255);

		wm.arot=core::vector3df((f32)cfg.Getval_double(buf+"rotation_x", 0),
				(f32)cfg.Getval_double(buf+"rotation_y", 0),
				(f32)cfg.Getval_double(buf+"rotation_z", 0));
		wm.node=node;
		wm.node_debug=node_debug;

		wheels.push_back(wm);
	}
	maxgear=cfg.Getval_int("Dynamics/Gearbox/gear_count", 0);
	gearN=cfg.Getval_int("Dynamics/Gearbox/gear_N", 0);
	gearR=cfg.Getval_int("Dynamics/Gearbox/gear_R", 0);
	maxrpm=cfg.Getval_int("Dynamics/Engine/torque_4x", 0);
	minrpm=cfg.Getval_int("Dynamics/Engine/torque_1x", 0);
	debugTextNode=sm->addTextSceneNode(device->getGUIEnvironment()->getBuiltInFont(), 
			L"DEBUG TEXT", 
			video::SColor(255,255,100,50), body.node_debug, core::vector3df(0, 2, 0));

	//load sounds
	try {
		snd_engine = new openalpp::Source(("data/cars/"+profile+"/"+cfg.Getval_str("Sound/Engine/wave")).c_str());
		if (!snd_engine.valid())
			DBGCOUT("ALUT", "Coulnd't load file", ("data/cars/"+profile+"/"+cfg.Getval_str("Sound/Engine/wave")).c_str());
		else {
			snd_engine->setGain(1);
			snd_engine->setPosition(0.0,0.0,0.0);
			snd_engine->setLooping(true);
		}
		snd_engine_pitch_low=cfg.Getval_double("Sound/Engine/pitch_low");
		snd_engine_pitch_high=cfg.Getval_double("Sound/Engine/pitch_high");
	} catch(openalpp::Error e) {
		std::cerr << e << "\n";
	}
}

void CCarModel::show_debug(bool show)
{
	body.node_debug->setVisible(show);
	for (std::deque<CModelAttr>::iterator j=wheels.begin(); j!=wheels.end(); j++) {
		j->node_debug->setVisible(show);
	}
	debugTextNode->setVisible(show);
}

void CCarModel::soundStart()
{
	if (snd_engine.valid()) snd_engine->play();
}

void CCarModel::soundStop()
{
	if (snd_engine.valid()) snd_engine->stop();
}

void CCarModel::soundSetProperties()
{
	if (snd_engine.valid()) {
		float p=(float)((snd_engine_pitch_low+double(r_rpm-minrpm)*(snd_engine_pitch_high-snd_engine_pitch_low)/double(maxrpm-minrpm) ));
		snd_engine->setPitch((ALfloat)p);
		core::vector3df pos=body.node->getPosition();
		snd_engine->setPosition(pos.X, pos.Y, pos.Z);
		//cerr << pos.X << " "<< pos.Y << " "<< pos.Z << endl;
		//irr::core::vector3df vel=(body.posf-body.pos)*(f32)3.6/(body.dtime/(f32)1000.0);
//		snd_engine->setVelocity(vel.X, vel.Y, vel.Z);
	}
}
