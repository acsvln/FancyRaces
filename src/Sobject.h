/**
 * \file
 * This file contains declaration of SObject and SObjectGroup classes.
**/

#ifndef _STERRAIN_H_
#define _STERRAIN_H_

#include <ode/ode.h>
#include <string>
#include <iostream>
#include <fstream>
#include <deque>
#include "Acfgfile.h"
#include "Sgeomobj.h"
#include <irrlicht/irrlicht.h>

struct SObjectGroup;

/**
 * contains informations about each sub-object of SObjectGroup
 * in case it is made of trimesh, the ode_*id variables of parent class are used instead of "nonid" ones
 * @see SObjectGroup
**/
struct SObject:public SGeomObj {
public:
	/**
	 * zeroes out critical variables
	**/
	SObject():SGeomObj(TERRAIN),vertices(0),indices(0) {}

	/**
	 * tries to release as much memory used by the class as possible
	**/
	~SObject() {if (vertices) delete[] vertices; if (indices) delete[] indices;}

	/**
	 * initializes the geom variable (by creating object in the specified space) from vertices/indices (used when creating trimesh object)
	 * @param space ODE space, into which the object will be inserted
	 * @return true on success, false otherwise
	**/
	bool Init(dSpace &space);

	dVector3 *vertices; //!< vertex array for trimesh geom
	int *indices; //!< index array for trimesh geom
	int vertexcount; //!< number of vertices in the vertex array
	int indexcount; //!< number of indices in the index array
};

/**
 * Contains list of object parts; it just initializes the list of STerrain_part classes - just used for memory management and initialization
**/
struct SObjectGroup {
	std::deque<SObject*> parts; //!<parts of the object (buildings, roads, grass, trees...)
public:
	/**
	 * initializes parent class and zeroes out critical variables
	**/
	SObjectGroup() {}

	/**
	 * tries to release as much memory used by the class as possible
	**/
	~SObjectGroup();

	/**
	 * initializes objects' data (i.e. loads object and places it into the simulation space)
	 * @param map name of the map
	 * @param cfg configuration file
	 * @param incfgpath path of the object configuration (in cfg object) to be initialized
	 * @param space space, into which the object will be put to
	 * @param world world, into which the object will be put to (used if the object will be moveable - i.e. will have some weight etc.)
	 * @param sm scene manager object (used to load/parse 3ds files)
	 * @param mo deque, where copies of pointers to moveable objects will be stored
	 * @param surfaceTypes reference to SSimulation::surfaces
	 * @return true on success; false if anything went wrong (couldn't open file etc.)
	**/
	bool Load(const std::string &map, CfgFile &cfg, const std::string &incfgpath, dWorld &world, dSpace &space, irr::scene::ISceneManager* sm, std::deque<const SObject*> &mo, std::map<std::string, int> &surfaceTypes);
private:
	/**
	 * initializes the object data from heightmap
	 * @return true on success; false otherwise
	**/
	bool MakeFromHeightmap(irr::scene::ITerrainSceneNode* terrain, dSpace &space, std::map<std::string, int> &surfaceTypes);

	/**
	 * initializes the object data from mesh
	 * @return true on success; false otherwise
	**/
	bool MakeFromMesh(irr::scene::IMesh* mesh, dSpace &space, CfgFile &cfg, const std::string &cfg_path, std::map<std::string, int> &surfaceTypes);
};

#endif //_STERRAIN_H_
