/**
 * \file
 * This file contains declarations of SVehicle_wheel, SVehicle_chassis and SVehicle classes.
**/

#ifndef _SVEHICLE_H
#define _SVEHICLE_H

#include <ode/ode.h>
#include <string>
#include <iostream>
#include <fstream>
#include <deque>
#include "Acfgfile.h"
#include "Sgeomobj.h"

/**
 * This enum declares constants used in classes declared in this file
**/
enum {
	WHEEL_STRAIGHT=1, //!< a not-steering wheel
	WHEEL_STEER=2,  //!< a steering wheel
	WHEEL_REVERSED=4, //!< the wheel is steering in the opposite way, than the user wants to go
	WHEEL_THURST=8, //!< the wheel makes use of engine force (torque) to move the car

	TORQUECURVENO=100, //!< number of points the engine torque courve is interpolated to
};

class SVehicle;

/**
 * Contains all informations about a wheel required for simulation
**/
struct SVehicle_wheel:public SGeomObj {
	dHinge2Joint *jid; //!< id of the joint connecting wheel with chassis

	double radius; //!< radius of the wheel
	double width; //!< width of the wheel
	unsigned int attr; //!< bitarray of attributes of the wheel (WHEEL_STRAIGHT, WHEEL_STEER etc.)
	double brakes; //!< value, of how much of total braking power goes to this wheel
	bool left; //!< if true, it is wheel on the left side of the car (used when calculating slip coefficients)

	SVehicle *belongs_to; //!< pointer to the 'parent' vehicle

	/**
	 * initializes parent class and zeroes out critical variables
	**/
	SVehicle_wheel():SGeomObj(CAR_WHEEL),jid(0) {}

	/**
	 * destructor, tries to release as much memory used by the class as possible
	**/
	~SVehicle_wheel() {if (jid) delete jid;}

	/**
	 * loads wheel configuration
	 * @param cfg configuration file to load from
	 * @param buf2 prefix of configuration for the wheel
	 * @param vehicle parent vehicle
	**/
	void Load(dWorld &world, dSpace &space, CfgFile &cfg, std::string buf2, SVehicle *vehicle, const dVector3 &pos, std::map<std::string, int> &surfaceTypes);

	dReal kp; //!< spring constant (for use in ODE simulation)
	dReal kd; //!< damping constant (for use in ODE simulation)
};

/**
 * Contains all informations about a car chassis required for simulation
**/
struct SVehicle_chassis:public SGeomObj {
	std::deque<dGeom*> parts;
	double totalWeight;

	SVehicle *belongs_to;

	/**
	 * initializes parent class and zeroes out critical variables
	**/
	SVehicle_chassis():SGeomObj(CAR_CHASSIS),totalWeight(0) {}

	/**
	 * tries to release as much memory used by the class as possible
	**/
	~SVehicle_chassis() {
		while (!parts.empty()) {
			delete *parts.begin();
			parts.pop_front();
		}
	}

	/**
	 * loads chassis configuration
	 * @param cfg configuration file to load from
	 * @param buf2 prefix of configuration for the wheel
	 * @param vehicle parent vehicle
	**/
	void Load(dWorld &world, dSpace &space, CfgFile &cfg, SVehicle *vehicle, const dVector3 &pos, std::map<std::string, int> &surfaceTypes);
};

/**
 * Encapsulates informations about a car in ODE simulation
**/
class SVehicle {
public:
	SVehicle_chassis *chassis; //!< contains handle to car body intormation structure...

	std::deque<SVehicle_wheel> wheels; //!< contains wheel informations for the simulation

	dJointGroup *contactgroup; //!< specific to the ODE simulation - contains handle for the simulation objects
	dSpace *car_space; //!< specific to the ODE simulation - contains handle for the simulation objects

	double c_steer; //!< desired controls' parameters (steering)
	double c_accel; //!< desired controls' parameters (acceleration)
	double c_brakes; //!< desired controls' parameters (brakes pressed)
	double c_handbrake; //!< desired controls' parameters (handbrake pressed)
	double c_clutch; //!< desired controls' parameters (clutch pressed)
	double c_rescue; //!< desired controls' parameters (rescue force used)
	int c_gear; //!< desired controls' parameters (gear)
	
	double r_steer; //!< controls' parameters (steering) really used for the simulation - they are actual after calling the ApplyControls() method
	double r_accel; //!< controls' parameters (acceleration) really used for the simulation - they are actual after calling the ApplyControls() method

	double *gearbox; //!< array of gearbox ratios
	int gearbox_no; //!< size of the gearbox array
	int gearbox_N; //!< index of neutral gear in the gearbox array
	int gearbox_R; //!< index of reaverse gear in the gearbox array
	double brakes_maxforce; //!< maximal force of the brakes used (per each wheel)
	double handbrake_maxforce; //!< maximal force of the handbrake
	double differential; //!< differential ratio
	double trans_eff; //!< transmission efficiency
	double breaking_power; //!< the coefficient determining, how good the engine lowers vehicle speed (the torque used to slow down is computed as the negative of normal torque multiplied by this coefficient)
	
	int rpm_min; //!< minimal engine rpm
	int rpm_max; //!< maximal engine rpm
	double *torquecurve; //!< array of 4 values representing a bezier courve of the torque

	int nlaps; //!< number of laps made by the car
	bool *checkpoints; //!<contains list of checkpoints, which the car went through...

	int simid; //!<"backreference" - contains index of car in the SSimulation->vehicles array (mainly used by sending messages from simulation loop back to server)

	int finishorder; //!< order of the car in the race end (indexed from 0)

	bool removeFromRace; //!< if true, the vehicle is removed from list of playing cars (used when player disconnects - the car is still in simulation, but not controled - and thus most probably it never gets to the finish. Because of that we don't care of the car status.)
	double aeroDrag; //!< aerodynamic drag constant
	double rollResistance; //!< rolling resistance of 1 wheel

public:
	/**
	 * Initializes the object. This means reading the configuration from the config file and creating ODE objects (car body and wheels).
	**/
	SVehicle(dWorld &_world, dSpace &_space, irr::io::IFileSystem *ifs, std::string config, const dVector3 &pos, int ncheckpoints, std::map<std::string, int> &surfaceTypes);

	/**
	 * Deinitializes the object (mainly frees the memory occupied)
	**/
	~SVehicle();

	/**
	 * According to the c_* variables, parameters of the car's objects in the ODE simulation are set.
	 * @param stepsize length (time) of the the current step - used by calculating erp/cfm
	**/
	void ApplyControls(double stepsize);

	/**
	 * returns a point from the torque curve
	 * @param point a value in range 0.0 - 1.0 of the bezier curve (the t parameter :o)
	**/
	double GetTorqueAt(double point);

	/**
	 * returns a point from the torque curve
	 * @param point a value from rpm_min to rpm_max (internally it's normalized to 0.0-1.0 and sent to the other variant of this function)
	**/
	double GetTorqueAt(int point);

	/**
	 * returns current gear ratio - ratio between rotation of engine and rotation of wheels (uses clutch, gear, differential etc.)
	**/
	double GetCurrentGearRatio();

	/**
	 * This function returns current rpm; if engine parameter is true, it's rpm of the engine, otherwise it's rpm of the non-thurst wheels.
	**/
	int GetRPM(bool engine=true);

	/**
	 * returns current speed in km/h
	**/
	int GetSpeed();
};

#endif
