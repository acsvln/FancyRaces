/**
 * \file
 * This file contains declaration of CGame and CGameCameraPos classes.
**/

#ifndef __C_GAME_H_INCLUDED__
#define __C_GAME_H_INCLUDED__

#if !defined(WIN32) && !defined(_XBOX) && !defined(OS2) && !defined(MACOS)
#define LINUX
#endif

#include "Aasynccomm.h"

#include <irrlicht/irrlicht.h>
#include "main.h"
#include "Autil.h"
#include "Acfgfile.h"
#include <openalpp/alpp.h>
#include "CCandies.h"
#include "CModels.h"

using namespace irr;
const int CAMERA_COUNT = 7;

/**
 * Handles the camera movement (position or look-at point)
**/
class CGameCameraPos {
public:
	//constructor...
    CGameCameraPos(long time_lagged);

    // Sets the current position of the camera
	void setCurrentPosition(core::vector3df current_position, u32 realtime, u32 w);

    // Gets the smoothed position
    core::vector3df getSmooothedPosition(u32 realtime);

    // Clear all the positions data
    void clear();

	void setTimeLagged(long tl) {time_lagged=tl;}

private:
    // Time to hold old camera poitions
    long time_lagged;

    // A structure descibing the camera's position at a certain time.
    struct camera_details {
		core::vector3df pos;
        long time;
		long weight;
    };

    std::deque <camera_details> camera_positions;
};

/**
 * Handles all the gameplay stuff - showing graphics interface, processing user input, communicating with server etc.
**/
class CGame : public IEventReceiver
{
public:
	/**
	 * constructor, initializes some internals
	 * @param spectate if true, the class won't try to control any car - instead it will let user just to watch the race
	**/
	CGame(AGameSettings* ags, bool spectate, bool admin);

	~CGame();

	/**
	 * Initializes the irrlicht library and depending on which scene is active it loops calls to doScene0() and doScene2(). In the end, it closes the irrlicht device. When it is required, the switchToNextScene() function is called to switch to next scene :-)
	 * @return 0 if window is closing, 1 if user wanted to quit, 2 if server initiated the quit
	**/
	int run();

	/**
	 * Processes irrlicht events - things like keypresses etc. (exits the game cycle on esc keypress, sets the kbd_states array, which is used by processControls())
	**/
	virtual bool OnEvent(const SEvent &event);

	/**
	 * returns pointer of the player order GUI object (used by CCandies)
	**/
	gui::IGUIStaticText* GetGUIPlayerOrder() {return playerOrder;}

	/**
	 * return constant reference to internal deque, containing list of results
	**/
	const std::deque<std::string> &GetResults() {return results;}
private:
	/**
	 * Draws load screen (sets background color, initializes the fadein effect, draws text).
	**/
	void doScene0_introScreen();

	/**
	 * This function simply waits until the server is ready to go (i.e. until it receives GETREADY message from server). After that it switches to the next scene.
	**/
	void doScene1_serverWait(video::IVideoDriver* driver, scene::ISceneManager* smgr, gui::IGUIEnvironment* guienv, gui::IGUIFont* font); //load server

	/**
	 * This function reads all the messages from the server until the WAITING message. It stores world name (WORLD message), creates car objects (with configuration file paths and IDs - CAR messages).
	 * \n Also stores the ID of current player (YOURID message). After messages are processed, the whole graphics scene is created (terrain, car models) - using the already known settings.
	 * \n Finally a READY message is sent to the server.
	**/
	void doScene2_loadData();

	/**
	 * Procedure called from inside of doScene2_loadData. Loads map (using data from current directory), according to config file.
	**/
	void doScene2_loadData_loadMap(CfgFile &cfg, video::IVideoDriver* driver, scene::ISceneManager* sm);

	/**
	 * This is the main scene where the game is processed. Eeach run of the function does the following: First, user input is processed (processControls()) and then sent to the server (by CONTROL message).
	 * \n Then, messages from server are processed (namely messages PLAYER (and 'submessages' POSROT and CONTROL), PONG). Next steps are applying the movement and rotation to the scene objects (CCarModel::move()) and at the last drawing the scene (drawing of car and environment is done by irrlicht, the UI is up to this function).
	**/
	void doScene3_main(video::IVideoDriver* driver, scene::ISceneManager* smgr, gui::IGUIEnvironment* guienv, gui::IGUIFont* font); //gameplay

	/**
	 * Does the scene switching. There are currently 4 scenes:
	 * \n first -1 (loading screen, createLoadingScreen()),
	 * \n 0 (wait until server starts, another handler is doScene0()),
	 * \n 1 (load scene loadSceneData()),
	 * \n 2 (game, handler for this scene is called from run() function; in this place, only a camera is created and a fade-in from loading scene to game scene is started)
	**/
	void switchToNextScene();
	
	/**
	 * Reads the controls (like which keys are pressed etc.), and according to that it updates the c_* variables for own CCarModel.
	**/
	void processControls();

	/**
	 * Process the "PLAYER" message from server. This message informs clients about movement of vehicles.
	 * @return id of player processed
	**/
	int processMessage_PLAYER(cmd_map &cmd);

	/**
	 * Process the "SIMSTATUS" message from server. This message informs clients about checkpoint crossings etc.
	**/
	void processMessage_SIMSTATUS(cmd_map &cmd);

	/**
	 * Process the "OBJECTS" message from server. This message informs clients about movement of objects (excluding vehicles) in the scene
	**/
	void processMessage_OBJECTS(cmd_map &cmd);

	IrrlichtDevice *device; //!< pointer to the irrlicht device

	/**
	 * current scene of the game (e.g. loading screen, gameplay)
	 * @see switchToNextScene
	**/
	int currentScene;

	video::SColor backColor; //!< global background color

	gui::IGUIStaticText* statusText; //!< control containing status text
	gui::IGUIInOutFader* inOutFader; //!< irrlicht in-out fader (for fade between loading and gameplay scene)
	gui::IGUIStaticText* playerOrder; //!< control containing players' order

	scene::ISceneNode* skyboxNode; //!< irrlicht node containing skybox

	u32 sceneStartTime; //!< time, when current scene started (used by switchToNextScene() )
	int timeForThisScene; //!< lenght of the current scene (used by switchToNextScene() )

	AGameSettings *ags; //!< the settings received from the CMainMenu class

	//camera
	f32 cam_distance; //!< camera distance from target
	core::vector3df cam_rot; //!< camera rotation "offset"
	int cam_tgt; //!< camera's target car
	CGameCameraPos cam_lookat; //!< class managing position of the camera target
	CGameCameraPos cam_pos; //!< class managing the camera current position

	std::deque<CCarModel> carmodels; //!<list of all car models in the game
	int mymodelid; //!< id of players own vehicle (not used if in spectator mode)

	std::deque<CTerrainPart> terrainParts; //!< deque of moveable terrain parts

	video::ITexture *rpm_img_1, *rpm_img_2, *digits_spd, *digits_gbx;
	int rpm_img_w, rpm_img_h, rpm_img_x, rpm_img_y;
	int digits_spd_x, digits_spd_y, digits_spd_w, digits_spd_h, digits_gbx_x, digits_gbx_y, digits_gbx_w, digits_gbx_h;

	irr::u32 sim_time; //!< current time of simulation

	bool show_debuginfo; //!<if true, prints debug info (key J)
	bool show_controls; //!<if true, displays controls (key K)
	bool show_simdebug; //!<if true, shows real simulation boxes of cars' chassis etc.

	int quitGame; //!< non-zero if the run() function should quit; 1: user wants to quit, 2: server wants to quit

	bool kbd_states[KEY_KEY_CODES_COUNT]; //!< list of key states
	unsigned char kbd_states_fti[KEY_KEY_CODES_COUNT]; //!<frames to ignore - how many frames the key will be ignored

	friend class CCandies;
	CCandies *candies; //!< class for 'candies' in the game (like checkpoints, starting places, players order list)

	openalpp::ref_ptr<openalpp::Listener> snd_listener; //!< object containing listener

	std::deque<std::string> results; //!< string containing game results (available after a successful end of game)

	gui::IGUIWindow *closeWindow; //!< irrlich window handle (of the exit-game-prompt window)
	gui::IGUIWindow *infoWindow; //!< irrlich window handle (used when server is restarting to inform user about why is he waiting)

	bool spectatorMode; //!< if true, the class is in spectator mode (no car is controlled)

	s32 simOffsetTime; //!< time, when the first position packed was received

	bool adminMode; //!< true, if the player has administrator rights for the game
};

#endif
