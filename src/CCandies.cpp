/**
 * \file
 * This file contains implementation of CCandies class.
**/


#include "CGame.h"
#include "CCandies.h"

using namespace std;

#define STR_PLR_ORDER "::Players' order"

CCandies::CCandies(CGame *_cg, CfgFile &cfg):
cg(_cg)
{
	irr::scene::ISceneManager *sm=cg->device->getSceneManager();
	int i;
	string s;

//INITIALIZE STARTING PLACES
	for (i=0; unsigned(i)<cg->carmodels.size(); i++) {
		s="Startingplaces/Start_"+numtostring(i+1)+"/";
		irr::scene::ISceneNode *sn=sm->addCubeSceneNode(1, 0, -1, cfg.Getval_vector(s+"pos")-irr::core::vector3df(0,0.5,0), cfg.Getval_vector(s+"rot"),
			core::vector3df((irr::f32)0.1, 0.5, 2.5)
			);
		sn->setMaterialTexture(0, cg->device->getVideoDriver()->getTexture("data/misc/startplace.jpg"));
		sn->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
		sn->setVisible(false);
		startPlaces.push_back(sn);
	}

//INITIALIZE CHECKPOINTS
	for (i=0;; i++) {
		s="Checkpoints/Check_"+numtostring(i+1)+"/";
		if (cfg.Getval_exists(s+"pos")) {
			irr::scene::ISceneNode *sn=sm->addCubeSceneNode(1, 0, -1, cfg.Getval_vector(s+"pos"), cfg.Getval_vector(s+"rot"), cfg.Getval_vector(s+"size"));
			sn->setMaterialTexture(0, cg->device->getVideoDriver()->getTexture("data/misc/checkpoint.png"));
			sn->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
			sn->setVisible(true);
			checkpoints.push_back(sn);
		} else break;
	}
	if (cfg.Getval_str("Checkpoints/order", "sequential")=="random") checkpoints_order=RANDOM;
	else checkpoints_order=SEQUENTIAL;
	//initialize and clear out all checkpoints for all cars
	for (std::deque<CCarModel>::iterator it=cg->carmodels.begin(); it!=cg->carmodels.end(); it++) {
		it->checkpoints.clear();
	}

//INITIALIZE START/FINISH
	node_finish=0;

	//init finish
	if (cfg.Getval_exists("Checkpoints/Finish/pos")) {
		s="Checkpoints/Finish/";
		node_finish=sm->addCubeSceneNode(1, 0, -1, cfg.Getval_vector(s+"pos"), cfg.Getval_vector(s+"rot"), cfg.Getval_vector(s+"size"));
		node_finish->setMaterialTexture(0, cg->device->getVideoDriver()->getTexture("data/misc/finish.jpg"));
		node_finish->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
		node_finish->setVisible(true);
	}
	startPlaces_shown=false;

	// players order text
	core::dimension2d<u32> size = cg->device->getVideoDriver()->getScreenSize();
	const int lwidth = 180;
	const int lheight = 150;
	core::rect<int> pos(size.Width-lwidth-10, size.Height-lheight-10, size.Width-10, size.Height-10);
	cg->device->getGUIEnvironment()->addImage(pos);
	cg->playerOrder = cg->device->getGUIEnvironment()->addStaticText(L"",pos, true);
	cg->playerOrder->setOverrideColor(video::SColor(255,200,210,220));
	cg->playerOrder->setOverrideFont(cg->device->getGUIEnvironment()->getBuiltInFont());

	players_order=new int[cg->carmodels.size()];
	for (i=0; unsigned(i)<cg->carmodels.size(); i++) {
		players_order[i]=i;
	}
	PlayersResort();
}

CCandies::~CCandies()
{
	while (!startPlaces.empty()) {
		startPlaces.front()->remove();
		startPlaces.pop_front();
	}
}

void CCandies::StartPlaces_Show()
{
	if (startPlaces_shown) return;

	deque<irr::scene::ISceneNode *>::iterator i;
	
	for (i=startPlaces.begin(); i!=startPlaces.end(); i++) {
		(*i)->setVisible(true);
	}
	startPlaces_shown=true;
}

void CCandies::StartPlaces_Hide()
{
	if (!startPlaces_shown) return;

	deque<irr::scene::ISceneNode *>::iterator i;
	
	for (i=startPlaces.begin(); i!=startPlaces.end(); i++) {
		(*i)->setVisible(false);
	}
	startPlaces_shown=false;
}

void CCandies::Checkpoint_Crossed(int car, int checkpoint, irr::u32 time)
{
	//DBGCOUT("CCandies", "Checkpoint_Crossed", numtostring(car)+" "+numtostring(checkpoint));
	if (car==cg->cam_tgt) {
		if (checkpoints_order==RANDOM) {
			//hide current checkpoint / show finish if all hidden
			checkpoints[checkpoint]->setVisible(false);
			bool f=true;
			for (deque<scene::ISceneNode*>::iterator i=checkpoints.begin(); i!=checkpoints.end(); i++) {
				if ((*i)->isVisible()) f=false;
			}
			node_finish->setVisible(f);
		} else if (checkpoints_order==SEQUENTIAL) {
			//show next (or finish), hide current
			checkpoints[checkpoint]->setVisible(false);
			if (checkpoint==(checkpoints.size()-1)) node_finish->setVisible(true);
			else checkpoints[checkpoint+1]->setVisible(true);
		}
	}
	cg->carmodels[car].checkpoints.push_back(make_pair(checkpoint, time));
	PlayersResort();
}

void CCandies::Finish_Crossed(int car, int laps, irr::u32 time)
{
	if (checkpoints_order==RANDOM) {
		//finish -> show all checkpoints again
		if (car==cg->cam_tgt) {
			for (deque<scene::ISceneNode*>::iterator i=checkpoints.begin(); i!=checkpoints.end(); i++) (*i)->setVisible(true);
			node_finish->setVisible(false);
		}
	} else if (checkpoints_order==SEQUENTIAL) {
		//show first checkpoint, hide finish
		if (car==cg->cam_tgt)  {
			node_finish->setVisible(false);
			if (checkpoints.size()>0) checkpoints[0]->setVisible(true);
		}
	}
	cg->carmodels[car].checkpoints.push_back(make_pair(-laps, time));
	PlayersResort();
}

#define nlaps(c) (int)(c.checkpoints.size()/((int)checkpoints.size()+1))
#define nchks(c) (int)(c.checkpoints.size()%((int)checkpoints.size()+1))

void CCandies::PlayersResort()
{
	int s=(signed)cg->carmodels.size();
	int i, j;

	//bubble sort
	for (i=s-1; i>=0; i--) {
		for (j=0; j<i; j++) {
			//now compare cg->carmodels[players_order[j]] with cg->carmodels[players_order[j+1]] 
			CCarModel &c1=cg->carmodels[players_order[j]];
			CCarModel &c2=cg->carmodels[players_order[j+1]];
			bool sw=false;

			if (c1.checkpoints.size()<c2.checkpoints.size()) sw=true;
			else if (c1.checkpoints.size()==c2.checkpoints.size()) {
				if ((!c1.checkpoints.empty() && !c2.checkpoints.empty()) && (c1.checkpoints.back().second>c2.checkpoints.back().second)) sw=true;
			}
			if (sw==true) {
				int t=players_order[j];
				players_order[j]=players_order[j+1];
				players_order[j+1]=t; 
			}
		}
	}

	//for (i=0; i<s; i++) cerr << "TTT: " << players_order[i] << ": "<<cg->carmodels[players_order[i]].checkpoints.size() << endl;

	gui::IGUIStaticText*st=cg->GetGUIPlayerOrder();
	string str=STR_PLR_ORDER+(string)"\n";
	for (i=0; i<s; i++) {
		str+=numtostring(i+1)+". "+cg->carmodels[players_order[i]].nick;
		if (i>0 && cg->carmodels[players_order[i-1]].checkpoints.size()>0) { //we use "short evaluation" here :); do nothing if we are before 1st checkpoint
			irr::s32 diffTime=0, diffChks=0, diffLaps=0;
			//if difference<=1 lap, show time
			CCarModel &c1=cg->carmodels[players_order[i-1]];
			CCarModel &c2=cg->carmodels[players_order[i]];

			if (checkpoints_order==RANDOM) {
				if (nlaps(c1)==nlaps(c2)) {
					diffChks=nchks(c1)-nchks(c2);
				} else {
					diffLaps=nlaps(c1)-nlaps(c2);
					diffChks=nchks(c1); //the 2nd player has to finish some laps and then he will have to do number of checkpoints same as the first player
				}
			} else if (checkpoints_order==SEQUENTIAL) {
				int d1, d2;
				d1=(int)c1.checkpoints.size();
				d2=(int)c2.checkpoints.size();
				diffLaps=(d1-d2)/((int)checkpoints.size()+1);
				diffChks=(d1-d2)%((int)checkpoints.size()+1);
				if ((d1-d2)<=int(checkpoints.size()+1)) { //we can calculate time difference
					if (!c2.checkpoints.empty() && !c1.checkpoints.empty()) {
						diffTime=c2.checkpoints.back().second-c1.checkpoints[c2.checkpoints.size()-1].second;
						if (diffTime>=0) { //players switched order, return checkpoint difference...
							diffLaps=0;
							diffChks=0;
						} else diffTime=0;
					}
				}
			}
			str+=" +";
			if (diffTime) str+=numtostring(diffTime/1000)+"s ";
			else {
				if (diffLaps) str+=numtostring(diffLaps)+"l ";
				if (diffChks) str+=numtostring(diffChks)+"ch";
			}
		}
		str+="\n";
	}
	wchar_t *ws=a2u(str.c_str());
	st->setText(ws);
	delete ws;
}

void CCandies::RenewAllSpecific()
{
	//Startplaces - do nothing (not specific...)

	//Checkpoints/finish - draw according to checkpoints_order
	if (checkpoints_order==RANDOM) {
		//show all not crossed checkpoint or finish, if all are crossed
		int i, off=(int)(nlaps(cg->carmodels[cg->cam_tgt])*(checkpoints.size()+1));
		for (i=0; i<(signed)checkpoints.size(); i++) checkpoints[i]->setVisible(true);
		for (i=off; i<(signed)cg->carmodels[cg->cam_tgt].checkpoints.size(); i++) checkpoints[cg->carmodels[cg->cam_tgt].checkpoints[i].first]->setVisible(false);
		if (nchks(cg->carmodels[cg->cam_tgt])<checkpoints.size()) node_finish->setVisible(false);
		else node_finish->setVisible(true);
	} else if (checkpoints_order==SEQUENTIAL) {
		//find last crossed checkpoint, if all crossed show finish; otherwise show just the next one to last crossed checkpoint
		bool f=true;
		int i, off=(int)(nlaps(cg->carmodels[cg->cam_tgt])*(checkpoints.size()+1));
		for (i=0; i<(signed)checkpoints.size(); i++) checkpoints[i]->setVisible(false);
		if (nchks(cg->carmodels[cg->cam_tgt])<checkpoints.size()) {
			checkpoints[nchks(cg->carmodels[cg->cam_tgt])]->setVisible(true);
			node_finish->setVisible(false);
		} else node_finish->setVisible(true);
	}
}
