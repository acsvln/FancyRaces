/**
 * \file
 * This file contains declaration of the AsyncComm class.
 * It also defines messages used in communication between client and server.
 *
 * The meaning and format of each message is described in this documentation file.
 * Each format string is here written as following:
 *
 * ID1:value1|ID2:value2 ...
 *
 * ID1, ID2 etc. mean names of the items and value1, value2 etc. mean their respective values.
 * The complete format of the message is then %:MESSAGENAME|format_string, where MESSAGENAME is one of the message names defined by this file.
 *
 * Character '|' (vertical line, operator OR) should be replaced in outgoing messages by line-end characters (0x0a).
 * There are no spaces between id, :, value and | characters (spaces may be used in this documentation to make it more readable).
**/

#ifndef _AASYNCOMM_H_
#define _AASYNCOMM_H_

#include <string>
#include <queue>
#include "Asocket.h"
#include <OpenThreads/Mutex>

/**
 * Format: nick1: car profile of the player with nick1 | nick2: carprofile2 | nick3 (spectator) | BOT1: car profile ...
 *
 * Meaning if sent to client: offers the client list of connected users 
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_USERS "USERS"

/**
 * Format: DATA:message text
 *
 * Meaning if sent to client: informs the all clients about a message from some client
 *
 * Meaning if sent to server: sent if client wants to put a message on the chat. This message is sent back to clients as the same message type, but the format is 'DATA:nick: original message'
**/
#define AC_CHAT "CHAT"

/**
 * Format of this message depends on type of information sent by server (this message isn't sent by client)
 *
 * One purpose is to inform the player about position of vehicle of a player. In this case the format is following: 
 *
 * ID:id_of_vehicle_in_simulation | WHAT:POSROT | TIME:time_of_the_information | BODY:position | WHEEL1:position | WHEEL2:position...
 *
 * the position of object is specified by a position vector (made of 3 coordinates) and a rotation quaternion (another 4 values)
 *
 * The second purpose of the message is to inform player about values used by the server to control the vehicle. The format is following:
 *
 * ID:id_of_vehicle_in_simulation | WHAT:POSROT | TIME:time_of_the_information | STEER:steer_amount | ACCEL:gas_position | BRAKE:brake_position
 * | HANDBRAKE:handbrake_position | GEAR:gear_used | RESCUE:amount_of_rescue_force | CLUTCH:clutch_position | RPM: rpm_of_the_engine | SPEED: speed_of_thurst_wheels'_rotation
**/
#define AC_PLAYER "PLAYER"

/**
 * Format: DATA:nick
 *
 * Meaning if sent to client: the nick of the client has been changed
 *
 * Meaning if sent to server: client demands the change of his nick. If the change is succesful, this message with new nick is sent back to him
**/
#define AC_NICK "NICK"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: client gained administrator rights
 *
 * Meaning if sent to server: client demands administrator rights. If the gaining is succesful, this message is sent back to him
**/
#define AC_ADMIN "ADMIN"

/**
 * Format: cartype1|cartype2|...
 *
 * Meaning if sent to client: contains list of cars available to be used in the simulation
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_LIST_CARS "LIST_CARS"

/**
 * Format: mapname1|mapname2|...
 *
 * Meaning if sent to client: contains list of maps available to be used in the simulation
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_LIST_MAPS "LIST_MAPS"

/**
 * Format: DATA:cartype
 *
 * Meaning if sent to client: the car type, that the client will control in the simulation has been changed
 *
 * Meaning if sent to server: client wants to control the specified type of car inside of the simulation; If the car type exists and the change of type is successful, the same message is returned back to client
**/
#define AC_CAR "CAR"

/**
 * Format: DATA:mapname
 *
 * Meaning if sent to client: the map name, that will be used in simulation has been changed
 *
 * Meaning if sent to server: client wants to set the speficied map to be used in  the simulation; To change the map, it must exist and user's got to be administrator
**/
#define AC_MAP "MAP"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: administrator wants to add a bot to the list of players
**/
#define AC_BOT_ADD "BOD_ADD"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: administrator wants to add a bot to the list of players
**/
#define AC_BOT_RM "BOT_RM"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: the game (more precisely: the initialization of the game session) has been started
 *
 * Meaning if sent to server: administrator wants to start the game
**/
#define AC_STARTGAME "STARTGAME"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: server begun initialization of the simulation; client's got to wait until it finishes
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_SERVERINIT "SERVERINIT"

/**
 * Format: DATA:time
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: client wants to calculate ping time. Response with PONG message with 'time' as the parameter
**/
#define AC_PING "PING"

/**
 * Format: DATA:time
 *
 * Meaning if sent to client: this is an answer of server to a previously sent PING message (contains the same 'time' data that were sent by client on that time)
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_PONG "PONG"

/**
 * Format: (no additional parameters) 
 *
 * Meaning if sent to client: informs client that the game is about to start and it should switch to scene-initialization mode
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_GETREADY "GETREADY"

/**
 * Format: DATA:map
 *
 * Meaning if sent to client: informs the client, which map will be used for the simulation
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_WORLD "WORLD"

/**
 * Format: DATA:id
 *
 * Meaning if sent to client: informs client, what id has got his vehicle
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_YOURID "YOURID"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: informs client about the end of configuration block; now it should end it's initialization and send READY message to the server when done
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_WAITING "WAITING"

/**
 * Format: DATA:nick (no parameters in case the message is sent to client)
 *
 * Meaning if sent to client: the client has been successfuly registered as a player
 *
 * Meaning if sent to server: client wants to connect become a player
**/
#define AC_JOIN "JOIN"

/**
 * Format: DATA:nick (no parameters in case the message is sent to client)
 *
 * Meaning if sent to client: the client has been successfuly registered as a spectator
 *
 * Meaning if sent to server:client wants to connect become a spectator
**/
#define AC_SPECTATE "SPECTATE"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: the sending client has finished it's initialization and is ready to 'play'
**/
#define AC_READY "READY"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: (message currently not used)
 *
 * Meaning if sent to server: (message currently not used)
**/
#define AC_RESETAUTOMOVE "RESETAUTOMOVE"

/**
 * Format: STEER:steer_amount | ACCEL:gas_position | BRAKE:brake_position
 * | HANDBRAKE:handbrake_position | GEAR:gear_used | RESCUE:amount_of_rescue_force | CLUTCH:clutch_position | RPM: rpm_of_the_engine | SPEED: speed_of_thurst_wheels'_rotation
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: by this message informes what controls does the player want to get in use
**/
#define AC_CONTROL "CONTROL"

/**
 * Format: TIME:time | NCHECKPOINT:nch | NFINISH:nf | CHECKPOINT0:vehicleID0 checkpointNo0 | ... | CHECKPOINT(nch-1):vehicleID(nch-1) checkpointNo(nch-1)
 * | FINISH0:vehicleID0 lapsNo0 | ... | CHECKPOINT(nch-1):vehicleID(nf-1) lapsNo(nf-1)
 *
 * Meaning if sent to client: This message informs about changes in simulation. It informs about current time of simulation and about checkpoint and finish crossings.
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_SIMSTATUS "SIMSTATUS"

/**
 * Format: TIME:time | 0:position0 | 1:position1 ...
 *
 * the position of object is specified by a position vector (made of 3 coordinates) and a rotation quaternion (another 4 values)
 *
 * Meaning if sent to client: this message informs client about positions of objects (excepting vehicles) in the simulation
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_OBJECTS "OBJECTS"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: by this message the server informs client, that their connection is about to be closed - the message is currently near to be useless :)
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_DISCONNECT "DISCONNECT"

/**
 * Format: STYLE:style | LAPS:laps
 *
 * Meaning if sent to client: (client doesn't accept this kind of messages)
 *
 * Meaning if sent to server: by this message the administrator sets the style (stored in gameStyle, 0=single race; 1=tournament; 2=knockout) of the upcoming game and number of laps per race (stored in noLaps)
**/
#define AC_GAMESTYLE "GAMESTYLE"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: this message is sent, when all racers finished and the game should move to show results
 *
 * Meaning if sent to server: (server doesn't accept this kind of messages)
**/
#define AC_FINISHED "FINISHED"

/**
 * Format: (no additional parameters)
 *
 * Meaning if sent to client: if received this message, the server is being restarted and so should be the client part
 *
 * Meaning if sent to server: message is sent by administrator to make the server restart simulation into initial state
**/
#define AC_RESTART "RESTART"

class AsyncCommThread;

/**
 * This class offers possibility to communicate through sockets assynchronically - e.g. you the communication is nonblocking.
**/
class AsyncComm {
public:
	/**
	 * when creating the object, you have to pass the Socket object (which should be connected already).
	**/
	AsyncComm();
	AsyncComm(Socket *s);
	~AsyncComm();

	/**
	 * You can check whether the socket is operational with this function; returns true, if it's alive.
	**/
	bool Alive() {return stop==0;}

	/**
	 * can be used to communicate with the "other side of the socket" (they simply send raw strings, as they are passed to the function). The ReadString() function on the otherside of the connection will receive exactly the same string as it was send by SendString() (i.e. not longer nor truncated).
	**/
	void SendString(std::string str);

	/**
	 * can be used to communicate with the "other side of the socket" (they simply reads raw strings, as they are passed to the function). The ReadString() function on the otherside of the connection will receive exactly the same string as it was send by SendString() (i.e. not longer nor truncated).
	**/
	bool ReadString(std::string &str);

	/**
	 * initializes the object with the socket
	**/
	void Init(Socket *s);

	/**
	 * terminates the connection
	**/
	void disconnect();
private:
	friend class AsyncCommThread;

	AsyncCommThread *thread; //!< pointer to thread, which is processing messages for this socket
	OpenThreads::Mutex mutex; //!< mutex for synchronizing between thread and thread of this class (used when accesing string queue)
	volatile int stop; //!< if 0, the class is in normal state; 1 means we want the thread to exit; 2 means thread has stopped and we are clear to get deleted

	std::queue<std::string> strings; //!< contains list of strings that came from otherside of the socket
	Socket *sock; //!< pointer to the socket (really used just by the thread)
};

#endif //_AASYNCOMM_H_
