/**
 * \file
 * This file contains implementation of CGame and CGameCameraPos classes.
**/


#include "CGame.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include "Sserver.h"
#include "Autil.h"
#include <stdlib.h>
#include <irrlicht/IMeshCache.h>

using namespace std;

enum {
	FRAMELIMITER=0, //TODO
	TL_LOOKAT=400,
	TL_POS=750,
};

CGame::CGame(AGameSettings *_ags, bool spectate, bool admin)
: ags(_ags), currentScene(-1),
timeForThisScene(0),
cam_tgt(0), cam_distance(5), cam_rot(core::vector3df(0, 0, -M_PI/8)),
skyboxNode(0),
cam_lookat(TL_LOOKAT), cam_pos(TL_POS),
show_debuginfo(false), show_controls(false),
show_simdebug(false),
quitGame(0), closeWindow(0), spectatorMode(spectate), simOffsetTime(0), adminMode(admin)
{
	for (int i=0; i<KEY_KEY_CODES_COUNT; i++) {
		kbd_states[i]=false;
		kbd_states_fti[i]=0;
	}

	device=ags->device;
	snd_listener=new openalpp::Listener();
}

CGame::~CGame()
{
}

/**
 * This function calculates position of a rectangle so it is centered in the screen.
 * @param w width of the rectangle
 * @param h height of the rectangle
 * @param guienv handle to the IGUIEnvironment object used to show gui inside of irrlicht (it is a part of the irrlicht device).
 * @return returns the rectangle, containing desired position (it's width and height are the same as input parameters - w and h)
**/
core::rect<s32> CenterRect(s32 w, s32 h, gui::IGUIEnvironment* guienv)
{
	core::dimension2d<u32> ss=guienv->getVideoDriver()->getScreenSize();
	core::rect<s32> rv;
	rv.UpperLeftCorner.X=ss.Width/2-w/2;
	rv.UpperLeftCorner.Y=ss.Height/2-h/2;
	rv.LowerRightCorner.X=ss.Width/2+w/2;
	rv.LowerRightCorner.Y=ss.Height/2+h/2;
	return rv;
}

int CGame::run()
{
	using namespace std;

//INITIALIZE GRAPHICS SYSTEM
	device->setEventReceiver(this);
	video::IVideoDriver* driver = device->getVideoDriver();
	scene::ISceneManager* smgr = device->getSceneManager();
	gui::IGUIEnvironment* guienv = device->getGUIEnvironment();

	device->setWindowCaption(L"Anorasi::Game");
	smgr->clear();

	gui::IGUIFont* font = device->getGUIEnvironment()->getFont("data/misc/fontcourier.bmp"); 

//MAIN LOOP
	while(device->run() && !quitGame) {
		//if (device->isWindowActive()) {
			// load next scene if necessary
			u32 now = device->getTimer()->getRealTime();
			if (now - sceneStartTime > (u32)timeForThisScene && timeForThisScene!=-1)
				switchToNextScene();
			
			switch (currentScene) {
			case 1:
				doScene1_serverWait(driver, smgr, guienv, font);
				break;
			case 3: //SCENE: GAMEPLAY
				doScene3_main(driver, smgr, guienv, font);
				break;
			}

			OpenThreads::Thread::YieldCurrentThread();
		//}
	}
	return quitGame;
}

void CGame::switchToNextScene()
{
	currentScene++;
	if (currentScene > 3)
		currentScene = 3;

	scene::ISceneManager* sm = device->getSceneManager();
	scene::ISceneNodeAnimator* sa = 0;
	scene::ICameraSceneNode* camera = 0;

	camera = sm->getActiveCamera();
	if (camera) {
		sm->setActiveCamera(0);
		camera->remove();
	}

	switch(currentScene) {
	case 0: // loading screen
		timeForThisScene = 0;
		doScene0_introScreen();
		break;

	case 1: // start server
		timeForThisScene = -1;
		//doScene0_introScreen does the rest 
		break;

	case 2: // load scene
		timeForThisScene = 0;
		doScene2_loadData();
		//currentScene += 2;
		break;

	case 3: // game...
		{
			timeForThisScene = (u32)-1;

			camera = sm->addCameraSceneNode(0, core::vector3df(0,0,0), core::vector3df(0, 0, 100));
			camera->setFarValue(350);
			
			inOutFader->fadeIn(500);
			//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);
		}
		break;
	}

	// if we've got a new created camera, we call OnPostRender to let all animators
	// set the right position of the camera, otherwise the camera would
	// be at a wrong position in the first frame
	if (device->getSceneManager()->getActiveCamera()) device->getSceneManager()->getActiveCamera()->OnAnimate(sceneStartTime);

	sceneStartTime = device->getTimer()->getRealTime();
}

void CGame::doScene0_introScreen()
{
	core::dimension2d<u32> size = device->getVideoDriver()->getScreenSize();

	device->getCursorControl()->setVisible(false);

	// setup loading screen
	backColor.set(255,0, 0, 0);

	// create in fader
	inOutFader = device->getGUIEnvironment()->addInOutFader();
	inOutFader->setColor(backColor);

	// loading text
	const int lwidth = 320;
	const int lheight = 15;
	core::rect<int> pos(10, size.Height-lheight-10, 10+lwidth, size.Height-10);
	device->getGUIEnvironment()->addImage(pos);
	statusText = device->getGUIEnvironment()->addStaticText(L"Loading...", pos, true);
	statusText->setOverrideColor(video::SColor(255, 255, 255, 255));

	// load bigger font
	device->getGUIEnvironment()->getSkin()->setFont(device->getGUIEnvironment()->getFont("data/misc/fontcourier.bmp"));

	// set new font color
	device->getGUIEnvironment()->getSkin()->setColor(gui::EGDC_BUTTON_TEXT, video::SColor(255,0,0,0));
}

void CGame::doScene1_serverWait(video::IVideoDriver* driver, scene::ISceneManager* smgr, gui::IGUIEnvironment* guienv, gui::IGUIFont* font)
{
	using namespace std;

	driver->beginScene(true, true, backColor);
	smgr->drawAll();
	guienv->drawAll();
	driver->endScene();

	//SCENE: LOAD SERVER
	//do the communication - currently just wait for GETREADY message
	while (1) {
		string buf;
		if (!ags->connection || !ags->connection->Alive()) quitGame=1;
		if (ags->connection->ReadString(buf)) {
			cmd_map cmd;
			cmd_String2Map(buf, cmd);
			
			if (cmd["%"]==AC_GETREADY) {
				DBGCOUT("CSt", "get ready", "");
				switchToNextScene();
				break;
			} else {
				DBGCOUT("CSt", cmd["%"], cmd["DATA"]);
			}
		}
	}
}

void CGame::doScene2_loadData()
{
	using namespace std;
	string buf, worldname, carname;
	video::IVideoDriver* driver = device->getVideoDriver();
	scene::ISceneManager* sm = device->getSceneManager();

	//Get info about the scene from server
	DBGCOUT("CGame", "doScene2_loadData", "Start");
	//read all the stuff server sent us
	while (1) {
		if (!ags->connection->Alive()) {
			quitGame=1;
			break;
		}
		ags->connection->ReadString(buf);
		cmd_map cmd;
		cmd_String2Map(buf, cmd);
		//DBGCOUT("Loa", "str", buf);
		if (cmd["%"]==AC_WORLD) {
			worldname=cmd["DATA"];
		} else if (cmd["%"]==AC_CAR) {
			carmodels.push_back(CCarModel());
			CCarModel &cm=*(carmodels.end()-1);
			cm.profile=cmd["PROFILE"];
			cm.id=atol(cmd["ID"].c_str());
			cm.nick=cmd["NICK"];
			cm.Load(device, sm, driver);
			cm.show_debug(show_simdebug);
		} else if (cmd["%"]==AC_YOURID) {
			mymodelid=atol(cmd["DATA"].c_str());
		} else if (cmd["%"]==AC_WAITING) { //this is the end of from-server data
			break;
		} else if (cmd["%"]==AC_PLAYER) { //player position
			int id=processMessage_PLAYER(cmd);
			carmodels[id].r2c();
		} else DBGCOUT("CGame", "doScene2_loadData", "Unhandled ReadString - '"+buf+"'");
	}
	DBGCOUT("CGame", "doScene2_loadData", "End");

	//initialize resources
	CfgFile cfg(device->getFileSystem(), "data/maps/"+worldname+"/config.ini");
	string curmappath="data/maps/"+worldname+"/";

	// create sky box
	//driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, false);
	if (cfg.Getval_exists("Terrain/Skybox/up")) {
	skyboxNode = sm->addSkyBoxSceneNode(
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/up", "")).c_str()),
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/dn", "")).c_str()),
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/lf", "")).c_str()),
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/rt", "")).c_str()),
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/ft", "")).c_str()),
		driver->getTexture((curmappath+cfg.Getval_str("Terrain/Skybox/bk", "")).c_str())
		);
	} else {
		skyboxNode=0;
	}

	//driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);

	//load map
	if (worldname!="") {
		std::string cwd=device->getFileSystem()->getWorkingDirectory().c_str();
		device->getFileSystem()->changeWorkingDirectoryTo(("data/maps/"+worldname).c_str());
		
		doScene2_loadData_loadMap(cfg, driver, sm);

		device->getFileSystem()->changeWorkingDirectoryTo(cwd.c_str());
	}
	
	//add map candies (start, finish, checkpoints etc.)
	candies=new CCandies(this, cfg);

	//load user interface
	CfgFile cfg2;
	cfg2.Load(device->getFileSystem(), "data/settings.ini");
	rpm_img_x=cfg2.Getval_int("Race_UI/RPM/pos_x", 0);
	rpm_img_y=cfg2.Getval_int("Race_UI/RPM/pos_y", 0);
	rpm_img_w=cfg2.Getval_int("Race_UI/RPM/width", 0);
	rpm_img_h=cfg2.Getval_int("Race_UI/RPM/height", 0);
	rpm_img_1=driver->getTexture(("data/"+cfg2.Getval_str("Race_UI/RPM/img_1", "")).c_str());
	rpm_img_2=driver->getTexture(("data/"+cfg2.Getval_str("Race_UI/RPM/img_2", "")).c_str());

	digits_spd_w=cfg2.Getval_int("Race_UI/Speed/digit_w", 0);
	digits_spd_h=cfg2.Getval_int("Race_UI/Speed/digit_h", 0);
	digits_spd_x=cfg2.Getval_int("Race_UI/Speed/pos_x", 0);
	digits_spd_y=cfg2.Getval_int("Race_UI/Speed/pos_y", 0);
	digits_spd=driver->getTexture(("data/"+cfg2.Getval_str("Race_UI/Speed/digits", "")).c_str());

	digits_gbx_w=cfg2.Getval_int("Race_UI/Gearbox/digit_w", 0);
	digits_gbx_h=cfg2.Getval_int("Race_UI/Gearbox/digit_h", 0);
	digits_gbx_x=cfg2.Getval_int("Race_UI/Gearbox/pos_x", 0);
	digits_gbx_y=cfg2.Getval_int("Race_UI/Gearbox/pos_y", 0);
	digits_gbx=driver->getTexture(("data/"+cfg2.Getval_str("Race_UI/Gearbox/digits", "")).c_str());

	// set background color
	backColor.set(0,0,0,0);

	//camera settings
	if (spectatorMode)
		cam_tgt=carmodels[0].id;
	else cam_tgt=mymodelid;
	candies->RenewAllSpecific();

	//start soundsystem
	for (deque<CCarModel>::iterator cmi=carmodels.begin(); cmi!=carmodels.end(); cmi++) {
		cmi->soundStart();
	}

	//finito, tell server
	ags->connection->SendString("%:"AC_READY);
}

void CGame::doScene2_loadData_loadMap(CfgFile &cfg, video::IVideoDriver* driver, scene::ISceneManager* sm)
{
	scene::ISceneNode* node;
	int i;

	for (i=0;; i++) {
		node=0;
		char buf[100];
		sprintf(buf, "Terrain/Object_%d/", i+1);
		string buf2=buf;

		if (cfg.Getval_flag(buf2+"skip", "yes")) {
			continue;
		} else if (cfg.Getval_exists(buf2+"file")) {
			string map=cfg.Getval_str(buf2+"file", "");
			string ext;
			try {
				ext=map.substr(map.rfind('.'));
				if (ext.size()>100) throw "bad size";
				ext=StringLower(ext);
			} catch (...) {}
			scene::IMesh* mesh=0;

			if (ext==".bmp" || ext==".png") { //TODO:another irrlicht supported formats?
				scene::ITerrainSceneNode* terrain = sm->addTerrainSceneNode(map.c_str());
				terrain->setMaterialFlag(video::EMF_LIGHTING, false);
				terrain->setScale(cfg.Getval_vector(buf2+"scale", core::vector3df(1, 1, 1)));

				terrain->setMaterialTexture(0, driver->getTexture("texture.jpg"));
				mesh=terrain->getMesh();
			} else if (ext==".x" || ext==".3ds" || ext==".ms3d") {
				scene::IAnimatedMesh* amesh=sm->getMesh(map.c_str());
				mesh=amesh->getMesh(0);
			}
			
			if (mesh) {
				node=sm->addMeshSceneNode(mesh);
				node->setMaterialFlag(video::EMF_LIGHTING, false);
				//for (int j=0; j<node->getMaterialCount(); j++) node->getMaterial(j).FogEnable=true;
				//driver->setFog(video::SColor(128, 0, 0, 0), true, 250, 350);
				node->setDebugDataVisible(irr::scene::EDS_OFF);
				node->setScale(cfg.Getval_vector(buf2+"scale", core::vector3df(1, 1, 1)));
				node->setPosition(cfg.Getval_vector(buf2+"pos", core::vector3df(0, 0, 0)));
				node->setRotation(cfg.Getval_vector(buf2+"rot", core::vector3df(0, 0, 0)));
			}
		} else if (cfg.Getval_exists(buf2+"type")) {
			if (cfg.Getval_str(buf2+"type")=="water") {
				core::dimension2d<f32> tile_size, texture_repeat_count;
				core::dimension2d<u32> tile_count;
				core::vector3df t;
				
				t=cfg.Getval_vector(buf2+"tile_size"); tile_size.Width=t.X; tile_size.Height=t.Y;
				t=cfg.Getval_vector(buf2+"texture_repeat_count"); texture_repeat_count.Width=t.X; texture_repeat_count.Height=t.Y;
				t=cfg.Getval_vector(buf2+"tile_count"); tile_count.Width=(s32)t.X; tile_count.Height=(s32)t.Y;

				scene::IAnimatedMesh* m=sm->getMesh(("myWater-"+buf2).c_str());

				if (m) sm->getMeshCache()->removeMesh(m);
				scene::IAnimatedMesh *mesh = sm->addHillPlaneMesh(("myWater-"+buf2).c_str(),
					tile_size,
					tile_count, 0, 0,
					core::dimension2d<f32>(0,0),
					texture_repeat_count
				);

				if (mesh) {
					node = sm->addWaterSurfaceSceneNode(mesh->getMesh(0),
						(f32)cfg.Getval_double(buf2+"wave_height", 0.3),
						(f32)cfg.Getval_double(buf2+"wave_speed", 300),
						(f32)cfg.Getval_double(buf2+"wave_length", 2));
					node->setPosition(cfg.Getval_vector(buf2+"pos"));
					node->setMaterialTexture(0,driver->getTexture(cfg.Getval_str(buf2+"texture_water").c_str()));
					node->setMaterialTexture(1,driver->getTexture(cfg.Getval_str(buf2+"texture_ground").c_str()));
					node->setMaterialType(video::EMT_REFLECTION_2_LAYER);
				}
			} else if (cfg.Getval_str(buf2+"type")=="box") {
				scene::IMesh* mesh=CreateCube(cfg.Getval_int(buf2+"texture_type", 0));

				node=sm->addMeshSceneNode(mesh);
				node->setMaterialTexture(0, driver->getTexture(cfg.Getval_str(buf2+"texture", "").c_str()));
				node->setMaterialFlag(video::EMF_LIGHTING, false);
				node->setDebugDataVisible(irr::scene::EDS_OFF);
				node->setPosition(cfg.Getval_vector(buf2+"pos"));
				node->setScale(cfg.Getval_vector(buf2+"scale", core::vector3df(1, 1, 1)));
				node->setRotation(cfg.Getval_vector(buf2+"rot", core::vector3df(0, 0, 0)));
			} else if (cfg.Getval_str(buf2+"type")=="sphere") {
				scene::IAnimatedMesh* amesh=sm->getMesh("../../misc/ball.3ds");
				scene::IMesh* mesh=amesh->getMesh(0);

				node=sm->addMeshSceneNode(mesh);
				node->setMaterialTexture(0, driver->getTexture(cfg.Getval_str(buf2+"texture", "").c_str()));
				node->setMaterialFlag(video::EMF_LIGHTING, false);
				//for (int j=0; j<node->getMaterialCount(); j++) node->getMaterial(j).FogEnable=true;
				//driver->setFog(video::SColor(128, 0, 0, 0), true, 250, 350);
				node->setDebugDataVisible(irr::scene::EDS_OFF);
				node->setPosition(cfg.Getval_vector(buf2+"pos"));
				float mscale=(float)cfg.Getval_double(buf2+"radius", 1.0)*2;
				node->setScale(core::vector3df(mscale, mscale, mscale));
				node->setRotation(cfg.Getval_vector(buf2+"rot", core::vector3df(0, 0, 0)));
			}
		} else break;

		if (cfg.Getval_exists(buf2+"Physics/surface") && cfg.Getval_exists(buf2+"Physics/weight")) {
			CTerrainPart ma;
			ma.node=node;
			ma.InitDebug(device, sm);
			ma.show_debug(show_simdebug);
			terrainParts.push_back(ma);
		}
	}
}

void CGame::doScene3_main(video::IVideoDriver* driver, scene::ISceneManager* smgr, gui::IGUIEnvironment* guienv, gui::IGUIFont* font)
{
	int j;
	static int goping=0; //TODO: move to class?
	static u32 sping=0;
	static u32 lasttime_ctrl=0;
	wchar_t tmp[1024];
	char buf[1024];

	processControls();

	CCarModel &mcm=spectatorMode ? carmodels[cam_tgt] : carmodels[mymodelid];

	if (!ags->connection->Alive()) {
		quitGame=2; //kinda nasty (as CMainMenu thinks the game ended normally - it has to discover the connection's dead
		return;
	}

	if (!spectatorMode) {
		if (lasttime_ctrl==0 || (lasttime_ctrl-device->getTimer()->getRealTime())>10) {
			//process communication
			sprintf(buf,
				"%%:"AC_CONTROL"\nSTEER:%5.2g\nACCEL:%5.2g\nBRAKE:%5.2g\nHANDBRAKE:%5.2g\nGEAR:%d\nCLUTCH:%5.2g\nRESCUE:%5.2g",
				mcm.c_steer, mcm.c_accel, mcm.c_brakes, mcm.c_handbrake, mcm.c_gear, mcm.c_clutch, mcm.c_rescue);
			ags->connection->SendString(buf);
			lasttime_ctrl=device->getTimer()->getRealTime();
		}
	}

	if (goping==0) {
		sprintf(buf, "%%:PING\nDATA:%lu", (unsigned long)device->getTimer()->getRealTime());
		ags->connection->SendString(buf);
		goping=-1;
	} else if (goping>0) {
		goping--;
	}

	std::string buf2;

	while (ags->connection->ReadString(buf2)) {
		cmd_map cmd;
		cmd_String2Map(buf2, cmd);

		if (cmd["%"]==AC_PLAYER) {
			processMessage_PLAYER(cmd);
			if (simOffsetTime==0) simOffsetTime=device->getTimer()->getRealTime();
		} else if (cmd["%"]==AC_PONG) {
			istringstream is(cmd["DATA"].c_str());
			u32 pong;
			is >> pong;

			sping=device->getTimer()->getRealTime()-pong;
			goping=50;
		} else if (cmd["%"]==AC_RESETAUTOMOVE) {
			//TODO: reset all automove vectors
		} else if (cmd["%"]==AC_SIMSTATUS) {
			processMessage_SIMSTATUS(cmd);
		} else if (cmd["%"]==AC_OBJECTS) {
			processMessage_OBJECTS(cmd);
		} else if (cmd["%"]==AC_FINISHED) {
			quitGame=2;
			results.clear();
			for (int i=0;; i++) {
				if (cmd["RESULTS"+numtostring(i)]=="") break;
				results.push_back(numtostring(i+1)+". "+cmd["RESULTS"+numtostring(i)]);
			}
		} else if (cmd["%"]==AC_USERS) {
			//do nothing now - just some spectator connected probably
		} else if (cmd["%"]==AC_DISCONNECT) {
            quitGame=2; //kinda nasty
			ags->DeleteConnection();
			return;
		} else if (cmd["%"]==AC_RESTART) {
			//start the restart procedure... i.e wait until the server finishes reloading and sends AC_WAITING
			infoWindow=guienv->addWindow(CenterRect(200, 70, guienv), false, L"Please wait....");
			guienv->addStaticText(L"Server is restarting the game...", core::rect<s32>(10, 20, 190, 50), false, true, infoWindow);
			for (deque<CCarModel>::iterator cmi=carmodels.begin(); cmi!=carmodels.end(); cmi++) {
				cmi->clearPosData();
				cmi->checkpoints.clear();
			}
			for (deque<CTerrainPart>::iterator tpi=terrainParts.begin(); tpi!=terrainParts.end(); tpi++) {
				tpi->clearPosData();
			}
			simOffsetTime=0;
		} else if (cmd["%"]==AC_WAITING) {
			infoWindow->remove();
			candies->RenewAllSpecific();
			for (deque<CCarModel>::iterator cmi=carmodels.begin(); cmi!=carmodels.end(); cmi++) cmi->r2c();
		} else {
			DBGCOUT("CGame", "doScene3_main - unprocessed from-server-message", buf2);
		}
	}

	s32 latency=80;
	u32 curSimTime=max(((s32)device->getTimer()->getRealTime()-(s32)simOffsetTime-latency), 0);

	for (deque<CCarModel>::iterator cmi=carmodels.begin(); cmi!=carmodels.end(); cmi++) {
		cmi->move(curSimTime);
		cmi->soundSetProperties();
	}
	for (deque<CTerrainPart>::iterator tpi=terrainParts.begin(); tpi!=terrainParts.end(); tpi++) {
		tpi->move(curSimTime);
	}

	u32 posfactor=mcm.body.speed+1;

	//calculate position and target of camera
	CCarModel &ccm=carmodels[cam_tgt];
	cam_lookat.setCurrentPosition(ccm.body.node->getPosition(), device->getTimer()->getRealTime(), posfactor);
	smgr->getActiveCamera()->setTarget(cam_lookat.getSmooothedPosition(device->getTimer()->getRealTime()));

	core::matrix4 drotm; drotm.setRotationDegrees(ccm.body.node->getRotation());
	core::matrix4 drotm2; drotm2.setInverseRotationDegrees(cam_rot);
	core::vector3df dpos;
	if (ccm.r_speed>=-5)
		dpos=core::vector3df(cam_distance*cos(M_PI+cam_rot.X), cam_distance*sin(-cam_rot.Z), cam_distance*sin(M_PI+cam_rot.X));
	else dpos=core::vector3df(cam_distance*cos(cam_rot.X), cam_distance*sin(-cam_rot.Z), cam_distance*sin(cam_rot.X));
	drotm2.transformVect(dpos);
	drotm.transformVect(dpos);

	cam_pos.setCurrentPosition(ccm.body.node->getPosition()+dpos, device->getTimer()->getRealTime(), posfactor);
	
	core::vector3df cp1=smgr->getActiveCamera()->getPosition();
	core::vector3df cp2=cam_pos.getSmooothedPosition(device->getTimer()->getRealTime());
	smgr->getActiveCamera()->setPosition(cp2);

	/*cerr << "POS: " << cp2.X << " " << cp2.Y << " " <<cp2.Z <<" / " << ccm.body.node->getPosition().X << " "<< ccm.body.node->getPosition().Y << " "<< ccm.body.node->getPosition().Z <<" : "
		<< carmodels[0].body.locations[0].pos.X << " "<< carmodels[0].body.locations[0].pos.Y << " "  << carmodels[0].body.locations[0].pos.Z << " " 
		<< "@" << carmodels[0].body.locations[0].tim << " & " << (u32)((s32)device->getTimer()->getRealTime()-(s32)simOffsetTime-60) << "(" << device->getTimer()->getRealTime() << "-"<<(s32)simOffsetTime << "-60"
		<<endl;*/
	//set sound listener position.. BIGTODO: calculate to meters etc.
	snd_listener->setPosition(cp2.X, cp2.Y, cp2.Z);
	//cerr << cp2.X << " "<< cp2.Y << " "<< cp2.Z << endl;
	cp2-=cp1;
	//snd_listener->setVelocity(cp2.X, cp2.Y, cp2.Z);
	cp2=smgr->getActiveCamera()->getTarget()-smgr->getActiveCamera()->getPosition();
	cp2.normalize();
	snd_listener->setOrientation(cp2.X, cp2.Y, cp2.Z, 0, -1, 0);

	//draw scene candies (start, checkpoints etc.)
	if (sim_time>=0 && sim_time<5000) {
		candies->StartPlaces_Show();
	} else {
		candies->StartPlaces_Hide();
	}

	// draw everything
	driver->beginScene(true, true, backColor);
	driver->setAmbientLight(video::SColor(255, 255, 255, 255));
	smgr->drawAll();

	if (show_controls) {
		//draw real parameters
		int UI_CENTR_X=640-100-20;
		int UI_CENTR_Y=480-100-20;
		driver->draw2DRectangle(video::SColor(100,0,100,255), core::rect<s32>(UI_CENTR_X-10, int(UI_CENTR_Y-mcm.r_accel*100), UI_CENTR_X+10, UI_CENTR_Y)); 
		driver->draw2DRectangle(video::SColor(100,255,0,0), core::rect<s32>(UI_CENTR_X-10, UI_CENTR_Y, UI_CENTR_X+10, int(UI_CENTR_Y+mcm.r_brakes*100)));
		driver->draw2DRectangle(video::SColor(100,255,0,0), core::rect<s32>(UI_CENTR_X-30, UI_CENTR_Y, UI_CENTR_X-10, int(UI_CENTR_Y+mcm.r_handbrake*100)));
		driver->draw2DRectangle(video::SColor(100,255,255,255), core::rect<s32>(UI_CENTR_X+min(int(mcm.r_steer*100), 0), UI_CENTR_Y-10, UI_CENTR_X+max(int(mcm.r_steer*100), 0), UI_CENTR_Y+10)); 
		driver->draw2DRectangle(video::SColor(100,0,255,0), core::rect<s32>(UI_CENTR_X+100, UI_CENTR_Y+20-mcm.r_gear*20, UI_CENTR_X+120, UI_CENTR_Y+40-mcm.r_gear*20)); 
		driver->draw2DRectangle(video::SColor((s32)(mcm.r_clutch*100),0,255,0), core::rect<s32>(UI_CENTR_X+10, UI_CENTR_Y-100, UI_CENTR_X+100, UI_CENTR_Y-10));
		
		swprintf(tmp, sizeof(tmp)/sizeof(wchar_t), L"Speed: %d, RPM: %d, Gear: %d", mcm.r_speed, mcm.r_rpm, mcm.r_gear-mcm.gearN);
		font->draw(tmp, core::rect<s32>(400,10,640,50), video::SColor(150,255,255,255));
	}

	//draw rpm-meter
	driver->draw2DImage(rpm_img_2, core::position2d<s32>(rpm_img_x, rpm_img_y), core::rect<s32>(0, 0, rpm_img_w*mcm.r_rpm/mcm.maxrpm, rpm_img_h), 0, video::SColor(255,255,255,255), true);
	driver->draw2DImage(rpm_img_1, core::position2d<s32>(rpm_img_x+rpm_img_w*mcm.r_rpm/mcm.maxrpm, rpm_img_y), core::rect<s32>(rpm_img_w*mcm.r_rpm/mcm.maxrpm, 0, rpm_img_w, rpm_img_h-1), 0, video::SColor(255,255,255,255), true);
	swprintf(tmp, sizeof(tmp)/sizeof(wchar_t), L"%d", mcm.r_rpm);
	font->draw(tmp, core::rect<s32>(digits_gbx_x,digits_gbx_y-20,digits_gbx_x+100,digits_gbx_y), video::SColor(150,255,255,255));
	
	//draw gear
	driver->draw2DImage(digits_gbx, core::position2d<s32>(digits_gbx_x, digits_gbx_y), core::rect<s32>(mcm.r_gear*digits_gbx_w, 0, (mcm.r_gear+1)*digits_gbx_w, digits_gbx_h), 0, video::SColor(255,255,255,255), true);

	//draw speed
	int spd=mcm.r_speed;
	int pxd=0;
	bool pxdneg=false;
	if (spd<0) {
		spd=-spd;
		pxdneg=true;
	}
	
	if (spd==0) {
		driver->draw2DImage(digits_spd, core::position2d<s32>(digits_spd_x, digits_spd_y), core::rect<s32>(0, 0, digits_spd_w, digits_spd_h), 0, video::SColor(255,255,255,255), true);
	} else while (spd>0) {
		int d=spd%10;
		
		driver->draw2DImage(digits_spd, core::position2d<s32>(digits_spd_x+pxd, digits_spd_y), core::rect<s32>(d*digits_spd_w, 0, (d+1)*digits_spd_w, digits_spd_h), 0, video::SColor(255,255,255,255), true);

		spd=spd/10;
		pxd-=digits_spd_w;
	}
	if (pxdneg) {
		driver->draw2DImage(digits_spd, core::position2d<s32>(digits_spd_x+pxd, digits_spd_y), core::rect<s32>((10)*digits_spd_w, 0, (11)*digits_spd_w, digits_spd_h), 0, video::SColor(255,255,255,255), true);
	}


	if (show_debuginfo) {
		//print debug
		deque<string>::iterator j2;
		int jmax=25;

		DBGCOUT_data_beg();
		for (j=0, j2=DBGCOUT_data().begin(); (j<jmax) && (j2!=DBGCOUT_data().end()); j++, j2++) {
			wchar_t* tmp2=a2u(j2->c_str());
			font->draw(tmp2, core::rect<s32>(0,(jmax-j)*15,600,(jmax-j)*15+15), video::SColor(150,255,255,255));
			delete tmp2;
		}
		DBGCOUT_data_end();
	}

	guienv->drawAll();
	driver->endScene();

	// write statistics
	swprintf(tmp, 255, L"%s fps:%d, ping: %lu, time: %6.2fs", driver->getName(), driver->getFPS(), sping, (float)sim_time/1000);
	statusText->setText(tmp);
}

int CGame::processMessage_PLAYER(cmd_map &cmd)
{
	string what=cmd["WHAT"];
	int id=atol(cmd["ID"].c_str());

	CCarModel &cm=carmodels[id];

	cm.lastInfoTime=device->getTimer()->getRealTime();

	if (what=="POSROT") {
		cm.body.setPosRot(cmd["BODY"], atol(cmd["TIME"].c_str()));
		
		for (int i=0; i<(signed)cm.wheels.size(); i++) {
			char buf[10];
			sprintf(buf, "WHEEL%d", i);
			cm.wheels[i].setPosRot(cmd[buf], atol(cmd["TIME"].c_str()));
		}
	} else if (what=="CONTROL") {
		cm.r_steer=(irr::f32)atof(cmd["STEER"].c_str());
		cm.r_accel=(irr::f32)atof(cmd["ACCEL"].c_str());
		cm.r_brakes=(irr::f32)atof(cmd["BRAKE"].c_str());
		cm.r_handbrake=(irr::f32)atof(cmd["HANDBRAKE"].c_str());
		cm.r_gear=atol(cmd["GEAR"].c_str());
		cm.r_clutch=(irr::f32)atof(cmd["CLUTCH"].c_str());
		cm.r_rpm=atol(cmd["RPM"].c_str());
		cm.r_speed=atol(cmd["SPEED"].c_str());
		cm.r_rescue=(irr::f32)atof(cmd["RESCUE"].c_str());
	}
	return id;
}

void CGame::processMessage_SIMSTATUS(cmd_map &cmd)
{
	//deque<pair<string, string> > parms=cmd_GetParameters(cmd["DATA"]);

	sim_time=(irr::u32)atol(cmd["TIME"].c_str());
	//cerr << device->getTimer()->getRealTime()-simOffsetTime << ": " << sim_time-carmodels[0].body.locations[0].tim << " " << carmodels[0].body.locations.size() << endl;
	
	//if we get out of sync with server too much, reset the simOffsetTime
	if (sim_time-carmodels[0].body.locations[0].tim>100) {
		simOffsetTime=(u32)((s32)device->getTimer()->getRealTime()-(s32)sim_time-50);
		//cerr << "posreset " << device->getTimer()->getRealTime()<<" "<<sim_time << "=> " << simOffsetTime << endl;
	}

	int f, c, i;
	f=(int)atol(cmd["NFINISH"].c_str());
	c=(int)atol(cmd["NCHECKPOINT"].c_str());
	for (i=0; i<c; i++) {
		int x, y;
		sscanf(cmd["CHECKPOINT"+numtostring(i)].c_str(), "%d %d", &x, &y);
		candies->Checkpoint_Crossed(x, y, sim_time);
	}
	for (i=0; i<f; i++) {
		int x, y;
		sscanf(cmd["FINISH"+numtostring(i)].c_str(), "%d %d", &x, &y);
		candies->Finish_Crossed(x, y, sim_time);
	}
}

void CGame::processMessage_OBJECTS(cmd_map &cmd)
{
	//DBGCOUT("CGame", "processMessage_OBJECTS", "received");
	u32 t=atol(cmd["TIME"].c_str());

	for (cmd_map::iterator i=cmd.begin(); i!=cmd.end(); i++) {
		if (i->first=="%" || i->first=="TIME") continue;
		int j=atol(i->first.c_str());
		//DBGCOUT("CGame", "processMessage_OBJECTS", i->first+"=>"+i->second);
		terrainParts[j].setPosRot(i->second, t);
	}
}

enum {
	ACTRL_ACCEL_UP=KEY_UP,
	ACTRL_ACCEL_DOWN=KEY_DOWN,
	ACTRL_STEER_LEFT=KEY_LEFT,
	ACTRL_STEER_RIGHT=KEY_RIGHT,
	ACTRL_GEAR_UP=KEY_KEY_A,
	ACTRL_GEAR_DOWN=KEY_KEY_Z,
	ACTRL_CLUTCH=KEY_KEY_X,
	ACTRL_HANDBRAKE=KEY_SPACE,
	ACTRL_CHANGECAMTGT=KEY_TAB,
	ACTRL_SHOWCONTROLS=KEY_KEY_K,
	ACTRL_SHOWDEBUGINFO=KEY_KEY_J,
	ACTRL_SHOWSIMDEBUG=KEY_KEY_L,
	ACTRL_RESCUE=KEY_KEY_S,
};

void CGame::processControls()
{
	//this is not the clean way, but as we don't send any controls info, it's ok
	CCarModel &mcm=spectatorMode?carmodels[0]:carmodels[mymodelid];

//acceleration/breaks
	if (kbd_states[ACTRL_ACCEL_UP]) {
		mcm.c_brakes=0;

		mcm.c_accel=(mcm.c_accel+0.01f)*1.5f;
		if (mcm.c_accel>1) mcm.c_accel=1;
	} else mcm.c_accel=mcm.c_accel/2.0f;

	if (kbd_states[ACTRL_ACCEL_DOWN]) {
		mcm.c_accel=0;

		mcm.c_brakes=(mcm.c_brakes+0.01f)*1.5f;
		if (mcm.c_brakes>1) mcm.c_brakes=1;
	} else mcm.c_brakes=0;

//steer
	f32 steerratio;
	if (mcm.r_speed>50) steerratio=(f32)0.05;
	else steerratio=(f32)0.2;
	if (kbd_states[ACTRL_STEER_LEFT]) {
		mcm.c_steer=(f32)max(double(mcm.c_steer-steerratio), (double)-1);
	} else if (kbd_states[ACTRL_STEER_RIGHT]) {
		mcm.c_steer=(f32)min(double(mcm.c_steer+steerratio), (double)1);
	} else {
		mcm.c_steer=mcm.c_steer/1.5f;
		if (mcm.c_steer<0.05 && mcm.c_steer>-0.05)
			mcm.c_steer=0;
	}

//gear
	if (kbd_states[ACTRL_GEAR_UP]) {
		if (kbd_states_fti[ACTRL_GEAR_UP]==0) {
			mcm.c_gear++;
			if (mcm.c_gear>=mcm.maxgear)
				mcm.c_gear=mcm.maxgear-1;
		}
		kbd_states_fti[ACTRL_GEAR_UP]=100;
	} else kbd_states_fti[ACTRL_GEAR_UP]=0;

	if (kbd_states_fti[ACTRL_GEAR_UP])
		kbd_states_fti[ACTRL_GEAR_UP]--;

	if (kbd_states[ACTRL_GEAR_DOWN]) {
		if (kbd_states_fti[ACTRL_GEAR_DOWN]==0) {
			mcm.c_gear--;
			if (mcm.c_gear<0)
				mcm.c_gear=0;
		}
		kbd_states_fti[ACTRL_GEAR_DOWN]=100;
	} else kbd_states_fti[ACTRL_GEAR_DOWN]=0;

	if (kbd_states_fti[ACTRL_GEAR_DOWN])
		kbd_states_fti[ACTRL_GEAR_DOWN]--;

	if (kbd_states[ACTRL_CHANGECAMTGT]) {
		if (kbd_states_fti[ACTRL_CHANGECAMTGT]==0) {
			cam_tgt++;
			if (unsigned(cam_tgt)>=carmodels.size()) cam_tgt=0;
			candies->RenewAllSpecific();
			kbd_states_fti[ACTRL_CHANGECAMTGT]=100;
		}
	} else kbd_states_fti[ACTRL_CHANGECAMTGT]=0;
//clutch
	if (kbd_states[ACTRL_CLUTCH]) {
		if (mcm.c_clutch!=1) {
			mcm.c_clutch=(f32)((mcm.c_clutch+0.1)*1.5);
			if (mcm.c_clutch>1) mcm.c_clutch=1;
		}
	} else {
		if (mcm.c_clutch!=0) {
			mcm.c_clutch=(f32)((mcm.c_clutch-0.1)*0.5);
			if (mcm.c_clutch<0) mcm.c_clutch=0;
		}
	}

//handbrake
	if (kbd_states[ACTRL_HANDBRAKE])
		mcm.c_handbrake=(f32)min(mcm.c_handbrake+0.4, (double)1);
	else {
		mcm.c_handbrake=mcm.c_handbrake/10;
		if (mcm.c_handbrake<0.2) mcm.c_handbrake=0.0f;
	}

//rescue car :)
	if (kbd_states[ACTRL_RESCUE])
		mcm.c_rescue=(f32)min(mcm.c_rescue+0.4, (double)1);
	else {
		mcm.c_rescue=mcm.c_rescue/10;
		if (mcm.c_rescue<0.2) mcm.c_rescue=0.0f;
	}

//switches
#define myswitch(ACTRLITEM, VARIABLE) \
	if (kbd_states[ACTRLITEM]) { \
		if (kbd_states_fti[ACTRLITEM]==0) { \
			VARIABLE=!VARIABLE; \
		} \
		kbd_states_fti[ACTRLITEM]=100; \
	} else kbd_states_fti[ACTRLITEM]=0;

	myswitch(ACTRL_SHOWCONTROLS, show_controls)
	myswitch(ACTRL_SHOWDEBUGINFO, show_debuginfo)
	bool oldsd=show_simdebug;
	myswitch(ACTRL_SHOWSIMDEBUG, show_simdebug)
	if (show_simdebug!=oldsd) {
		for (std::deque<CCarModel>::iterator i=carmodels.begin(); i!=carmodels.end(); i++) {
			i->show_debug(show_simdebug);
		}
		for (std::deque<CTerrainPart>::iterator i=terrainParts.begin(); i!=terrainParts.end(); i++) {
			i->show_debug(show_simdebug);
		}
	}
#undef myswitch
}

bool CGame::OnEvent(const SEvent &event)
{
	if (event.EventType == EET_KEY_INPUT_EVENT) {
		switch (event.KeyInput.Key) {
			case KEY_ESCAPE:
				if (event.KeyInput.PressedDown == false) {
					if (!closeWindow) {
						gui::IGUIEnvironment* guienv = device->getGUIEnvironment();
						{
							for (s32 i=0; i<gui::EGDC_COUNT ; ++i) {
								video::SColor col = guienv->getSkin()->getColor((gui::EGUI_DEFAULT_COLOR)i);
								col.setAlpha(190);
								guienv->getSkin()->setColor((gui::EGUI_DEFAULT_COLOR)i, col);
							}
						}
						closeWindow = guienv->addWindow(CenterRect(300, 170, guienv), true, L"Game menu", 0);
						closeWindow->getCloseButton()->setEnabled(false);
						guienv->addStaticText(L"What do you want to do?", core::rect<int>(10, 20, 290, 60), false, true, closeWindow);
						guienv->addButton(irr::core::rect<s32>(10, 70, 290, 95), closeWindow, 501, L"Return to game");
						guienv->addButton(irr::core::rect<s32>(10, 100, 290, 125), closeWindow, 502, L"Restart race")->setEnabled(adminMode);
						guienv->addButton(irr::core::rect<s32>(10, 130, 290, 155), closeWindow, 503, L"Quit game");
						device->getCursorControl()->setVisible(true);
						closeWindow->setEnabled(true);
					} else {
MSGBOX_QUIT_RESUME:
						if (closeWindow) {
							closeWindow->remove();
							closeWindow=0;
						}
						device->getCursorControl()->setVisible(false);
					}
				}
				break;
			case KEY_KEY_E:
				cam_distance-=0.5;
				break;
			case KEY_KEY_T:
				cam_distance+=0.5;
				break;
			case KEY_KEY_D:
				cam_rot.X-=M_PI/(5*cam_distance);
				break;
			case KEY_KEY_G:
				cam_rot.X+=M_PI/(5*cam_distance);
				break;
			case KEY_KEY_R:
				cam_rot.Z-=M_PI/(5*cam_distance);
				if (cam_rot.Z<-M_PI/2) cam_rot.Z=-M_PI/2;
				break;
			case KEY_KEY_F:
				cam_rot.Z+=M_PI/(5*cam_distance);
				if (cam_rot.Z>M_PI/2) cam_rot.Z=M_PI/2;
				break;
			case KEY_KEY_V:
				cam_distance=5;
				cam_rot=core::vector3df(0, 0, -M_PI/8);
				break;
			case KEY_KEY_Q:
				if (event.KeyInput.PressedDown==false && closeWindow) goto MSGBOX_QUIT_QUIT;
				break;
			default:
				kbd_states[event.KeyInput.Key]=event.KeyInput.PressedDown;
				break;
		}
	} else if (event.EventType==EET_GUI_EVENT) {
		if (event.GUIEvent.EventType==gui::EGET_BUTTON_CLICKED)
		switch (event.GUIEvent.Caller->getID()) {
		case 503:
MSGBOX_QUIT_QUIT:
			// user wants to quit.
			if (closeWindow) {
				quitGame=1;
				closeWindow=0;
			}
			break;
		case 501:
			goto MSGBOX_QUIT_RESUME;
		case 502: //restart
			ags->connection->SendString("%:"AC_RESTART);
			goto MSGBOX_QUIT_RESUME;
		}
	}

	return false;
}


CGameCameraPos::CGameCameraPos(long time_lagged)
{
    this->time_lagged = time_lagged;
}

void CGameCameraPos::setCurrentPosition(core::vector3df current_position, u32 realtime, u32 w)
{
    // Add the details of this camera position and the current time.
    camera_details cam;
    cam.pos = current_position;
    cam.time = realtime;
	cam.weight=w;
    camera_positions.push_back(cam);
}

core::vector3df CGameCameraPos::getSmooothedPosition(u32 realtime)
{
    // Remove all the camera positions older than getRealTime() - time_lagged.
    long threshold = realtime - time_lagged;
	int j;

    std::deque<camera_details>::iterator i;

	for (j=0, i = camera_positions.begin(); i != camera_positions.end(); i++) {
		if (i->time >= threshold)
			break;
		j++;
	}

	while (j) {
		camera_positions.pop_front();
		j--;
	}

	core::vector3df total(0.0, 0.0, 0.0);

	for (j=0, i = camera_positions.begin(); i != camera_positions.end(); i++) {total += (i->pos)*((f32)i->weight); j+=i->weight; if (i->weight>1) i->weight/=2;}

    total /= float(j);

    return total;
}

void CGameCameraPos::clear()
{
    camera_positions.clear();
}
