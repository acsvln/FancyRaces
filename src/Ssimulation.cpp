/**
 * \file
 * This file contains implementation of SSimulation classe.
**/

#include "Ssimulation.h"
#include "Autil.h"

using namespace std;
using namespace irr;

SSimulation::SSimulation(irr::IrrlichtDevice *_device) : 
	odeSpace(0), lasttime(0), contactgroup(), device(_device), 
	checkpoints_space(odeSpace), surfaceContactProperties(0), currentFinish(0)
{
	CfgFile cfg2;
	string buf;
	int i, j, s;

	dInitODE();
	
	cfg2.Load(device->getFileSystem(), "data/settings.ini");

	for (i=0;; i++) {
		buf="Physics/Surface_"+numtostring(i+1)+"/";
		if (cfg2.Getval_exists(buf+"name")) {
			surfaces[cfg2.Getval_str(buf+"name")]=i;
		} else break;
	}

	s=i;
	SSimulation_surface_contact_properties dp;
	buf=cfg2.Getval_str("Physics/SurfaceContactProperties/default", "1.5 0.1 45 100");
	sscanf(buf.c_str(), "%g %g %g %g", &dp.mu, &dp.slip, &dp.damp, &dp.spring);

	surfaceContactProperties=new SSimulation_surface_contact_properties*[s];
	for (i=0; i<s; i++) {
		surfaceContactProperties[i]=new SSimulation_surface_contact_properties[s];
		for (j=0; j<s; j++) surfaceContactProperties[i][j]=dp;
	}
	
	for (i=0; i<s; i++) {
		for (j=i; j<s; j++) {
			buf=cfg2.Getval_str("Physics/SurfaceContactProperties/"+numtostring(i+1)+"+"+numtostring(j+1), "");
			if (buf=="") continue;
			SSimulation_surface_contact_properties *p=&surfaceContactProperties[i][j];
			sscanf(buf.c_str(), "%g %g %g %g", &p->mu, &p->slip, &p->damp, &p->spring);
			surfaceContactProperties[j][i]=surfaceContactProperties[i][j];
		}
	}
/*
	for (i=0; i<s; i++) {
		for (j=0; j<s; j++) {
			cout << i+1 << ":" << j+1 << "=> " << (int)surfaceContactProperties[i][j].mu << "|" << (int)surfaceContactProperties[i][j].slip << "|" << (int)surfaceContactProperties[i][j].damp << "|" <<(int)surfaceContactProperties[i][j].spring << "\t";
		}
		cout << endl;
	}
*/
	//another nasty fix - because we need to send position data before simulation begins
	starttime=device->getTimer()->getTime();
	lasttime=starttime;
}

void SSimulation::SetWorld(std::string file)
{
	scene::ISceneManager* sm = device->getSceneManager();

	cfg.Load(device->getFileSystem(), "data/maps/"+file+"/config.ini");
	odeWorld.setGravity(0, (dReal)cfg.Getval_double("Physics/gravity", -9.81), 0);
	
	//now initialize terrain from "map"
	char buf[100];
	string buf2;
	int i;
	bool loadit;
	SObjectGroup *ter;

	for (i=0;; i++) {
		sprintf(buf, "Terrain/Object_%d/", i+1);
		buf2=buf;
		loadit=false;
		if (cfg.Getval_flag(buf2+"skip", "yes")) continue;
		else if (cfg.Getval_exists(buf2+"file")) {
			loadit=true;
		} else if (cfg.Getval_exists(buf2+"type")) {
			if (!cfg.Getval_exists(buf2+"Physics/surface")) continue;
			ter=0;

			if (cfg.Getval_str(buf2+"type", "")=="box") {
				loadit=true;
			} else if (cfg.Getval_str(buf2+"type", "")=="sphere") {
				loadit=true;
			} else {
				DBGCOUT("SSimulation", "SetWorld", "Don't know what "+cfg.Getval_str(buf2+"type", "")+" is...");
				continue;
			}
		} else break;

		if (loadit) {
			ter=new SObjectGroup();
			ter->Load(file, cfg, buf2, odeWorld, odeSpace, sm, moveableObjects, surfaces);
			terrain.push_back(ter);
		}
	}
	
	if (cfg.Getval_str("Checkpoints/order", "sequential")=="random") checkpoints_order=RANDOM;
	else checkpoints_order=SEQUENTIAL;

	//checkpoints
	for (i=0;; i++) {
		sprintf(buf, "Checkpoints/Check_%d/", i+1);
		buf2=buf;
		if (cfg.Getval_exists(buf2+"pos")) {
			SGameplace *gp;
			gp=new SGameplace();
			gp->Init(cfg, buf2, checkpoints_space, i);
			checkpoints_objects.push_back(gp);
		} else break;
	}
	checkpoint_finish=new SGameplace();
	checkpoint_finish->Init(cfg, "Checkpoints/Finish/", checkpoints_space, -1);

	DBGCOUT("SSimulation", "setworld", "done");
}

int SSimulation::AddPlayer(std::string carprofile, const dVector3 &pos)
{
	SVehicle *v=new SVehicle(odeWorld, odeSpace, device->getFileSystem(), carprofile, pos, (int)checkpoints_objects.size(), surfaces);
	v->simid=(int)vehicles.size();
	vehicles.push_back(v);
	return (int)(vehicles.size()-1);
}

bool SSimulation::GetPlayerInfo(unsigned int id, SVehicle **v)
{
	if (id==-1 || id>=vehicles.size())
		return false;
	
	*v=vehicles[id];

	return true;
}

void SSimulation::Start()
{
	starttime=device->getTimer()->getTime();
	lasttime=starttime;

	for (std::deque<SVehicle*>::iterator i=vehicles.begin(); i!=vehicles.end(); i++) {
		for (size_t j=0; j<checkpoints_objects.size(); j++) (*i)->checkpoints[j]=false;
	}
}

void SSimulation::nearCallback (void *data, dGeomID o1, dGeomID o2)
{
	SSimulation *ss=(SSimulation*)data;
	enum {
		MAX_CONTACTS=10
	};

	if (dGeomIsSpace(o1) ||	dGeomIsSpace(o2)) {
		// colliding a space with something
		dSpaceCollide2(o1, o2, data, nearCallback);

		// collide all geoms internal to the space(s)
		if (dGeomIsSpace(o1)) dSpaceCollide((dSpaceID)o1, data, nearCallback);
		if (dGeomIsSpace(o2)) dSpaceCollide((dSpaceID)o2, data, nearCallback);
	} else {

		dContact contact[MAX_CONTACTS];
		dBodyID	b1 = dGeomGetBody(o1);
		dBodyID	b2 = dGeomGetBody(o2);

		// return without doing	anything if	both bodies	are	part of	the	static environment.
		if (b1 == 0	&& b2 == 0)	return;

		SGeomObj*g1=(SGeomObj*)dGeomGetData(o1);
		SGeomObj*g2=(SGeomObj*)dGeomGetData(o2);
		if ((g1->type==SGeomObj::CAR_CHASSIS && g2->type==SGeomObj::GAMEPLACE) ||
			(g2->type==SGeomObj::CAR_CHASSIS && g1->type==SGeomObj::GAMEPLACE)) {
				int	numc = dCollide(o1,	o2,	MAX_CONTACTS, &contact[0].geom,	sizeof(dContact));
				if (numc>0) {
					SVehicle_chassis *v;
					SGameplace *g;
					if (g1->type==SGeomObj::GAMEPLACE) { //swap to correct order
						v=(SVehicle_chassis*)g2;
						g=(SGameplace*)g1;
					} else {
						v=(SVehicle_chassis*)g1;
						g=(SGameplace*)g2;
					}

					SSimulation_event sse;
					//cerr << "DBG: Car " << v->belongs_to->simid << " went through checkpoint " << g->GetParam() << endl;
					if (g->GetParam()<0) { //finish
						sse.type=sse.CROSSED_FINISH;
						for (size_t j=0; j<ss->checkpoints_objects.size(); j++) if (!v->belongs_to->checkpoints[j]) return; //do nothing, if not all checkpoints are crossed
						for (size_t j=0; j<ss->checkpoints_objects.size(); j++) v->belongs_to->checkpoints[j]=false; //went thru finish - can eventually make another lap ;-)
						v->belongs_to->nlaps++;
						sse.crossFinish.vehicleId=v->belongs_to->simid;
						sse.crossFinish.laps=v->belongs_to->nlaps;
						ss->events.push_back(sse);
						if (v->belongs_to->nlaps==ss->laps) v->belongs_to->finishorder=ss->currentFinish++;

						//check whether all players finished enough rounds, finish race if yes
						bool af=true;
						for (std::deque<SVehicle*>::iterator it=ss->vehicles.begin(); it!=ss->vehicles.end(); it++) {
							if ((*it)->nlaps<ss->laps && !(*it)->removeFromRace) af=false;
						}
						if (af) {
							sse.type=sse.RACE_FINISHED;
							ss->events.push_back(sse);
						}
					} else { //checkpoint
						sse.type=sse.CROSSED_CHECKPOINT;
						if (v->belongs_to->checkpoints[g->GetParam()]) return; //already crossed this one
						if (ss->checkpoints_order==ss->RANDOM) {
							v->belongs_to->checkpoints[g->GetParam()]=true;
						} else if (ss->checkpoints_order==ss->SEQUENTIAL) {
							if (g->GetParam()==0)
								v->belongs_to->checkpoints[0]=true;
							else {
								if (!v->belongs_to->checkpoints[g->GetParam()-1]) return; //didn't cross the previous checkpoint, loser =)
								v->belongs_to->checkpoints[g->GetParam()]=true;
							}
						}
						sse.crossCheckpoint.vehicleId=v->belongs_to->simid;
						sse.crossCheckpoint.checkpoint=g->GetParam();
						ss->events.push_back(sse);
					}
					//cerr << "Car " << v->belongs_to->simid << " went through checkpoint " << g->GetParam() << endl;
				}
				return;
		}

		//if one of the objects is gameplace, skip the collision
		if (g1->type==SGeomObj::GAMEPLACE || g2->type==SGeomObj::GAMEPLACE) return;

		// return without doing	anything if	the	two	bodies are connected by	a joint
		if (b1 && b2 &&	dAreConnectedExcluding(b1, b2, dJointTypeContact)) return;

		//do nothing if both objects belong to same vehicle
		if ((g1->type==SGeomObj::CAR_CHASSIS || g1->type==SGeomObj::CAR_WHEEL) && (g2->type==SGeomObj::CAR_CHASSIS || g2->type==SGeomObj::CAR_WHEEL)) {
			SVehicle *bt1, *bt2;
			if (g1->type==SGeomObj::CAR_CHASSIS) bt1=((SVehicle_chassis*)g1)->belongs_to;
			else bt1=((SVehicle_wheel*)g1)->belongs_to;
			if (g2->type==SGeomObj::CAR_CHASSIS) bt2=((SVehicle_chassis*)g2)->belongs_to;
			else bt2=((SVehicle_wheel*)g2)->belongs_to;
			if (bt1==bt2) return;
		}

		if (int	numc = dCollide(o1,	o2,	MAX_CONTACTS, &contact[0].geom,	sizeof(dContact))) {
			for	(int i=0; i	< numc;	i++) {
				SSimulation_surface_contact_properties cp=ss->surfaceContactProperties[g1->surface_type][g2->surface_type];
				contact[i].surface.mode	= dContactSlip1	| dContactSlip2	| dContactSoftERP |	dContactSoftCFM	| dContactApprox1;

				if (g1->type==SGeomObj::CAR_WHEEL || g2->type==SGeomObj::CAR_WHEEL) {
					SGeomObj *go=g1;
					if (g2->type==SGeomObj::CAR_WHEEL) go=g2;

					float inv=(float)((((SVehicle_wheel*)g2)->left)?-1:1);
					dReal const * vel = go->ode_body->getLinearVel();
					float colVel= core::vector3df(vel[0], vel[1], vel[2]).dotProduct(
						core::vector3df(contact[i].geom.normal[0], contact[i].geom.normal[1], contact[i].geom.normal[2]));

					//if two wheels collide, the result is kinda undefined (it's way too complicated to simulate, i guess)... who cares anyway :)
					core::vector3df front;
					core::vector3df up;
					//Compute fDir1
					dReal const * R = go->ode_body->getRotation();
					
					//http://www.cubic.org/docs/camera.htm - conversion from 3 vectors to matrix
					front = core::vector3df( R[2] * inv, R[6] * inv, R[10] * inv );
					up = core::vector3df( R[1] * inv, R[5] * inv, R[9] * inv );
					//Set Slip2
					contact[i].surface.mode |= dContactSlip2 | dContactFDir1;
					float v = sqrtf(vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2]);

					contact[i].surface.slip1 = cp.slip;
					contact[i].surface.slip2 = cp.slip*v/10; //set slipping in the second direction according to the speed and contact properties
					contact[i].surface.mu = contact[i].surface.mu2 = cp.mu;
					dVector3 front2;
					front2[0]=front.X;
					front2[1]=front.Y;
					front2[2]=front.Z;
					if( fabsf( dDot( contact[i].geom.normal, front2, 3 ) ) > 0.5f ) {
						vector_convert(up, contact[i].fdir1);
					} else {
						vector_convert(front, contact[i].fdir1);
					}
				} else {
					//if ((g1->surface_type==3 && g2->surface_type==4) || (g1->surface_type==4 && g2->surface_type==3)) cerr << ".";c
					//cerr << g1->surface_type+1 << g2->surface_type+1 << ";";
					//cerr << cp.mu << "; ";
					contact[i].surface.mu =	cp.mu;
					contact[i].surface.slip1 = cp.slip;
					contact[i].surface.slip2 = cp.slip;
				}
				double h=ss->currentStepSize;
				double ERP = h*cp.spring / (h*cp.spring + cp.damp);
				double CFM = 1 / (h*cp.spring + cp.damp);
				contact[i].surface.soft_erp	= (dReal)ERP;
				contact[i].surface.soft_cfm	= (dReal)CFM;

				dJointID c = dJointCreateContact (ss->odeWorld, ss->contactgroup, &contact[i]);

				dJointAttach (c,b1,b2);
			}
		}
	}
}

bool SSimulation::Step()
{
	bool simulated=false;

	if (lasttime==0) {
		lasttime=device->getTimer()->getTime();
		return false;
	}

	dReal time=(dReal)((device->getTimer()->getTime()-lasttime)/1000.0);
	//dReal time=0.05;

	if (time>=0.01) {//limit iterations per second
		if (time>0.05) time=(dReal)0.05;
		simulated=true;
		currentStepSize=time;
		for (deque<SVehicle*>::iterator i=vehicles.begin(); i!=vehicles.end(); i++)
			(*i)->ApplyControls(time);
		odeSpace.collide(this, nearCallback);
		//	DBGCOUT("SSimulation", "Step", "Time:" + numtostring(time));
		odeWorld.step(time);
		lasttime=device->getTimer()->getTime();
	
		contactgroup.empty();
	}
	return simulated;
}

void SSimulation::RemoveFromRace(int simid)
{
	for (deque<SVehicle*>::iterator i=vehicles.begin(); i!=vehicles.end(); i++) {
		if ((*i)->simid==simid) {
			(*i)->removeFromRace=true;
			break;
		}
	}
}

SSimulation::~SSimulation()
{
	while (!vehicles.empty()) {
		SVehicle *v=vehicles.front();
		vehicles.pop_front();
		delete v;
	}

	while (!terrain.empty()) {
		SObjectGroup *t=terrain.front();
		terrain.pop_front();
		delete t;
	}

	while (!checkpoints_objects.empty()) {
		SGameplace *t=checkpoints_objects.front();
		checkpoints_objects.pop_front();
		delete t;
	}
}
