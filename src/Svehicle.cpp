/**
 * \file
 * This file contains implementation of SVehicle_wheel, SVehicle_chassis and SVehicle classes.
**/

#include "Svehicle.h"
#include "Autil.h"
#include "../_others/raycar/deps/include/ode/mass.h"

using namespace std;
using namespace irr;

void SVehicle_wheel::Load(dWorld &world, dSpace &space, CfgFile &cfg, std::string buf2, SVehicle *vehicle, const dVector3 &pos, std::map<std::string, int> &surfaceTypes)
{
	dMass m;
	map<string, unsigned int> wheelattr;
	wheelattr["STRAIGHT"]=WHEEL_STRAIGHT;
	wheelattr["STEER"]=WHEEL_STEER;
	wheelattr["REVERSED"]=WHEEL_REVERSED;
	wheelattr["THURST"]=WHEEL_THURST;

	ode_body = new dBody(world.id());
	dQuaternion q;
	dQFromAxisAndAngle(q, 1, 0, 0, M_PI*0.5);
	ode_body->setQuaternion(q);
	radius=(dReal)cfg.Getval_double(buf2+"radius", cfg.Getval_double("Body/Wheel/radius", 1));
	brakes=(dReal)cfg.Getval_double(buf2+"brakes", cfg.Getval_double("Body/Wheel/brakes", 1));
	//m.setSphere(1, (dReal)radius);
	dMassSetCylinder(&m, 1, 1, (dReal)radius, (dReal)
		(dReal)cfg.Getval_double(buf2+"width", cfg.Getval_double("Body/Wheel/width", 0.2)));
	m.adjust((dReal)cfg.Getval_double(buf2+"weight", cfg.Getval_double("Body/Wheel/weight", 20)));
	ode_body->setMass(&m);
	//ode_body->setFiniteRotationMode(1);
	ode_geom = new dSphere(0, (dReal)radius);
	//ode_geom = new dCapsule(0, radius, 0.2);
	belongs_to=vehicle;
	ode_geom->setData((SGeomObj*)this);
	ode_geom->setBody(ode_body->id());

	irr::core::vector3df v=cfg.Getval_vector(buf2+"pos")+irr::core::vector3df(pos[0], pos[1], pos[2]);
	ode_body->setPosition(v.X, v.Y, v.Z);
	left=cfg.Getval_vector(buf2+"pos").Z<0;

	attr=cfg.Getval_flags(buf2+"attr", wheelattr, 0);

	jid = new dHinge2Joint(world.id(),0);

	jid->attach(vehicle->chassis->ode_body->id(), ode_body->id());
	const dReal *a	= ode_body->getPosition();
	jid->setAnchor(a[0],a[1],a[2]);

	if (attr&WHEEL_STEER) {
		jid->setAxis1(0,1,0);
		jid->setAxis2(0,0,1);
	} else if (attr&WHEEL_STRAIGHT) {
		//set stops to make sure wheels always stay in alignment
		jid->setAxis1(1,0,0); //TODO: is this the right way to stop thurst wheels from rotating along vertical axis?
		jid->setAxis2(0,0,1);
		jid->setParam(dParamLoStop,0);
		jid->setParam(dParamHiStop,0);
		jid->setParam(dParamFMax,0);
		jid->setParam(dParamStopERP, 0.99f );
		jid->setParam(dParamStopCFM, 0.001f );
	}

	//set joint suspension
	kp=(dReal)cfg.Getval_double(buf2+"spring", cfg.Getval_double("Body/Wheel/spring", 1));
	kd=(dReal)cfg.Getval_double(buf2+"damp", cfg.Getval_double("Body/Wheel/damp", 1));

	surface_type=surfaceTypes["rubber"];

	vehicle->car_space->add(ode_geom->id());
}

void SVehicle_chassis::Load(dWorld &world, dSpace &space, CfgFile &cfg, SVehicle *vehicle, const dVector3 &posoff, std::map<std::string, int> &surfaceTypes)
{
	dMass tm, m;
	int i;
	deque<dGeom *> g2d;
	ode_body = new dBody(world);

	totalWeight=0;
	//load all chassis parts - create "geom group" and set body attributes
	tm.setZero();
	parts.clear();
	g2d.clear();
	for (i=0;; i++) {
		if (!cfg.Getval_exists("Body/Chassis/"+numtostring(i+1)+"/scale")) break;
		if (cfg.Getval_exists("Body/Chassis/"+numtostring(i+1)+"/skip")) continue;
		core::vector3df size=cfg.Getval_vector("Body/Chassis/"+numtostring(i+1)+"/scale", core::vector3df(1, 1, 1));
		core::vector3df rot=cfg.Getval_vector("Body/Chassis/"+numtostring(i+1)+"/rot", core::vector3df(0, 0, 0))/180*M_PI;
		core::vector3df pos=cfg.Getval_vector("Body/Chassis/"+numtostring(i+1)+"/pos", core::vector3df(0, 0, 0));
		m.setZero();
		double w=(dReal)cfg.Getval_double("Body/Chassis/"+numtostring(i+1)+"/weight", 0.0001);
// 		printf("CHASSIS %g %g %g %g\n", (double)(dReal(w/(
// 			size.X*size.Y*size.Z))), size.X, size.Y, size.Z);
		//TODO: why using libode-sp?
		m.setBox(dReal(w/(
			size.X*size.Y*size.Z)),
			size.X, size.Y, size.Z);
// 		printf("CHASSIS xxx %g (%g %g %g)\n", m.mass, m.I(0,0), m.I(1,1), m.I(2,2));
		totalWeight+=w;
		dQuaternion q;
		dMatrix3 rm;
		irr::core::quaternion qi(rot.X, rot.Y, rot.Z);
		q[0]=qi.W; q[1]=qi.X; q[2]=qi.Y; q[3]=qi.Z;
		dQtoR(q, rm);

		//move mass
		m.translate(pos.X, pos.Y, pos.Z);
		m.rotate(rm);

		dGeomTransform *g=new dGeomTransform(space.id());
		g->setData((SGeomObj*)this);

		parts.push_back(g);
		g->setCleanup(1); //remove child geom when deleted
		dGeom *g2;

		g2=new dBox(0, size.X, size.Y, size.Z);
		g->setGeom(g2->id());
		//move geom
		g2->setPosition(pos.X, pos.Y, pos.Z);
		g2->setRotation(rm);
		g2d.push_back(g2);
		tm.add(&m);
	}
/*
	//now move all objects so that center of mass is (0, 0, 0) - required according to ODE documentation, but works without it :-/
	for (i=0; i<(signed)g2d.size(); i++) {
		core::vector3df pos=cfg.Getval_vector("Body/Chassis/"+numtostring(i+1)+"/pos", core::vector3df(0, 0, 0));
		g2d[i]->setPosition(pos.X-m.c[0], pos.Y-m.c[1], pos.Z-m.c[2]);
	}
	m.translate(-m.c[0],-m.c[1],-m.c[2]);
*/
	for (i=0; i<(signed)parts.size(); i++) {
		parts[i]->setBody(ode_body->id());
	}

	tm.translate(-tm.c[0], -tm.c[1], -tm.c[2]);
	ode_body->setMass(&tm);
	ode_body->setPosition(posoff[0], posoff[1], posoff[2]);
	
	belongs_to=vehicle;
	surface_type=surfaceTypes["steel"];
}

SVehicle::SVehicle(dWorld &world, dSpace &space, irr::io::IFileSystem *ifs, std::string config, const dVector3 &pos, int ncheckpoints, std::map<std::string, int> &surfaceTypes)
{
	int i;
	char buf[200];
	string buf2;
	CfgFile cfg;

	removeFromRace=false;
	nlaps=0;
	c_accel=0;
	c_steer=0;
	c_clutch=0;
	c_handbrake=0;
	c_brakes=0;
	c_rescue=0;
	finishorder=-1;

	cfg.Load(ifs, "data/cars/"+config+"/config.ini");

	//create car space and add it to the top level space
	car_space = new dSimpleSpace(space.id());

	chassis=new SVehicle_chassis();
	chassis->Load(world, *car_space, cfg, this, pos, surfaceTypes);

	wheels.clear();
	//	wheel bodies
	for (i=0;; i++) {
		sprintf(buf, "Body/Wheel_%d/", i+1);
		buf2=buf;
		if (!cfg.Getval_exists(buf2+"attr")) break;

		wheels.push_back(SVehicle_wheel()); //this is nasty

		wheels[i].Load(world, space, cfg, buf2, this, pos, surfaceTypes);
	}

	//physics
	torquecurve=new double[TORQUECURVENO];
	Point2D *curve=new Point2D[TORQUECURVENO];

	Point2D cp[4];
	for (i=0; i<4; i++) {
		sprintf(buf, "Dynamics/Engine/torque_%dx", i+1);
		cp[i].x=cfg.Getval_double(buf, 0);
		sprintf(buf, "Dynamics/Engine/torque_%dy", i+1);
		cp[i].y=cfg.Getval_double(buf, 0);
	}

	Bezier_Compute(cp, TORQUECURVENO, curve);
	rpm_min=(int)cp[0].x;
	rpm_max=(int)cp[3].x;
	for (i=0; i<TORQUECURVENO; i++)
		torquecurve[i]=curve[i].y;

	//get gearbox config
	gearbox_no=cfg.Getval_int("Dynamics/Gearbox/gear_count", 0);
	gearbox=new double[gearbox_no];
	gearbox_N=cfg.Getval_int("Dynamics/Gearbox/gear_N", -1);
	gearbox_R=cfg.Getval_int("Dynamics/Gearbox/gear_R", -1);

	for (i=0; i<gearbox_no; i++) {
		sprintf(buf, "Dynamics/Gearbox/gear_%d", i);
		gearbox[i]=cfg.Getval_double(buf, 0);
	}
	differential=cfg.Getval_double("Dynamics/Gearbox/differential", 1);
	trans_eff=cfg.Getval_double("Dynamics/Gearbox/transmission_efficiency", 0.7);
	brakes_maxforce=cfg.Getval_double("Dynamics/Brakes/max_brakeforce", 500);
	handbrake_maxforce=cfg.Getval_double("Dynamics/Brakes/max_handbrakeforce", 1500);
	breaking_power=cfg.Getval_double("Dynamics/Engine/breaking_power", 0.25);
	rollResistance=cfg.Getval_double("Dynamics/Tires/rolling_resistance", 3.1);
	aeroDrag=cfg.Getval_double("Dynamics/Aero/drag_constant", 0.4);

	checkpoints=new bool[ncheckpoints];
	for (i=0; i<ncheckpoints; i++) checkpoints[i]=false;
}

double SVehicle::GetCurrentGearRatio()
{
	if (c_gear>gearbox_no || c_gear<0)
		return 0;
	return gearbox[c_gear]*differential;
}

int SVehicle::GetRPM(bool engine)
{
	double rpm=0;
	int i, j=0;
	for (i=0; i<(signed)wheels.size(); i++) {
		if ( (engine && wheels[i].attr&WHEEL_THURST) || (!engine && (wheels[i].attr&WHEEL_THURST)) ) {
			rpm+=wheels[i].jid->getAngle2Rate();
			j++;
		}
	}
	rpm/=(double)j; //average wheel rpm (TODO: use differential)

	if (engine) {
		double cgr=GetCurrentGearRatio();
		if (cgr!=0) {
			rpm*=cgr*60/(2*M_PI);
		} else rpm=0;
		if (rpm<rpm_min) rpm=rpm_min;
		else if (rpm>rpm_max) rpm=rpm_max;
	}

	return (int)rpm;
}

int SVehicle::GetSpeed()
{
	int war=GetRPM(false);
	int i;
	double r=0;
	
	//get average wheel radius
	for (i=0; i<(signed)wheels.size(); i++)
		r+=(double)wheels[i].radius;
	r=r/(double)wheels.size();

	return (int)((double)war*r*3.6);
}

void SVehicle::ApplyControls(double stepsize)
{
	int i, j;

	double thurst_torque=0;
	double cur_fwrpm;

	//check controls ranges
	if (c_gear>=gearbox_no)
		c_gear=gearbox_no-1;
	else if (c_gear<0) c_gear=0;

	double cgr=GetCurrentGearRatio();

	cur_fwrpm=GetRPM();

	//calculate torque from engine until the differential
	if ((c_accel*(rpm_max-rpm_min)+rpm_min)>cur_fwrpm) { //if the gas is pushed more than the actual rpm -> we want to gain more speed
		thurst_torque=GetTorqueAt((int)cur_fwrpm)*cgr*trans_eff*(1.0-c_clutch)*c_accel;
	} else if (cur_fwrpm>rpm_min) { //while the current rpm is bigger than minimal,  slow down
		thurst_torque=-GetTorqueAt((int)cur_fwrpm)*breaking_power*cgr*trans_eff*(1.0-c_clutch)*(1-c_accel);
	}

	for (j=0, i=0; i<(signed)wheels.size(); i++) if (wheels[i].attr&WHEEL_THURST) j++;
	thurst_torque/=j;

	for (i=0; i<(signed)wheels.size(); i++) {
		//apply rolling resistance torque
		wheels[i].jid->addTorques(0, (dReal)(
			-1* //convert to direction opposite to rolling
			wheels[i].radius* //convert force to torque
			rollResistance*(wheels[i].jid->getAngle2Rate())*wheels[i].radius //Crr * speed of the wheels in m/s
			)
		);

		//apply thurst
		if (wheels[i].attr&WHEEL_THURST) {
			wheels[i].jid->addTorques(0, (dReal)thurst_torque);
		}

		//apply brakes and handbrake
		if (c_handbrake>0.00001) { //simply stop the wheels
			wheels[i].jid->setParam(dParamVel2, (dReal)0);
			wheels[i].jid->setParam(dParamFMax2, (dReal)(handbrake_maxforce*c_handbrake));
		} else if (c_brakes>0.00001) {
			wheels[i].jid->setParam(dParamVel2, (dReal)0);
			wheels[i].jid->setParam(dParamFMax2, (dReal)(brakes_maxforce*c_brakes*wheels[i].brakes));
		} else {
			wheels[i].jid->setParam(dParamFMax2, (dReal)0);
		}

		//apply steering
		if (wheels[i].attr&WHEEL_STEER) {
			dReal v;
			if (wheels[i].attr&WHEEL_REVERSED)
				v=(dReal)(c_steer - wheels[i].jid->getAngle1());
			else v=(dReal)(c_steer + wheels[i].jid->getAngle1());

			v*=10.0;

			if (wheels[i].attr&WHEEL_REVERSED)
				wheels[i].jid->setParam(dParamVel, v);
			else wheels[i].jid->setParam(dParamVel, -v);
 
			wheels[i].jid->setParam(dParamFMax,1000);
			wheels[i].jid->setParam(dParamLoStop, (dReal)-0.75);
			wheels[i].jid->setParam(dParamHiStop, (dReal)0.75);
			wheels[i].jid->setParam(dParamFudgeFactor,(dReal)0.1);
		}

		//set damping+spring constants
		double h=stepsize;
		double ERP = h*wheels[i].kp / (h*wheels[i].kp + wheels[i].kd);
		double CFM = 1 / (h*wheels[i].kp + wheels[i].kd);
		wheels[i].jid->setParam(dParamSuspensionERP, (dReal)ERP);
		wheels[i].jid->setParam(dParamSuspensionCFM, (dReal)CFM);

		/*dVector3 axis;
	    wheels[i].jid->getAxis2(axis);
		wheels[i].ode_body->setFiniteRotationAxis(axis[0], axis[1], axis[2]);
		*/
	}

	//add aerodynamic drag force
	const dReal *sp=chassis->ode_body->getLinearVel();
	core::vector3df sp2(sp[0], sp[1], sp[2]);
	sp2=-sp2*(f32)aeroDrag*(f32)sp2.getLength(); //Fdrag=-Cd*v*|v|
	chassis->ode_body->addForce(sp2.X, sp2.Y, sp2.Z);

	//add rescue force, if reqired
	if (c_rescue>0.05) chassis->ode_body->addForceAtRelPos(0, (dReal)(c_rescue*5*chassis->totalWeight), 0, 10, 0, 4);
}

SVehicle::~SVehicle()
{
	delete chassis;
	
	delete []gearbox;
	delete []torquecurve;
	delete []checkpoints;
}

double SVehicle::GetTorqueAt(double point)
{
	int i=int(double(TORQUECURVENO)*point);
	
	if (point==0)
		return torquecurve[0];
	else if (point==1)
		return torquecurve[TORQUECURVENO-1];
	else {
		double ratio=(point-double(i)/double(TORQUECURVENO)); //position between
		return (1.0-ratio)*torquecurve[i]+ratio*torquecurve[i+1];
	}
}

double SVehicle::GetTorqueAt(int point)
{
	return GetTorqueAt(double(point-rpm_min)/double(rpm_max-rpm_min));
}
