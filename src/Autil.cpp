/**
 * \file
 * This file contains implementation of heler functions used across the whole application.
**/

#include "Autil.h"
#include <iostream>
#include <fstream>
#include <OpenThreads/Thread>
#include <OpenThreads/Mutex>
#include <irrlicht/irrlicht.h>
#include <ctype.h>

using namespace std;

static OpenThreads::Mutex mymutex;

static deque<string> cons;

void DBGCOUT(string sect, string action, string param)
{
	mymutex.lock();

	string str;

	str=sect + "::" + action;
	if (!param.empty())
		str += ": " + param;
	fprintf(stderr, "%s\n", str.c_str());
	cons.push_front(str);

	mymutex.unlock();
}

void DBGCOUT_data_beg() {mymutex.lock();}
deque<string>& DBGCOUT_data() {return cons;}
void DBGCOUT_data_end() {mymutex.unlock();}

void cmd_Map2String(cmd_map &mp, std::string &rv)
{
	rv="%:"+mp[(string)"%"];
	for (cmd_map::iterator i=mp.begin(); i!=mp.end(); i++) {
		if (i->first=="%") continue;
		rv+="\n"+i->first+":"+i->second;
	}
}

void cmd_String2Map(std::string &str, cmd_map &rv)
{
	string line="";
	string::iterator i;
	string::size_type p;
	string buf1, buf2;

	for (i=str.begin();; i++) {
		if (*i=='\n' || i==str.end()) {
			if (line=="" && i==str.end()) break; //fixes line ends at the end of string...

			p=line.find(":",0);
			if (p!=str.npos) rv[line.substr(0, p)]=line.substr(p+1);
			else rv[line]="OK";

			if (i==str.end()) break;
			line="";
		} else {
			line+=*i;
		}
	}
}

std::string numtostring(int i)
{
	char buf[100];
	sprintf(buf, "%d", i);
	return buf;
}

std::string numtostring(unsigned int i)
{
	char buf[100];
	sprintf(buf, "%u", i);
	return buf;
}

std::string numtostring(double i, const char* format)
{
	char buf[100];
	sprintf(buf, format, i);
	return buf;
}

char* u2a(const wchar_t* src)
{
	irr::core::string<char> str(src);

	char *result=new char[str.size()+1];
	memcpy(result, str.c_str(), str.size()*sizeof(char));
	result[str.size()]=0;
	return result;
}

wchar_t* a2u(const char* src)
{
	irr::core::string<wchar_t> str(src);

	wchar_t *result=new wchar_t[str.size()+1];
	memcpy((char*)result, str.c_str(), str.size()*sizeof(wchar_t));
	result[str.size()]=0;
	return result;
}

Point2D Bezier_PointOnCubicBezier(Point2D* cp, double t)
{
	double   ax, bx, cx;
	double   ay, by, cy;
	double   tSquared, tCubed;
	Point2D result;

	cx = 3.0 * (cp[1].x - cp[0].x);
	bx = 3.0 * (cp[2].x - cp[1].x) - cx;
	ax = cp[3].x - cp[0].x - cx - bx;

	cy = 3.0 * (cp[1].y - cp[0].y);
	by = 3.0 * (cp[2].y - cp[1].y) - cy;
	ay = cp[3].y - cp[0].y - cy - by;

	tSquared = t * t;
	tCubed = tSquared * t;

	result.x = (ax * tCubed) + (bx * tSquared) + (cx * t) + cp[0].x;
	result.y = (ay * tCubed) + (by * tSquared) + (cy * t) + cp[0].y;

	return result;
}

void Bezier_Compute(Point2D* cp, int numberOfPoints, Point2D* curve)
{
	double dt;
	int i;

	dt = 1.0 / ( numberOfPoints - 1 );

	for( i = 0; i < numberOfPoints; i++)
		curve[i] = Bezier_PointOnCubicBezier( cp, i*dt );
}



////////////////////////////////////GRAPHICS UTILITY FUNCTIONS...



using namespace irr;

const double GRAD_PI = 180.0 / 3.1415926535897932384626433832795;

scene::IMesh* CreateCylinder(int gridX, int gridY,f32 ridus)
{
	using namespace scene;
	SMesh* msh = new SMesh();      
	SMeshBuffer* mb = new SMeshBuffer();
	msh->addMeshBuffer(mb);
	f32 Tstp = 2*3.14f/(float)(gridX-1);
	int vertCnt = 0;
	for (int x = 0; x < gridX; x++) {
		for (int y = 0; y < gridY; y++) {
			f32 yy   = (f32)(0.5-(float)y/(gridY-1))*2;
			f32 xx   = sin((f32)x*Tstp)*ridus;
			f32 zz   = cos((f32)x*Tstp)*ridus;
			f32 xcord = 1-(float)x/(gridX-1);
			f32 ycord = (float)y/(gridY-1)-1;
			if (x < gridX-1) {
				mb->Vertices.push_back(video::S3DVertex(xx,yy,zz, 0,0,0,video::SColor(255,255,255,255),xcord,ycord));
			} else {  
				f32 yy   = (f32)(0.5-(float)y/(gridY-1))*2;
				f32 xx   = (f32)sin(0.0)*ridus;  
				f32 zz   = (f32)cos(0.0)*ridus;
				mb->Vertices.push_back(video::S3DVertex(xx,yy,zz, 0,0,0,video::SColor(255,255,255,255),xcord,ycord));
			}
			if (x < gridX - 1 && y < gridY - 1) {
				mb->Indices.push_back(vertCnt + 0);
				mb->Indices.push_back(vertCnt + 1);
				mb->Indices.push_back(vertCnt + 1 + gridY);
				mb->Indices.push_back(vertCnt + 1 + gridY);
				mb->Indices.push_back(vertCnt + gridY);
				mb->Indices.push_back(vertCnt + 0);
			}
			vertCnt++;
		}
	}
	return msh;
}

scene::IMesh* CreateCube(int t)
{
	t=t&0x1; // t={0|1}

	using namespace scene;
	using namespace video;
	SMesh* msh = new SMesh();
	SMeshBuffer* mb = new SMeshBuffer();
	msh->addMeshBuffer(mb);

	if (t==0) {
		S3DVertex vtx[12];
		vtx[0]  = S3DVertex(-0.5,-0.5,-0.5, -1,-1,-1, SColor(255,255,255,255), 0, 1);
		vtx[1]  = S3DVertex( 0.5,-0.5,-0.5,  1,-1,-1, SColor(255,255,255,255), 1, 1);
		vtx[2]  = S3DVertex( 0.5, 0.5,-0.5,  1, 1,-1, SColor(255,255,255,255), 1, 0);
		vtx[3]  = S3DVertex(-0.5, 0.5,-0.5, -1, 1,-1, SColor(255,255,255,255), 0, 0);
		vtx[4]  = S3DVertex( 0.5,-0.5, 0.5,  1,-1, 1, SColor(255,255,255,255), 0, 1);
		vtx[5]  = S3DVertex( 0.5, 0.5, 0.5,  1, 1, 1, SColor(255,255,255,255), 0, 0);
		vtx[6]  = S3DVertex(-0.5, 0.5, 0.5, -1, 1, 1, SColor(255,255,255,255), 1, 0);
		vtx[7]  = S3DVertex(-0.5,-0.5, 0.5, -1,-1, 1, SColor(255,255,255,255), 1, 1);
		vtx[8]  = S3DVertex(-0.5, 0.5, 0.5, -1, 1, 1, SColor(255,255,255,255), 0, 1);
		vtx[9]  = S3DVertex(-0.5, 0.5,-0.5, -1, 1,-1, SColor(255,255,255,255), 1, 1);
		vtx[10] = S3DVertex( 0.5,-0.5, 0.5,  1,-1, 1, SColor(255,255,255,255), 1, 0);
		vtx[11] = S3DVertex( 0.5,-0.5,-0.5,  1,-1,-1, SColor(255,255,255,255), 0, 0);          

		for(int i=0;i<12;i++) mb->Vertices.push_back(vtx[i]);
		u16 indices[36] = {
			0,2,1,   0,3,2,   1,5,4,   1,2,5,
			4,6,7,   4,5,6,   7,3,0,   7,6,3,
			9,5,2,   9,8,5,   0,11,10, 0,10,7
		};
		for(int i=0;i<36;i++) mb->Indices.push_back(indices[i]); 
	} else if (t==1) {
		/*
				   -++         +++
				   /6--------/5         y
				  /  |      / |         ^  z
				 /   | ++- /  |         | /
			-+- 3---------2   |         |/
				|   7- - -| - 4 +-+     *---->x
				|  /--+   |  /
				|/        | /
				0---------1/
			   ---       +--       
		*/

		S3DVertex vtx[24];
		vtx[0]  = S3DVertex(-0.5,-0.5,-0.5, -1,-1,-1, SColor(255,255,255,255), (f32)0,		(f32)0.5);
		vtx[1]  = S3DVertex( 0.5,-0.5,-0.5,  1,-1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.5);
		vtx[2]  = S3DVertex( 0.5, 0.5,-0.5,  1, 1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.0);
		vtx[3]  = S3DVertex(-0.5, 0.5,-0.5, -1, 1,-1, SColor(255,255,255,255), (f32)0,		(f32)0.0);

		vtx[4]  = S3DVertex( 0.5,-0.5,-0.5,  1,-1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.5);
		vtx[5]  = S3DVertex( 0.5,-0.5, 0.5,  1,-1, 1, SColor(255,255,255,255), (f32)0.5,	(f32)0.5);
		vtx[6]  = S3DVertex( 0.5, 0.5, 0.5,  1, 1, 1, SColor(255,255,255,255), (f32)0.5,	(f32)0.0);
		vtx[7]  = S3DVertex( 0.5, 0.5,-0.5,  1, 1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.0);

		vtx[8]  = S3DVertex( 0.5,-0.5, 0.5,  1,-1, 1, SColor(255,255,255,255), (f32)0.5,	(f32)0.5);
		vtx[9]  = S3DVertex(-0.5,-0.5, 0.5, -1,-1, 1, SColor(255,255,255,255), (f32)0.75,	(f32)0.5);
		vtx[10] = S3DVertex(-0.5, 0.5, 0.5, -1, 1, 1, SColor(255,255,255,255), (f32)0.75,	(f32)0.0);
		vtx[11] = S3DVertex( 0.5, 0.5, 0.5,  1, 1, 1, SColor(255,255,255,255), (f32)0.5,	(f32)0.0);

		vtx[12] = S3DVertex(-0.5,-0.5, 0.5, -1,-1, 1, SColor(255,255,255,255), (f32)0.75,	(f32)0.5);
		vtx[13] = S3DVertex(-0.5,-0.5,-0.5, -1,-1,-1, SColor(255,255,255,255), (f32)1,		(f32)0.5);
		vtx[14] = S3DVertex(-0.5, 0.5,-0.5, -1, 1,-1, SColor(255,255,255,255), (f32)1,		(f32)0.0);
		vtx[15] = S3DVertex(-0.5, 0.5, 0.5, -1, 1, 1, SColor(255,255,255,255), (f32)0.75,	(f32)0.0);

		vtx[16] = S3DVertex(-0.5,-0.5, 0.5, -1,-1, 1, SColor(255,255,255,255), (f32)0,		(f32)1);
		vtx[17] = S3DVertex( 0.5,-0.5, 0.5,  1,-1, 1, SColor(255,255,255,255), (f32)0.25,	(f32)1);
		vtx[18] = S3DVertex( 0.5,-0.5,-0.5,  1,-1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.5);
		vtx[19] = S3DVertex(-0.5,-0.5,-0.5, -1,-1,-1, SColor(255,255,255,255), (f32)0,		(f32)0.5);

		vtx[20] = S3DVertex(-0.5, 0.5,-0.5, -1, 1,-1, SColor(255,255,255,255), (f32)0.5,	(f32)0.5);
		vtx[21] = S3DVertex( 0.5, 0.5,-0.5,  1, 1,-1, SColor(255,255,255,255), (f32)0.25,	(f32)0.5);
		vtx[22] = S3DVertex( 0.5, 0.5, 0.5,  1, 1, 1, SColor(255,255,255,255), (f32)0.25,	(f32)1);
		vtx[23] = S3DVertex(-0.5, 0.5, 0.5, -1, 1, 1, SColor(255,255,255,255), (f32)0.5,	(f32)1);

		for(int i=0;i<sizeof(vtx)/sizeof(S3DVertex);i++) mb->Vertices.push_back(vtx[i]);
		u16 indices[] = {
			0,2,1,		0,3,2,
			4,6,5,		4,7,6,
			8,10,9,		8,11,10,
			12,14,13,	12,15,14,
			16,18,17,	16,19,18,
			20,22,21,	20,23,22,
		};
		for(int i=0;i<sizeof(indices)/sizeof(u16);i++) mb->Indices.push_back(indices[i]); 
	}
	return msh;
}

void DeviceFS_AddData(IrrlichtDevice *device)
{
	//device->getFileSystem()->changeWorkingDirectoryTo("data");
	//TODO: when irrlicht fully supports zip filesystems
	/*const irr::c8 *cwd=device->getFileSystem()->getWorkingDirectory();
	irr::io::IFileList *ifl=device->getFileSystem()->createFileList();
	for (s32 i=0; i<ifl->getFileCount(); i++) {
		if (!ifl->isDirectory(i)) {
			const c8 *cfn=ifl->getFullFileName(i);
			irr::core::string<c8> s=cfn;
			if (s.subString(s.findLast('.')+1, 3).equals_ignore_case("zip")) {
				if (!device->getFileSystem()->addZipFileArchive(cfn, true,false)) fprintf(stderr, "ARRR: %s\n", cfn);
			}
		}
	}
	ifl->drop();
	device->getFileSystem()->changeWorkingDirectoryTo(cwd);*/
}

core::vector3df VectorToEuler(core::vector3df)
{
	core::vector3df rv;
	
	return rv;
}

void QuaternionToEuler(const dQuaternion quaternion, dReal *euler)
{
	dReal w,x,y,z;
	w=quaternion[0];
	x=quaternion[1];
	y=quaternion[2];
	z=quaternion[3];
	double sqw = w*w;    
	double sqx = x*x;    
	double sqy = y*y;    
	double sqz = z*z; 

	// heading
	euler[2] = (dReal) (atan2(2.0 * (x*y + z*w),(sqx - sqy - sqz + sqw))*GRAD_PI);
	// bank
	euler[0] = (dReal) (atan2(2.0 * (y*z + x*w),(-sqx - sqy + sqz + sqw))*GRAD_PI);  
	// attitude
	euler[1] = (dReal) (asin(-2.0 * (x*z - y*w))*GRAD_PI);

	//fprintf(stderr, "%7.5g %7.5g %7.5g %7.5g => %7.5g %7.5g %7.5g\n", q2[0], q2[1], q2[2], q2[3], euler[0], euler[1], euler[2]);
}

core::vector3df ConvertToWorldSpace(const core::vector3df &local_point, dBody *body)
{
    dVector3 result;

	body->getRelPointPos(local_point.X, local_point.Y, local_point.Z, result);

    return core::vector3df(result[0], result[1], result[2]);
}

core::vector3df dBodyGetDirection(dBody *body)
{
	core::vector3df centre  = ConvertToWorldSpace(core::vector3df(0.0, 0.0, 0.0), body);
	core::vector3df at      = ConvertToWorldSpace(core::vector3df(1.0, 0.0, 0.0), body);
    core::vector3df direction = at - centre;
    direction.normalize();

    return direction;
}

std::string StringLower(const std::string &str)
{
	char buf[101];
	strcpy(buf, str.c_str());
	int i, l=(int)strlen(buf);
	for (i=0; i<l; i++) buf[i]=tolower(buf[i]);
	//strlwr(buf);

	return buf;
}

void vector_convert(irr::core::vector3df &x, dVector3 &v)
{
	v[0]=x.X;
	v[1]=x.Y;
	v[2]=x.Z;
}
