/**
 * \file
 * This file contains declaration of SGameplace class.
**/

#ifndef _SGAMEPLACE_H_
#define _SGAMEPLACE_H_

#include <string>
#include <ode/ode.h>
#include <irrlicht/irrlicht.h>
#include "Acfgfile.h"
#include "Sgeomobj.h"

/**
 * Contains data for main terrain object of simulation
**/
struct SGameplace:public SGeomObj {
	/**
	 * initializes the class and zeroes out critical variables
	**/
	SGameplace():SGeomObj(GAMEPLACE, -1) {}

	/**
	 * tries to release as much memory used by the class as possible
	**/
	~SGameplace() {}

	/**
	 * initializes the object from config file
	 * @param cfg input configuration file
	 * @param path path of object in configuration file to initialize
	 * @param space space into which the gameplace will be put
	 * @return true if successful, false otherwise
	**/
	bool Init(CfgFile &cfg, std::string path, dSpace &space, int param_);

	/**
	 * @return returns 'user specified' param (set by init method)
	**/
	int GetParam() {return param;}
protected:
	int param; //!< 'user specified' parameter 
};

#endif //_SGAMEPLACE_H_
