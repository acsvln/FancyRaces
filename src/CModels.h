/**
 * \file
 * This file contains declaration and part of implementation of CModelAttr and CCarModel classes.
**/

#ifndef _CMODELS_H_
#define _CMODELS_H_

#include <irrlicht/irrlicht.h>
#include <deque>
#include <string>
#include <map>
#include "Autil.h"
#include "Acfgfile.h"
#include <openalpp/alpp.h>

using namespace irr;

/**
 * This class contains the client-side information about a model in the scene (models are all the dynamic things in the scene - wheels, car bodies etc.).
**/
struct CModelAttr {
	scene::ISceneNode* node; //!< an irrlicht scene node pointer.
	scene::ISceneNode* node_debug; //!< an irrlicht scene debug node pointer.

	/**
	 * This structure defines position and rotation of object in space at given time
	**/
	struct timestamp {
		core::vector3df pos; //!< position
		core::quaternion qua; //!< rotation (represented by quaternion)
		u32 tim; //!< time of the position informations
		
		/**
		 * Constructor
		 * \n Initializes the structure members according to input parameters
		**/
		timestamp(core::vector3df p=core::vector3df(), core::quaternion q=core::quaternion(), u32 t=0):pos(p), qua(q), tim(t) {}
	};

	std::deque<timestamp> locations; //!< list of positions and rotations (=quaternions) of the object

	/**
	 * Returns first two items of CModelAttr::locations deque - after all items with tim < t are removed.
	**/
	std::pair<timestamp, timestamp> getFirstTwo(u32 t) {
		//first remove all items with time < t; however at least 2 items shall remain in the end (if possible)
		while (locations.size()>2) {
			if (locations[0].tim<t && locations[1].tim<t) locations.pop_front();
			else break;
		}
		if (locations.size()==0) return std::make_pair(timestamp(), timestamp());
		else if (locations.size()==1) return std::make_pair(locations[0], timestamp());
		return std::make_pair(locations[0], locations[1]);
	}

	core::vector3df arot; //!<additional rotation (to fix model rotation)

	u32 speed; //!< calculated speed of the object (move() calculates it)
	
	/**
	 * zeroes out structure members
	**/
	CModelAttr():
		node(0),
		node_debug(0),
		speed(0)
	{
		locations.clear();
	}

	/**
	 * Destructor. Currently it does nothing.
	**/
	virtual ~CModelAttr() {}

	/**
	 * updates the node position by approximating position in specified time
	 * @param time Time, where the "user" wants to know the position. Note that each time you call function with some time parameter, the next time you should only use a bigger time value.
	 * @param calcbspeed If true, actual body speed is calculated and set to CModelAttr::speed
	**/
	virtual void move(u32 time, bool calcbspeed=false) {
		std::pair<timestamp, timestamp> loc=getFirstTwo(time);

		//std::cerr << time << " -> " << loc.first.tim << " / "<< locations.size() << std::endl;
		if (loc.second.tim==0 && loc.first.tim!=0) { //at least we've got one value, set the second value to the same
			loc.second=loc.first;
			loc.second.tim+=100; //to avoid eventual division by 0
		}
		if (loc.first.tim!=0) { //let's to some business :)
			irr::core::vector3df p;
			f32 dtime=(f32)(loc.second.tim-loc.first.tim);
			f32 utime=(f32)(time-loc.first.tim);
			p=loc.second.pos*utime/dtime+loc.first.pos*(f32)(dtime-utime)/dtime;
			node->setPosition(p);
			if (node_debug) node_debug->setPosition(p);
			
			if (calcbspeed) {
				speed=(u32)(3.6*(loc.second.pos-loc.first.pos).getLength()/(dtime/1000));
				//r_bodyspeed=(u32)(3.6*((cm.body.pos-cm.body.posf).getLength()/(f32)(cm.body.dtime/1000.0)))
			}

			core::quaternion q, q2;
			q.slerp(loc.first.qua, loc.second.qua, (f32)utime/(f32)dtime);
			q2.set(arot.X, arot.Y, arot.Z);
			q=q2*q;
			core::vector3df ea;
			q.toEuler(ea);

			if ((-10<ea.X && ea.X<10) && (-10<ea.Y && ea.Y<10) && (-10<ea.Z && ea.Z<10)) { //if we get some bad division etc.
				ea=ea*(f32)(180/3.1415926);
				node->setRotation(ea);
				if (node_debug) node_debug->setRotation(ea);
			}
		} else {
			node->setPosition(loc.first.pos);
			core::vector3df ea;
			loc.first.qua.toEuler(ea);
			ea=ea*(f32)(180/3.1415926);
			node->setRotation(ea);
			if (node_debug) {
				node_debug->setPosition(loc.first.pos);
				node_debug->setRotation(ea);
			}
		}
	}

	/**
	 * Parses str and adds the position information together with specified time to the end of CModelAttr::locations deque.
	**/
	void setPosRot(std::string str, u32 _time) {
		float _pos[3], _q[4];
		sscanf(str.c_str(), "%g %g %g %g %g %g %g", &_pos[0], &_pos[1], &_pos[2], &_q[3], &_q[0], &_q[1], &_q[2]);
		locations.push_back(timestamp(core::vector3df(_pos[0], _pos[1], _pos[2]), core::quaternion(_q[0], _q[1], _q[2], _q[3]), _time));
	}
};

/**
 * structure containing info about moveable terrain parts
**/
struct CTerrainPart:public CModelAttr {
public:
	/**
	 * simple constructor - zeroes out variables
	**/
	CTerrainPart():lastInfoTime(0),debugTextNode(0) {}
	
	/**
	 * destructor
	**/
	virtual ~CTerrainPart() {}

	u32 lastInfoTime; //!<last time we got new info from server, not used inside CCarModel
	irr::scene::ISceneNode *debugTextNode; //!< scene node containing text above the model

	/**
	 * show/hide debug objects
	**/
	void show_debug(bool show) {
		debugTextNode->setVisible(show);
	}

	virtual void move(u32 time) {
		if (debugTextNode->isVisible()) {
			char buf[100];
			core::vector3df pos=node->getPosition();
			sprintf(buf, "%.2f %.2f %.2f", (float)pos.X, (float)pos.Y, (float)pos.Z);
			wchar_t *wb=a2u(buf);
			((scene::ITextSceneNode*)debugTextNode)->setText(wb);
			delete wb;
		}
		((CModelAttr)*this).move(time);
		debugTextNode->setPosition(node->getPosition()+core::vector3df(0, 2, 0));
	}
	/**
	 * initializes the debugTextNode
	**/
	void InitDebug(IrrlichtDevice *device, scene::ISceneManager* sm) {
		debugTextNode=sm->addTextSceneNode(device->getGUIEnvironment()->getBuiltInFont(), 
			L"DEBUG TEXT", 
			video::SColor(255,255,100,50), 0);
	}

	/**
	 * Clears position data
	**/
	void clearPosData() {
		locations.clear();
	}
};

/**
 * This class contains the client-side information about a cars in the simulation.
**/
class CCarModel {
public:
	/**
	 * Constructor. Just tnitializes all internal values to 0.
	**/
	CCarModel():
	  c_steer(0), c_accel(0), c_brakes(0), c_handbrake(0),
	  c_clutch(0), c_gear(0), c_rescue(0),

	  r_steer(0), r_accel(0), r_brakes(0), r_handbrake(0),
	  r_clutch(0), r_gear(0), r_rescue(0),

	  debugTextNode(0)
	  {}

	/**
	 * Destructor. Currently it does nothing.
	**/
	  ~CCarModel() {}
public:
	int id; //!< car id
	std::string profile; //!< car profile path
	std::string nick; //!< nick of the player controlling this car

	CModelAttr body; //!< model of body
	std::deque<CModelAttr> wheels; //!< deque of wheel models
	irr::scene::ISceneNode *debugTextNode; //!< scene node containing text above the model

	int maxgear, gearN, gearR; //!< some gearbox parameters

	//controls car parameters
	f32 c_steer; //!< desired control parameter - steer
	f32 c_accel; //!< desired control parameter - acceleration
	f32 c_brakes; //!< desired control parameter - brakes
	f32 c_handbrake; //!< desired control parameter - handbrake
	f32 c_clutch; //!< desired control parameter - clutch
	f32 c_rescue; //!< desired control parameter - rescue force
	int c_gear; //!< desired control parameter - gear

	//real car parameters
	f32 r_steer; //!< controls really used by server - steer
	f32 r_accel; //!< controls really used by server - acceleration
	f32 r_brakes; //!< controls really used by server - brakes
	f32 r_handbrake; //!< controls really used by server - handbrake
	f32 r_clutch; //!< controls really used by server - clutch
	f32 r_rescue; //!< controls really used by server - rescue force
	int r_gear; //!< the controls really used by server - gear
	int r_rpm; //!<current rpm of the engine
	int r_speed; //!<current speed of the vehicle
	
	int maxrpm; //!<maximal rpm of the engine
	int minrpm; //!<minimal rpm of the engine
	
	u32 r_bodyspeed; //!< speed (in km/h) of the car body

	u32 lastInfoTime; //!<last time we got new info from server

	openalpp::ref_ptr<openalpp::Source> snd_engine; //!<pointer to the OpenAL sound source
	double snd_engine_pitch_low, snd_engine_pitch_high; //!<engine sound pitches

	std::deque<std::pair<int, irr::u32> > checkpoints; //!<array of checkpoints and finishes car went thru (the second value is 0 if not crossed, time when it happened otherwise)

	/**
	 * applies the statical properties (position & rotation) to the irrlicht objects - by calling the CModelAttr::move() method.
	**/
	void move(u32 time) {
		body.move(time, true);
		//if (time==0) body.dbg();
		for (std::deque<CModelAttr>::iterator j=wheels.begin(); j!=wheels.end(); j++) {
			j->move(time);
		}
		//std::cerr << wheels[0].node->getRotation().X << " " << wheels[0].node->getRotation().Y << " " << wheels[0].node->getRotation().Z << std::endl;
		if (debugTextNode->isVisible()) {
			char buf[100];
			core::vector3df pos=body.node->getPosition();
			sprintf(buf, "%.2f %.2f %.2f", (float)pos.X, (float)pos.Y, (float)pos.Z);
			wchar_t *wb=a2u(buf);
			((scene::ITextSceneNode*)debugTextNode)->setText(wb);
			delete wb;
		}
	}

	/**
	 * Copy r_* variables to c_* ones (used at game initialization)
	**/
	void r2c() {
		c_steer=r_steer; c_accel=r_accel; c_brakes=r_brakes; c_handbrake=r_handbrake; c_rescue=r_rescue;
		c_clutch=r_clutch; c_gear=r_gear;
	}

	/**
	 * show/hide debug objects
	**/
	void show_debug(bool show);

	/**
	 * Load data required (according to profile)
	**/
	void Load(IrrlichtDevice *device, scene::ISceneManager* sm, video::IVideoDriver* driver);

	/**
	 * Load model and return it's scene node handle
	 * @param cfg config file of the car
	 * @param what object path we want to load (probably Look/Body/ or Look/Wheel/)
	 * @return pointer to the createt scene node...
	**/
	scene::ISceneNode* loadModel(CfgFile &cfg, std::string what, IrrlichtDevice *device, scene::ISceneManager* sm, video::IVideoDriver* driver);

	/**
	 * Clears position data for all vehicle parts
	**/
	void clearPosData() {
		body.locations.clear();
		for (std::deque<CModelAttr>::iterator j=wheels.begin(); j!=wheels.end(); j++) {
			j->locations.clear();
		}
	}

	/**
	 * start screaming :-)
	**/
	void soundStart();

	/**
	 * stop screaming :-)
	**/
	void soundStop();

	/**
	 * Set correct sound source properties (according to the current movement etc.)
	**/
	void soundSetProperties();
};

#endif
