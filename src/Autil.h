/**
 * \file
 * This file contains declarations of heler functions used across the whole application.
**/

#ifndef _AUTIL_H_
#define _AUTIL_H_

#include <string>
#include <deque>
#include <irrlicht/irrlicht.h>
#include <ode/ode.h>
#include <ode/odecpp.h>
#include <map>

/**
 * This function puts it's parameters to the game's log (the concrete place depends on compilation target type). You have to call DBGCOUT_i() before you first use this function, and DBGCOUT_d() before the program exits.
**/
void DBGCOUT(std::string sect, std::string action, std::string param);

void DBGCOUT_data_beg();
std::deque<std::string>& DBGCOUT_data();
void DBGCOUT_data_end();

/**
 * this structure holds commad message - first item is always name of item, second is the value. Name of header of the command is '%'
**/
typedef std::map<std::string, std::string> cmd_map;

/**
 * converts cmd_map to string representation
**/
void cmd_Map2String(cmd_map &mp, std::string &rv);

/**
 * converts string to map
**/
void cmd_String2Map(std::string &str, cmd_map &rv);

/**
 * convert a integer to a string
 * @return returns string form of the number
**/
std::string numtostring(int i);

/**
 * convert a unsigned integer to a string
 * @return returns string form of the number
**/
std::string numtostring(unsigned int i);

/**
 * convert a double to a string
 * @return returns string form of the number
**/
std::string numtostring(double i, const char* format="%g");

/**
 * Converts widechar string to char (unicode to ansi).
 * @return returns pointer to the converted string. You have to free it by using delete...
**/
char* u2a( const wchar_t* src);

/**
 * Converts char string to widechar  (ansi to unicode).
 * @return returns pointer to the converted string. You have to free it by using delete...
**/
wchar_t* a2u( const char* src);

typedef struct {
	double x;
	double y;
} Point2D;

/**
 * Calculates point of a bezier curve on the specified position
 * @param cp array of 4 control points (starting point, 1st control point, 2nd control point, end point)
 * @param t point of the bezier curve (0.0 - 1.0)
 * @return point of the bezier curve
**/
Point2D Bezier_PointOnCubicBezier(Point2D* cp, double t);

/**
 * Computes given count of points of a bezier curve
 * @param cp array of 4 control points
 * @param numberOfPoints resolution of the resulting array
 * @param curve array of points of the curve - you have to initialize the array before calling to size <sizeof(Point2D) * numberOfPoints>
 * @return values of bezier curve in the 'curve' parameter
**/
void Bezier_Compute(Point2D* cp, int numberOfPoints, Point2D* curve);

/**
 * Removes unneeded char from the line; used for configuration load
 * @param str string to be trimmed
 * @return str
**/
char *strtrim(char *str);

/**
 * Creates a cube with center at position (0,0,0), with side length of 1.
 * @param texturing if is equal to 0, all sides use the whole texture; if it's, each side uses other part of texture (how to make the image can be seen in some of example maps)
**/
irr::scene::IMesh* CreateCube(int texturing=0);

/**
 * Creates a cylinder with center at position (0,0,0), with radius "ridus". The "cylinder circle" is divided in as many parts as specified by gridX and gridY.
**/
irr::scene::IMesh* CreateCylinder(int gridX=25, int gridY=25, irr::f32 ridus=1);

/**
 * Add standard data to device filesystem
**/
void DeviceFS_AddData(irr::IrrlichtDevice *device);

/**
 * Calculates euler angles for the vector
**/
irr::core::vector3df VectorToEuler(irr::core::vector3df);

/**
 * Converts the quaternion q to euler rotation angles (euler is a pointer to the dReal[3] buffer).
**/
void QuaternionToEuler(const dQuaternion quaternion, dReal *euler);

/**
 * Given a body and a point in it's local coordinate system (centre of body is origin),
 * return that point in the world's coordinate system ( (0,0,0) is origin ).
**/
irr::core::vector3df ConvertToWorldSpace(const irr::core::vector3df &local_point, dBody *body);

/**
 * get object rotation vector
**/
irr::core::vector3df dBodyGetDirection(dBody *body);

/**
 * convert string to lowercase
 * @return the str parameter with all characters lowercase
**/
std::string StringLower(const std::string &str);

/**
 * converts irrlicht vector representation to ode's
**/
void vector_convert(irr::core::vector3df &x, dVector3 &v);

#ifndef NDEBUG
/**
 * This define is used in debug mode to ensure the write out of message in case of error.
**/
#define ASSERT( isOK ) \
 ( (isOK) ? \
      (void)0 : \
      (void)DBGCOUT("", "ASSERT", "'"+(string)#isOK + "' failed on line "+numtostring(__LINE__)+" of "+__FILE__) )
#else
# define ASSERT( unused ) ;
#endif

#endif //_AUTIL_H_
