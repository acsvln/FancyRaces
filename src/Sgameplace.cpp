/**
 * \file
 * This file contains implementation of the SGameplace class.
**/

#include "Sgameplace.h"
#include "Autil.h"

using namespace std;
using namespace irr;

bool SGameplace::Init(CfgFile &cfg, std::string path, dSpace &space, int param_)
{
	param=param_;

	core::vector3df v=cfg.Getval_vector(path+"size"), v2;
	ode_geom = new dBox(space.id(), v.X, v.Y, v.Z);
	//gid = new dCapsule(0, radius, 0.2);
	ode_geom->setData((SGeomObj*)this);
	v=cfg.Getval_vector(path+"pos");
	v2=cfg.Getval_vector(path+"rot");
	SetPosRot(v.X, v.Y, v.Z, v2.X, v2.Y, v2.Z);

	return true;
}
