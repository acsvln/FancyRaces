/**
 * \file
 * This file contains implementation of Socket, SocketServer, SocketClient and SelectScoket classes.
**/

/* 
Socket.cpp

Copyright (C) 2002-2004	Ren�Nyffenegger

This source	code is	provided 'as-is', without any express or implied
warranty. In no	event will the author be held liable for any damages
arising	from the use of	this software.

Permission is granted to anyone	to use this	software for any purpose,
including commercial applications, and to alter	it and redistribute	it
freely,	subject	to the following restrictions:

1. The origin of this source code must not be misrepresented; you must not
claim that you wrote the original source code. If you use this source code
in a product, an acknowledgment	in the product documentation would be
appreciated	but	is not required.

2. Altered source versions must	be plainly marked as such, and must	not	be
misrepresented as being	the	original source	code.

3. This	notice may not be removed or altered from any source distribution.

Ren Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

#include <iostream>

#include "Asocket.h"
#include <errno.h>

#ifndef WIN32
#include <sys/time.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#else
typedef int socklen_t;
#endif

using namespace	std;

int	Socket::nofSockets_= 0;

void Socket::Start() {
#ifdef WIN32
	if (!nofSockets_) {
		WSADATA	info;
		if (WSAStartup(MAKEWORD(2,0), &info)) {
			throw "Could not start WSA";
		}
	}
#endif
	++nofSockets_;
}

void Socket::End() {
#ifdef WIN32
	WSACleanup();
#endif
}

Socket::Socket() : s_(0) {
	Start();
	// UDP:	use	SOCK_DGRAM instead of SOCK_STREAM
	s_ = socket(AF_INET,SOCK_STREAM,0);

	if (s_ == INVALID_SOCKET) {
		throw "INVALID_SOCKET";
	}

	refCounter_	= new int(1);
}

Socket::Socket(SOCKET s, in_addr &oa) :	s_(s), oppositeAddr(oa)	{
	Start();
	refCounter_	= new int(1);
};

Socket::~Socket() {
	if (! --(*refCounter_))	{
		Close();
		delete refCounter_;
	}


	--nofSockets_;
	if (!nofSockets_) End();
}

Socket::Socket(const Socket& o)	{
	refCounter_=o.refCounter_;
	(*refCounter_)++;
	s_		   =o.s_;


	nofSockets_++;
}

Socket&	Socket::operator =(Socket& o) {
	(*o.refCounter_)++;


	refCounter_=o.refCounter_;
	s_		   =o.s_;


	nofSockets_++;


	return *this;
}

void Socket::Close() {
#ifdef WIN32
	closesocket(s_);
#else
	close(s_);
#endif
}

std::string	Socket::ReceiveBytes() {
/*	std::string	ret;
	char buf[1024];

	for (;;) {
		u_long arg = 0;
		if (ioctlsocket(s_, FIONREAD, &arg) != 0) break;
		if (arg == 0) break;
		if (arg > 1024) arg = 1024;
		int rv = recv(s_, buf, arg, 0);
		if (rv <= 0) break;
		std::string t;
		t.assign (buf, rv);
		ret += t;
	}

	return ret;*/
	throw "not implemented";
}

std::string	Socket::ReceiveLine(int &err) {
	std::string ret;
	err=0;
	while (1) {
		char r;

		switch(recv(s_, &r, 1, 0)) {
		case 0: // not connected anymore
			err=2;
			return "";
		case SOCKET_ERROR:
#ifdef WIN32
			switch (WSAGetLastError()) {
#else
			switch (errno) {
#endif
			case EAGAIN:
#ifndef WIN32
				err=1;
#endif
				return ret;
#ifdef WIN32
			case WSAEWOULDBLOCK:
				err=1;
				return ret;
#endif
			default: //not connected anymore
				err=2;
				return "";
			}
		}
		ret	+= r;
		if (r == SOCK_ENDL) return ret;
	}
}

void Socket::SendLine(std::string s) {
	s += SOCK_ENDL;
	send(s_,s.c_str(),(int)s.length(),0);
}

void Socket::SendBytes(const std::string& s) {
	send(s_,s.c_str(),(int)s.length(),0);
}

void Socket::SetBlocking(TypeSocket type)
{
#ifdef WIN32
	u_long arg = type==NonBlockingSocket;
	ioctlsocket(s_,	FIONBIO, &arg);
#else
	fcntl(s_, F_SETFL, (type==NonBlockingSocket)?O_NONBLOCK:0);
#endif
}

SocketServer::SocketServer(int port, int connections, TypeSocket type) {
	sockaddr_in	sa;

	memset(&sa,	0, sizeof(sa));

	sa.sin_family =	PF_INET;
	sa.sin_port	= htons(port);
	s_ = socket(AF_INET, SOCK_STREAM, 0);
	if (s_ == INVALID_SOCKET) {
		throw "INVALID_SOCKET";
	}

	if(type==NonBlockingSocket)	{
#ifdef WIN32
		u_long arg = 1;
		ioctlsocket(s_,	FIONBIO, &arg);
#else
		fcntl(s_, F_SETFL, O_NONBLOCK);
#endif
	}

	/* bind	the	socket to the internet address */
	if (bind(s_, (sockaddr *)&sa, sizeof(sockaddr_in)) == SOCKET_ERROR)	{
#ifdef WIN32
		closesocket(s_);
#else
		close(s_);
#endif
		throw "INVALID_SOCKET";
	}

	listen(s_, connections);							   
}

Socket*	SocketServer::Accept() {
	unsigned char sabuf[256];
	socklen_t len=sizeof(sabuf);

	SOCKET new_sock	= accept(s_, (sockaddr*)&sabuf,	&len);
	sockaddr_in	*sa=(sockaddr_in *)sabuf;
	//TODO:	check if the address format	is correct (might be IPv6)

	if (new_sock ==	INVALID_SOCKET)	{
#ifdef	WIN32
		int rc = WSAGetLastError();
		if(rc==WSAEWOULDBLOCK) {
#else
		int rc=errno;
		if(rc==EAGAIN) {
#endif
			return 0; // non-blocking call,	no request pending
		}
		else {
			throw "Invalid Socket";
		}
	}

	Socket*	r =	new	Socket(new_sock, sa->sin_addr);
	return r;
}

SocketClient::SocketClient(const std::string& host,	int	port) :	Socket() {
	std::string	error;


	hostent	*he;
	if ((he	= gethostbyname(host.c_str())) == 0) {
		error =	strerror(errno);
		throw error;
	}


	sockaddr_in	addr;
	addr.sin_family	= AF_INET;
	addr.sin_port =	htons(port);
	addr.sin_addr =	*((in_addr *)he->h_addr);
	memset(&(addr.sin_zero), 0,	8);	


	if (::connect(s_, (sockaddr	*) &addr, sizeof(sockaddr))) {
#ifdef WIN32
		error =	strerror(WSAGetLastError());
#else
		error =	strerror(errno);
#endif
		throw error;
	}
}

SocketSelect::SocketSelect(Socket const	* const	s1,	Socket const * const s2, TypeSocket	type) {
	FD_ZERO(&fds_);
	FD_SET(const_cast<Socket*>(s1)->s_,&fds_);
	if(s2) {
		FD_SET(const_cast<Socket*>(s2)->s_,&fds_);
	}	  


#ifdef WIN32
	TIMEVAL	tval;
	TIMEVAL	*ptval;
#else
	timeval tval;
	timeval *ptval;
#endif
	tval.tv_sec	 = 0;
	tval.tv_usec = 1;


	if(type==NonBlockingSocket)	{
		ptval =	&tval;
	}
	else { 
		ptval =	0;
	}


	if (select (0, &fds_, (fd_set*)	0, (fd_set*) 0,	ptval) 
		== SOCKET_ERROR) throw "Error in select";
}

bool SocketSelect::Readable(Socket const * const s)	{
	if (FD_ISSET(s->s_,&fds_)) return true;
	return false;
}
