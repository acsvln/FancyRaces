/**
 * \file
 * This file contains implementations of some helper functions and the CfgFile
**/

#include "Acfgfile.h"
#include <fstream>
#include <irrlicht/IFileSystem.h>
#include <irrlicht/IReadFile.h>
#include <iostream>
#include "Autil.h"

using namespace std;

char *strtrim(char *str) //removes end-of-line characters, whitespaces, comments from the end of string
{
	int i;
	if (str==0 || *str=='\0') return str;
	for (i=(int)(strlen(str)-1); i>=0; i--) {
		if (str[i]=='#') str[i]='\0';
	}

	for (i=(int)(strlen(str)-1);i>=0; i--) {
		if (str[i]==' ' || str[i]=='\t' || str[i]==0x0d || str[i]==0x0a) str[i]='\0';
		else break;
	}

	return str;
}

char *strstart(char *str)
{
	while (*str==' ' || *str=='\t') str++;
	return str;
}

void cfg_getline(irr::io::IReadFile *f, char *buf, int max)
{
	while (f->getPos()<=f->getSize()) {
		if ((!f->read(buf, 1)) || (*buf==0x0a)) {
			*buf=0;
			break;
		}
		buf++;
		max--;
		if (max==0) break;
	}
}

bool CfgFile::Load(irr::io::IFileSystem *fs, std::string path)
{
	irr::io::IReadFile* fi;
	char buf[1024];
	deque<string> cursect;
	deque<string>::iterator i;
	string cursectstr, x;
	
	cfg.empty();
	
	fi=fs->createAndOpenFile(path.c_str());
	if (!fi) {
		DBGCOUT("CfgFile", "Load", "Can't open: "+path);
		return false;
	}

	while (fi->getPos()<fi->getSize()) {
		cfg_getline(fi, buf, 1024);
		strtrim(buf);

		if (buf[0]=='\0') continue;

		if (unsigned(strstart(buf)-buf)!=cursect.size()) {
			cursect.resize(strstart(buf)-buf, "!");
		}
		if (!strchr(buf, '=')) {
			//if (!cursect.empty())
			//	cursect.pop_back();
			cursect.push_back(strstart(buf));
			cursectstr="";
			for (i=cursect.begin(); i!=cursect.end(); i++)
				cursectstr+=*i+"/";
		} else {
			x=strchr(buf, '=')+1;
			*(strchr(buf, '='))=0;
			cfg[cursectstr+strstart(buf)]=x;
		}
	}
	fi->drop();
	return true;
}

string CfgFile::Getval_str(std::string path, std::string def)
{
	map<string, string>::iterator mi;

	mi=cfg.find(path);
	if (mi==cfg.end()) return def;
	else return mi->second;
}

bool CfgFile::Setval_str(std::string path, std::string val)
{
	map<string, string>::iterator mi;

	mi=cfg.find(path);
	if (mi==cfg.end()) return false;
	mi->second=val;
	return true;
}

int CfgFile::Getval_int(std::string path, int def)
{
	string str=Getval_str(path, "");
	char *end;

	if (str=="") return def;
	else return strtol(str.data(), &end, 10);
}

double CfgFile::Getval_double(std::string path, double def)
{
	string str=Getval_str(path, "");
	char *end;

	if (str=="") return def;
	else return strtod(str.data(), &end);
}

unsigned int CfgFile::Getval_flags(std::string path, std::map<std::string, unsigned int> vals, unsigned int def)
{
	string str=Getval_str(path, "");
	char *tok;
	const char *sep="|";
	char buf[1024];
	unsigned int rv=0;
	map<string, unsigned int>::iterator i;

	if (str=="") return def;

	strcpy(buf, str.data());

	tok=strtok(buf, sep);
	while (tok!=0) {
		i=vals.find(tok);
		if (i!=vals.end()) {
			rv|=i->second;
		}
		tok=strtok(0, sep);
	}
	return rv;
}

irr::core::vector3df CfgFile::Getval_vector(std::string path, irr::core::vector3df def)
{
	string str=Getval_str(path, "");

	if (str=="") return def;
	else {
		float x, y, z;
		sscanf(str.c_str(), "%g %g %g", &x, &y, &z);

		return irr::core::vector3df((irr::f32)x, (irr::f32)y, (irr::f32)z);
	}
}

bool CfgFile::Getval_flag(std::string path, std::string flag)
{
	string str=Getval_str(path, "");
	char *tok;
	const char *sep="|";
	char buf[1024];
	unsigned int rv=0;

	if (str=="") return false;

	strcpy(buf, str.data());

	tok=strtok(buf, sep);
	while (tok!=0) {
		if (flag==tok) return true;
		tok=strtok(0, sep);
	}
	return false;
}

bool CfgFile::Getval_exists(std::string path)
{
	const char *asdf="Th!s_V4lu3_W0n't_B3_Us3d_Evr-And_1'm_t00_l4zy_t0_d0_!t_th3_r1gh7_w4y";
	string str=Getval_str(path, asdf);

	if (str==asdf) return false;
	else return true;
}
